﻿using BookStore_Core.Models;

namespace BookStore_Core.ViewModel
{
    public class ShoppingCartViewModel
    {
        public Sach Sach { get; set; }
        public Cart Cart { get; set; }
        public BangGia BangGia { get; set; }
        public int GiaTong { get; set; }
    }
}
