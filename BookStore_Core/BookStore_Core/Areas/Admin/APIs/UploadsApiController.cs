﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace BookStore_Core.Areas.Admin.APIs
{
    [Route("api/[controller]")]
    [ApiController]
    public class UploadsApiController : ControllerBase
    {
        [Obsolete]
        public static IHostingEnvironment _environment;

        [Obsolete]
        public UploadsApiController(IHostingEnvironment environment)
        {
            _environment = environment;
        }
        public class FIleUploadAPI
        {
            public IFormFile files { get; set; }
        }
        [HttpGet]
        public string Get()
        {
            return "Nothing";
        }
        [HttpPost]
        [Obsolete]
        public async Task<string> PostAsync(IFormFile files)
        {
            if (files.Length > 0)
            {
                try
                {
                    if (!string.Equals(files.ContentType, "image/jpg", StringComparison.OrdinalIgnoreCase) &&
                        !string.Equals(files.ContentType, "image/jpeg", StringComparison.OrdinalIgnoreCase) &&
                        !string.Equals(files.ContentType, "image/pjpeg", StringComparison.OrdinalIgnoreCase) &&
                        !string.Equals(files.ContentType, "image/gif", StringComparison.OrdinalIgnoreCase) &&
                        !string.Equals(files.ContentType, "image/x-png", StringComparison.OrdinalIgnoreCase) &&
                        !string.Equals(files.ContentType, "image/png", StringComparison.OrdinalIgnoreCase))
                    {
                        return "File isn't images";
                    }
                        // var fileName = Path.GetFileName(files.FileName);
                    var path = Path.Combine(_environment.WebRootPath, "BookImages", Path.GetFileName(files.FileName));
                    var stream = new FileStream(path, FileMode.Create);
                    await files.CopyToAsync(stream);
                    return "Successful";
                }
                catch (Exception ex)
                {
                    return ex.ToString();
                }
            }
            else
            {
                return "Unsuccessful";
            }

        }
    }
}
