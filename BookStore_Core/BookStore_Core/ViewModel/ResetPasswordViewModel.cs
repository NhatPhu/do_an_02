﻿using System.ComponentModel.DataAnnotations;

namespace BookStore_Core.ViewModel
{
    public class ResetPasswordViewModel
    {
        [Display(Name = "Mật khẩu mới")]
        [Required]
        [DataType(DataType.Password)]
        public string NewPassword { get; set; }

        [Display(Name = "Nhập lại mật khẩu")]
        [DataType(DataType.Password)]
        [Compare("NewPassword", ErrorMessage = "Không khớp")]
        public string ConfirmPassword { get; set; }

        [Required]
        public string ResetCode { get; set; }
    }
}
