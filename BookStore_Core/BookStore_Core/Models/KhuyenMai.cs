﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace BookStore_Core.Models
{
    public class KhuyenMai
    {
        [Key]
        public string MaKhuyenMaiId { get; set; }
        public DateTime NgayBatDau { get; set; }
        public DateTime NgayKetThuc { get; set; }
        public string TenKhuyenMai { get; set; }
        public int HoaDonToiThieu { get; set; }
        public int SoLuongSpCungLoai { get; set; }
        public int SoLuongSpKhacLoai { get; set; }
        public string HinhAnh { get; set; }
        public bool TrangThai { get; set; }
        //public int PhanTramGiam { get; set; }
        [NotMapped]
        public List<BangGia> BangGias { get; set; }
    }
}
