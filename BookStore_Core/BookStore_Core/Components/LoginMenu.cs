﻿using BookStore_Core.DAL;
using BookStore_Core.Models;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using System.Linq;
using System.Threading.Tasks;

namespace BookStore_Core.Components
{
    public class LoginMenu : ViewComponent
    {
        private readonly BookStoreContext _context;
        public LoginMenu(BookStoreContext context)
        {
            _context = context;
        }
        public async Task<IViewComponentResult> InvokeAsync()
        {
            Login login = new Login();
            if (HttpContext.Session.GetString("UserName") != null && HttpContext.Session.GetString("UserName").ToString() != "")
            {
                var userEmail = HttpContext.Session.GetString("UserName").ToString();
                var user = await _context.Logins.FirstOrDefaultAsync(p => p.LoginName == userEmail);

                if (user != null)
                {
                    var cus = _context.Customers.FirstOrDefault(p => p.CustomerEmail == userEmail);
                    if (cus != null)
                    {
                        ViewBag.Diem = cus.DiemSo;
                    }
                    login = user;
                }
            }
            return View(login);
        }
    }
}
