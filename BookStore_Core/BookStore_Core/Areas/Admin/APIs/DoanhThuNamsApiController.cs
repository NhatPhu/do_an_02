﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using BookStore_Core.DAL;
using BookStore_Core.Models;

namespace BookStore_Core.Areas.Admin.APIs
{
    [Route("api/[controller]")]
    [ApiController]
    public class DoanhThuNamsApiController : ControllerBase
    {
        private readonly BookStoreContext _context;

        public DoanhThuNamsApiController(BookStoreContext context)
        {
            _context = context;
        }

        // GET: api/DoanhThuNamsApi
        [HttpGet]
        public async Task<ActionResult<IEnumerable<DoanhThuNam>>> GetDoanhThuNams()
        {
            return await _context.DoanhThuNams.ToListAsync();
        }

        // GET: api/DoanhThuNamsApi/5
        [HttpGet("{id}")]
        public async Task<ActionResult<DoanhThuNam>> GetDoanhThuNam(string id)
        {
            var doanhThuNam = await _context.DoanhThuNams.FindAsync(id);

            if (doanhThuNam == null)
            {
                return NotFound();
            }

            return doanhThuNam;
        }

        // PUT: api/DoanhThuNamsApi/5
        [HttpPut("{id}")]
        public async Task<IActionResult> PutDoanhThuNam(string id, DoanhThuNam doanhThuNam)
        {
            if (id != doanhThuNam.DoanhThuId)
            {
                return BadRequest();
            }

            _context.Entry(doanhThuNam).State = EntityState.Modified;

            try
            {
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!DoanhThuNamExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return NoContent();
        }

        // POST: api/DoanhThuNamsApi
        [HttpPost]
        public async Task<ActionResult<DoanhThuNam>> PostDoanhThuNam(DoanhThuNam doanhThuNam)
        {
            _context.DoanhThuNams.Add(doanhThuNam);
            await _context.SaveChangesAsync();

            return CreatedAtAction("GetDoanhThuNam", new { id = doanhThuNam.DoanhThuId }, doanhThuNam);
        }

        // DELETE: api/DoanhThuNamsApi/5
        [HttpDelete("{id}")]
        public async Task<ActionResult<DoanhThuNam>> DeleteDoanhThuNam(string id)
        {
            var doanhThuNam = await _context.DoanhThuNams.FindAsync(id);
            if (doanhThuNam == null)
            {
                return NotFound();
            }

            _context.DoanhThuNams.Remove(doanhThuNam);
            await _context.SaveChangesAsync();

            return doanhThuNam;
        }

        private bool DoanhThuNamExists(string id)
        {
            return _context.DoanhThuNams.Any(e => e.DoanhThuId == id);
        }
    }
}
