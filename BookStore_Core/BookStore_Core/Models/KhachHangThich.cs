﻿using System.ComponentModel.DataAnnotations;

namespace BookStore_Core.Models
{
    public class KhachHangThich
    {
        [Key]
        public int ThichId { get; set; }
        public string SachId { get; set; }
        public string CustomerId { get; set; }
        public virtual Sach Sach { get; set; }
        public virtual Customer Customer { get; set; }
    }
}
