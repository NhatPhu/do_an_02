﻿using System;
using System.ComponentModel.DataAnnotations;

namespace BookStore_Core.Models
{
    public class Manager
    {
        [Key]
        public string ManagerId { get; set; }
        public string ManagerName { get; set; }
        public string Phone { get; set; }
        public string Address { get; set; }
        public string Email { get; set; }
        public DateTime BirthDay { get; set; }
        public DateTime LastUpdate { get; set; }
        public bool IsDel { get; set; }
        //[NotMapped]
        //public List<BookUpdate> BookUpdate { get; set; }
    }
}
