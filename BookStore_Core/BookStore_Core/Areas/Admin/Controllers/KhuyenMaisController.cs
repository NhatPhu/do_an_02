﻿using BookStore_Core.DAL;
using BookStore_Core.Models;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace BookStore_Core.Areas.Admin.Controllers
{
    [Area("Admin")]
    [Route("Admin/KhuyenMais")]
    public class KhuyenMaisController : Controller
    {
        private readonly BookStoreContext _context;

        public KhuyenMaisController(BookStoreContext context)
        {
            _context = context;
        }

        [Route("Index")]
        // GET: Admin/KhuyenMais
        public async Task<IActionResult> Index()
        {
            return View(await _context.KhuyenMais.ToListAsync());
        }
        [Route("Details/{id}")]
        // GET: Admin/KhuyenMais/Details/5
        public async Task<IActionResult> Details(string id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var khuyenMai = await _context.KhuyenMais
                .FirstOrDefaultAsync(m => m.MaKhuyenMaiId == id);
            if (khuyenMai == null)
            {
                return NotFound();
            }

            return View(khuyenMai);
        }
        [Route("Create/{id}")]
        // GET: Admin/KhuyenMais/Create
        public IActionResult Create(string id)
        {
            BangGia gia = _context.BangGias.Include(p => p.Sach).Include(p => p.KhuyenMai).FirstOrDefault(p => p.SachId == id && p.IsDel == false);
            var km = _context.KhuyenMais.Where(p => p.TrangThai == true);
            ViewData["KhuyenMai"] = new SelectList(km, "MaKhuyenMaiId", "TenKhuyenMai");
            return View(gia);
        }

        // POST: Admin/KhuyenMais/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [Route("Create/{id}")]
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create(BangGia bangGia)
        {
            if (ModelState.IsValid)
            {
                var giaCu = _context.BangGias.FirstOrDefault(p => p.BangGiaId == bangGia.BangGiaId);
                giaCu.IsDel = true;
                _context.Update(giaCu);
                await _context.SaveChangesAsync();
                _context.BangGias.Add(new BangGia() { SachId = giaCu.SachId, NgayCoHieuLuc = DateTime.Now, DonGia = giaCu.DonGia, PhanTramKhuyenMai = bangGia.PhanTramKhuyenMai, MaKhuyenMaiId = bangGia.MaKhuyenMaiId });
                await _context.SaveChangesAsync();
                return RedirectToAction(nameof(ApDungKhuyenMai));
            }
            var km = _context.KhuyenMais.Where(p => p.TrangThai == true);
            ViewData["KhuyenMai"] = new SelectList(km, "MaKhuyenMaiId", "TenKhuyenMai", bangGia.KhuyenMai.MaKhuyenMaiId);
            return View(bangGia);
        }
        [Route("Edit/{id}")]
        // GET: Admin/KhuyenMais/Edit/5
        public async Task<IActionResult> Edit(string id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var khuyenMai = await _context.KhuyenMais.FindAsync(id);
            if (khuyenMai == null)
            {
                return NotFound();
            }
            return View(khuyenMai);
        }
        [Route("Edit/{id}")]
        // POST: Admin/KhuyenMais/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(string id, [Bind("MaKhuyenMaiId,NgayBatDau,NgayKetThuc,TenKhuyenMai,HoaDonToiThieu,SoLuongSpCungLoai,SoLuongSpKhacLoai,HinhAnh,TrangThai")] KhuyenMai khuyenMai)
        {
            if (id != khuyenMai.MaKhuyenMaiId)
            {
                return NotFound();
            }

            if (ModelState.IsValid)
            {
                try
                {
                    _context.Update(khuyenMai);
                    await _context.SaveChangesAsync();
                }
                catch (DbUpdateConcurrencyException)
                {
                    if (!KhuyenMaiExists(khuyenMai.MaKhuyenMaiId))
                    {
                        return NotFound();
                    }
                    else
                    {
                        throw;
                    }
                }
                return RedirectToAction(nameof(Index));
            }
            return View(khuyenMai);
        }
        [Route("Delete/{id}")]
        // GET: Admin/KhuyenMais/Delete/5
        public async Task<IActionResult> Delete(string id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var khuyenMai = await _context.KhuyenMais
                .FirstOrDefaultAsync(m => m.MaKhuyenMaiId == id);
            if (khuyenMai == null)
            {
                return NotFound();
            }

            return View(khuyenMai);
        }
        [Route("Delete/{i}")]
        // POST: Admin/KhuyenMais/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> DeleteConfirmed(string id)
        {
            var khuyenMai = await _context.KhuyenMais.FindAsync(id);
            _context.KhuyenMais.Remove(khuyenMai);
            await _context.SaveChangesAsync();
            return RedirectToAction(nameof(Index));
        }

        [Route("ApDungKhuyenMai")]
        public IActionResult ApDungKhuyenMai()
        {
            var sachs = _context.Saches.Where(p => p.IsDel == false);
            List<SachViewModel> sachsVM = new List<SachViewModel>();
            foreach (var x in sachs)
            {
                var gia = _context.BangGias.Include(p => p.KhuyenMai).FirstOrDefault(p => p.SachId == x.SachId && p.IsDel == false);
                sachsVM.Add(new SachViewModel() { Sach = x, BangGia = gia });
            }
            return View(sachsVM);
        }
        [Route("TaoKhuyenMai")]
        public IActionResult TaoKhuyenMai()
        {
            return View();
        }
        [HttpPost]
        [Route("TaoKhuyenMai")]
        public IActionResult TaoKhuyenMai(KhuyenMai km, string id)
        {
            if (ModelState.IsValid)
            {
                int dem = _context.KhuyenMais.Count();
                string tam = "KM" + dem;
                _context.KhuyenMais.Add(new KhuyenMai() { MaKhuyenMaiId = tam, NgayBatDau = km.NgayBatDau, NgayKetThuc = km.NgayKetThuc, TenKhuyenMai = km.TenKhuyenMai, TrangThai = true });
                _context.SaveChanges();
                return RedirectToAction(nameof(Index));
            }
            return View(km);
        }
        private bool KhuyenMaiExists(string id)
        {
            return _context.KhuyenMais.Any(e => e.MaKhuyenMaiId == id);
        }
    }
}
