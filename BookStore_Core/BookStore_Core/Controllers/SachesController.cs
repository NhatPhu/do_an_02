﻿using BookStore_Core.DAL;
using BookStore_Core.Models;
using BookStore_Core.ViewModel;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace BookStore_Core.Controllers
{
    public class SachesController : Controller
    {
        private readonly BookStoreContext _context;

        public SachesController(BookStoreContext context)
        {
            _context = context;
        }
        // GET: Saches/Details/5
        public async Task<IActionResult> Details(string id)
        {
            //Dem so luot xem cua sach
            var sach = await _context.Saches
                .Include(s => s.LoaiSachCuThe)
                .Include(s => s.TacGia)
                .FirstOrDefaultAsync(m => m.SachId == id);
            sach.LuotXem++;
            _context.Saches.Update(sach);
            _context.SaveChanges();

            //Nếu ng dùng nặc danh thì icon like luôn là unlike
            if (HttpContext.Session.GetString("UserName") == null || HttpContext.Session.GetString("UserName") == "" || HttpContext.Session.GetString("RoleId") == "1" || HttpContext.Session.GetString("RokeId") == "3")
            {
                ViewBag.SachDcThich = false;
            }
            else
            {
                var cusId = HttpContext.Session.GetString("UserName").ToString();
                var user = _context.Customers.Where(p => p.CustomerEmail == cusId).SingleOrDefault();
                //Lưu lại lượt thích để hiển thị lên View
                var thich = _context.KhachHangThiches.FirstOrDefault(p => p.SachId == id && p.CustomerId == user.CustomerId);
                if (thich == null)
                {
                    ViewBag.SachDcThich = false;
                }
                else ViewBag.SachDcThich = true;
            }
            if (id == null)
            {
                return NotFound();
            }

            //Tìm sách tương ứng với id
            
            if (sach == null)
            {
                return NotFound();
            }
            var bangGia = _context.BangGias.Where(p => p.SachId == sach.SachId).Last();
            var anhPhu = _context.AnhPhus.Where(p => p.SachId == id).ToList();

            //Tạo viewModel để đổ dữ liệu lên View
            SachViewModel sachVM = new SachViewModel() { Sach = sach, BangGia = bangGia, SachDuocThich = new SachDuocThich(), AnhPhus = anhPhu };

            //Tìm xem thể loại tổng quát của sách là gì
            var theloaiTQ = _context.LoaiSachTongQuats.FirstOrDefault(p => p.LoaiSachTQId == sach.LoaiSachCuThe.LoaiSachTQId);
            ViewBag.TheLoaiTongQuat = theloaiTQ;

            //Tìm ds thể loại cụ thể bên trong thể loại tổng quát
            var listTheLoaiCT = _context.LoaiSachCuThes.Where(p => p.LoaiSachTQId == theloaiTQ.LoaiSachTQId).ToList();
            ViewBag.DSTheLoaiCT = listTheLoaiCT;

            //Tạo list tạo để lưu số lượng sách ứng với mỗi thể loại
            List<int> soluongSP = new List<int>();
            foreach (var x in listTheLoaiCT)
            {
                soluongSP.Add(_context.Saches.Where(p => p.LoaiSachCTId == x.LoaiSachCTId).Count());
            }
            //Lưu số lượng sách tương ứng với mỗi thể loại để hiện lên view
            ViewBag.SoLuongSP = soluongSP;

            var comment = _context.SachDuocThiches.Include(m => m.Sach).Include(m => m.Customer).Where(p => p.SachId == sach.SachId).OrderByDescending(p => p.ThichId).ToList();
            ViewBag.Comment = comment;
            List<SachViewModel> sachsVM = new List<SachViewModel>();
            var theLoai = _context.Saches.Where(p => p.LoaiSachCTId == sach.LoaiSachCTId && p.SachId != sach.SachId).ToList();
            foreach (var x in theLoai)
            {
                var trangthai = "";
                var Gia = _context.BangGias.Include(p => p.KhuyenMai).FirstOrDefault(p => p.SachId == x.SachId && p.IsDel == false);
                if (DateTime.Now.Year == x.NgayTao.Year && DateTime.Now.Month - x.NgayTao.Month < 2)
                {
                    trangthai = "Mới";
                }
                sachsVM.Add(new SachViewModel() { Sach = x, BangGia = Gia, TrangThai = trangthai });
            }
            ViewBag.CungLoai = sachsVM;
            return View(sachVM);
        }

        public IActionResult HienThiTheoTheLoai(string id, int? page)
        {
            if (id == null)
            {
                return NotFound();
            }
            var theLoai = _context.LoaiSachCuThes.Where(p => p.LoaiSachTQId == id);
            List<SachViewModel> sachsVM = new List<SachViewModel>();
            foreach (var x in theLoai)
            {
                var sach = _context.Saches.Include(s => s.LoaiSachCuThe).Where(p => p.LoaiSachCTId == x.LoaiSachCTId);
                foreach (var y in sach)
                {
                    var trangthai = "";
                    var bangGia = _context.BangGias.Include(p => p.KhuyenMai).FirstOrDefault(p => p.SachId == y.SachId && p.IsDel == false);
                    if (DateTime.Now.Year == y.NgayTao.Year && DateTime.Now.Month - y.NgayTao.Month < 2)
                    {
                        trangthai = "Mới";
                    }
                    sachsVM.Add(new SachViewModel() { Sach = y, BangGia = bangGia, TrangThai = trangthai });
                }
            }
            var listTheLoaiCT = _context.LoaiSachCuThes.Where(p => p.LoaiSachTQId == id);
            ViewBag.TheLoaiCuThe = listTheLoaiCT.ToList();
            ViewBag.TenTheLoaiTongQuat = _context.LoaiSachTongQuats.FirstOrDefault(p => p.LoaiSachTQId == id).TenLoaiSachTQ;
            List<int> soluongSP = new List<int>();
            foreach (var x in listTheLoaiCT)
            {
                soluongSP.Add(_context.Saches.Where(p => p.LoaiSachCTId == x.LoaiSachCTId).Count());
            }
            ViewBag.SoLuongSP = soluongSP;
            ViewBag.Trang = page;
            if (page == null)
            {
                ViewBag.Trang = 1;
            }
            int pageSize = 9;
            if (sachsVM.Count() % pageSize != 0)
            {
                ViewBag.Max = sachsVM.Count() / pageSize + 1;
            }
            return View(PaginatedList<SachViewModel>.Create(sachsVM.AsQueryable(), page ?? 1, pageSize));
        }
        public IActionResult HienThiSachCuThe(string id, int? page)
        {
            if (id == null)
            {
                return NotFound();
            }
            var theLoai = _context.LoaiSachCuThes.FirstOrDefault(p => p.LoaiSachCTId == id);
            List<SachViewModel> sachsVM = new List<SachViewModel>();
            var sach = _context.Saches.Include(s => s.LoaiSachCuThe).Where(p => p.LoaiSachCTId == id);
            foreach (var y in sach)
            {
                var trangthai = "";
                var bangGia = _context.BangGias.Include(p => p.KhuyenMai).FirstOrDefault(p => p.SachId == y.SachId && p.IsDel == false);
                if (DateTime.Now.Year == y.NgayTao.Year && DateTime.Now.Month - y.NgayTao.Month < 2)
                {
                    trangthai = "Mới";
                }
                sachsVM.Add(new SachViewModel() { Sach = y, BangGia = bangGia, TrangThai = trangthai });
            }

            var listTheLoaiCT = _context.LoaiSachCuThes.Where(p => p.LoaiSachTQId == theLoai.LoaiSachTQId);
            ViewBag.TheLoaiCuThe = listTheLoaiCT.ToList();
            ViewBag.TenTheLoaiTongQuat = _context.LoaiSachTongQuats.FirstOrDefault(p => p.LoaiSachTQId == theLoai.LoaiSachTQId).TenLoaiSachTQ;
            List<int> soluongSP = new List<int>();
            foreach (var x in listTheLoaiCT)
            {
                soluongSP.Add(_context.Saches.Where(p => p.LoaiSachCTId == x.LoaiSachCTId).Count());
            }
            ViewBag.SoLuongSP = soluongSP;
            ViewBag.Trang = page;
            if (page == null)
            {
                ViewBag.Trang = 1;
            }
            int pageSize = 9;
            if (sachsVM.Count() % pageSize != 0)
            {
                ViewBag.Max = sachsVM.Count() / pageSize + 1;
            }
            return View(PaginatedList<SachViewModel>.Create(sachsVM.AsQueryable(), page ?? 1, pageSize));
        }

        public IActionResult Thich(string id)
        {
            if (HttpContext.Session.GetString("UserName") == null || HttpContext.Session.GetString("UserName") == "")
            {
                HttpContext.Session.SetString("IdSach", id);
                return RedirectToAction("Login", "Account", new { id = id });
            }
            if (HttpContext.Session.GetString("RoleId").ToString() == "1" || HttpContext.Session.GetString("RoleId").ToString() == "3")
            {
                return RedirectToAction("Details", "Saches", new { id = id });
            }
            var sach = _context.Saches.SingleOrDefault(p => p.SachId == id);
            if (sach == null)
            {
                return NotFound();
            }
            var cusId = HttpContext.Session.GetString("UserName").ToString();
            var user = _context.Customers.Where(p => p.CustomerEmail == cusId).SingleOrDefault();
            var thich = _context.KhachHangThiches.FirstOrDefault(p => p.SachId == id && p.CustomerId == user.CustomerId);
            if (thich == null)
            {
                sach.LuotThich++;
                _context.Update(sach);
                _context.SaveChanges();
                _context.KhachHangThiches.Add(new KhachHangThich() { SachId = id, CustomerId = user.CustomerId });
                _context.SaveChanges();
                return RedirectToAction("Details", "Saches", new { id = id, isLike = true });
            }
            else
            {
                sach.LuotThich--;
                _context.Update(sach);
                _context.SaveChanges();
                _context.Remove(thich);
                _context.SaveChanges();
                return RedirectToAction("Details", "Saches", new { id = id });
            }

        }
        public IActionResult Timtheoten2(string searchName)
        {
            return RedirectToAction("TimTheoTen", "Saches", new { searchName = searchName });
        }
        public IActionResult TimTheoTen(string searchName, int? page)
        {

            List<SachViewModel> sachsVM = new List<SachViewModel>();
            if (!String.IsNullOrEmpty(searchName))
            {
                ViewBag.search = searchName;
                var sachs = _context.Saches.Include(p => p.TacGia).Include(p => p.LoaiSachCuThe).Where(s => s.TenSach.ToLower().Contains(searchName.ToLower())).ToList();
                var theloai = _context.LoaiSachCuThes.Include(p => p.LoaiSachTongQuat).Where(s => s.TenLoaiSachCT.ToLower().Contains(searchName.ToLower())).ToList();

                if (sachs != null && sachs.Count > 0)
                {
                    foreach (var x in sachs)
                    {
                        var gia = _context.BangGias.Include(p => p.KhuyenMai).FirstOrDefault(p => p.SachId == x.SachId && p.IsDel == false);
                        sachsVM.Add(new SachViewModel() { BangGia = gia, Sach = x });
                    }
                }

                if (theloai != null && theloai.Count > 0)
                {
                    foreach (var tl in theloai)
                    {
                        var bookInCategorySearch = _context.Saches.Include(p => p.TacGia).Include(p => p.LoaiSachCuThe).Where(p => p.LoaiSachCTId == tl.LoaiSachCTId).ToList();

                        if (sachs == null || sachs.Count == 0)
                        {
                            foreach (var x in bookInCategorySearch)
                            {
                                var gia = _context.BangGias.Include(p => p.KhuyenMai).FirstOrDefault(p => p.SachId == x.SachId && p.IsDel == false);
                                sachsVM.Add(new SachViewModel() { BangGia = gia, Sach = x });
                            }
                        }
                        else
                        {
                            foreach (var x in bookInCategorySearch)
                            {
                                bool isBookExit = false;
                                foreach (var k in sachs)
                                {
                                    if (x.SachId == k.SachId)
                                    {
                                        isBookExit = true;
                                        break;

                                    }
                                }
                                if (isBookExit == false)
                                {
                                    var gia = _context.BangGias.Include(p => p.KhuyenMai).FirstOrDefault(p => p.SachId == x.SachId && p.IsDel == false);
                                    sachsVM.Add(new SachViewModel() { BangGia = gia, Sach = x });
                                }
                            }
                        }
                    }
                }

            }
            //var listTheLoaiCT = _context.LoaiSachCuThes.Where(p => p.LoaiSachTQId == id);
            //ViewBag.TheLoaiCuThe = listTheLoaiCT.ToList();
            //ViewBag.TenTheLoaiTongQuat = _context.LoaiSachTongQuats.FirstOrDefault(p => p.LoaiSachTQId == id).TenLoaiSachTQ;
            //List<int> soluongSP = new List<int>();
            //foreach (var x in listTheLoaiCT)
            //{
            //    soluongSP.Add(_context.Saches.Where(p => p.LoaiSachCTId == x.LoaiSachCTId).Count());
            //}
            //ViewBag.SoLuongSP = soluongSP;
            ViewBag.StringSearch = searchName;
            ViewBag.Trang = page;
            if (page == null)
            {
                ViewBag.Trang = 1;
            }
            int pageSize = 9;
            if (sachsVM.Count() % pageSize != 0)
            {
                ViewBag.Max = sachsVM.Count() / pageSize + 1;
            }
            return View(PaginatedList<SachViewModel>.Create(sachsVM.AsQueryable(), page ?? 1, pageSize));
        }

        public IActionResult HienThiTatCaTacGia(int? page)
        {
            List<SachViewModel> sachsVM = new List<SachViewModel>();
            var sachs = _context.Saches;
            foreach (var x in sachs)
            {
                var gia = _context.BangGias.Include(p => p.KhuyenMai).FirstOrDefault(p => p.SachId == x.SachId && p.IsDel == false);
                sachsVM.Add(new SachViewModel() { BangGia = gia, Sach = x });
            }
            var listTacGia = _context.TacGias;
            ViewBag.TacGia = listTacGia.ToList();
            //ViewBag.TenTheLoaiTongQuat = _context.LoaiSachTongQuats.FirstOrDefault(p => p.LoaiSachTQId == id).TenLoaiSachTQ;
            List<int> soluongSP = new List<int>();
            foreach (var x in listTacGia)
            {
                soluongSP.Add(_context.Saches.Where(p => p.TacGiaId == x.TacGiaId).Count());
            }
            ViewBag.SoLuongSP = soluongSP;

            ViewBag.Trang = page;
            if (page == null)
            {
                ViewBag.Trang = 1;
            }
            int pageSize = 9;
            if (sachsVM.Count() % pageSize != 0)
            {
                ViewBag.Max = sachsVM.Count() / pageSize + 1;
            }
            return View(PaginatedList<SachViewModel>.Create(sachsVM.AsQueryable(), page ?? 1, pageSize));
        }

        public IActionResult HienThiTheoTacGia(string id, int? page)
        {
            List<SachViewModel> sachsVM = new List<SachViewModel>();
            var sachs = _context.Saches.Where(p => p.TacGiaId == id);
            foreach (var x in sachs)
            {
                var gia = _context.BangGias.Include(p => p.KhuyenMai).FirstOrDefault(p => p.SachId == x.SachId && p.IsDel == false);
                sachsVM.Add(new SachViewModel() { BangGia = gia, Sach = x });
            }
            var tacgia = _context.TacGias.FirstOrDefault(p => p.TacGiaId == id);
            var listTacGia = _context.TacGias;
            ViewBag.TacGia = listTacGia.ToList();
            ViewBag.TenTacGia = _context.TacGias.SingleOrDefault(p => p.TacGiaId == id).TenTacGia;
            ViewBag.TenTheLoaiTacGia = _context.LoaiTacGiaTongQuats.FirstOrDefault(p => p.LoaiTacGiaTQId == tacgia.LoaiTacGiaTQId).TenLoaiTacGiaTQ;
            List<int> soluongSP = new List<int>();
            foreach (var x in listTacGia)
            {
                soluongSP.Add(_context.Saches.Where(p => p.TacGiaId == x.TacGiaId).Count());
            }
            ViewBag.SoLuongSP = soluongSP;

            ViewBag.Trang = page;
            if (page == null)
            {
                ViewBag.Trang = 1;
            }
            int pageSize = 9;
            if (sachsVM.Count() % pageSize != 0)
            {
                ViewBag.Max = sachsVM.Count() / pageSize + 1;
            }
            return View(PaginatedList<SachViewModel>.Create(sachsVM.AsQueryable(), page ?? 1, pageSize));
        }
        private bool SachExists(string id)
        {
            return _context.Saches.Any(e => e.SachId == id);
        }
    }


}
