﻿using BookStore_Core.Models;
using Microsoft.AspNetCore.Http;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace BookStore_Core.ViewModel
{
    public class ThemSachViewModel
    {
        public Sach Sach { get; set; }
        public BangGia BangGia { get; set; }
        public List<AnhPhu> AnhPhus { get; set; }
        [Required(ErrorMessage = "bắt buộc thêm hình ảnh")]
        public IFormFile file { get; set; }
    }
}
