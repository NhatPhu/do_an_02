﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace BookStore_Core.Models
{
    public class LoaiTacGiaTQ
    {
        [Key]
        public int LoaiTacGiaTQId { get; set; }
        public string TenLoaiTacGiaTQ { get; set; }
        [NotMapped]
        public List<TacGia> TacGias { get; set; }

    }
}
