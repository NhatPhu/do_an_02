﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace BookStore_Core.Models
{
    public class LoaiSachTongQuat
    {
        [Key]
        [Display(Name = "Id")]
        public string LoaiSachTQId { get; set; }
        [Display(Name = "Thể loại tổng quát")]
        public string TenLoaiSachTQ { get; set; }
        [NotMapped]
        public List<LoaiSachCuThe> LoaiSachCuThes { get; set; }
    }
}
