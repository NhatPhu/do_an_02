﻿using System.ComponentModel.DataAnnotations;

namespace BookStore_Core.Models
{
    public class AnhPhu
    {
        [Key]
        public int IdAnh { get; set; }
        public string HinhAnh { get; set; }
        public string SachId { get; set; }
        public virtual Sach Sach { get; set; }
    }
}
