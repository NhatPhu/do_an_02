﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using BookStore_Core.DAL;
using BookStore_Core.Models;

namespace BookStore_Core.Areas.Admin.APIs
{
    [Route("api/[controller]")]
    [ApiController]
    public class DoanhThuThangsApiController : ControllerBase
    {
        private readonly BookStoreContext _context;

        public DoanhThuThangsApiController(BookStoreContext context)
        {
            _context = context;
        }

        // GET: api/DoanhThuThangsApi
        [HttpGet]
        public async Task<ActionResult<IEnumerable<DoanhThuThang>>> GetDoanhThuThangs()
        {
            return await _context.DoanhThuThangs.ToListAsync();
        }

        // GET: api/DoanhThuThangsApi/5
        [HttpGet("{id}")]
        public async Task<ActionResult<DoanhThuThang>> GetDoanhThuThang(DateTime id)
        {
            var doanhThuThang = await _context.DoanhThuThangs.FindAsync(id);

            if (doanhThuThang == null)
            {
                return NotFound();
            }

            return doanhThuThang;
        }

        // PUT: api/DoanhThuThangsApi/5
        [HttpPut("{id}")]
        public async Task<IActionResult> PutDoanhThuThang(DateTime id, DoanhThuThang doanhThuThang)
        {
            if (id != doanhThuThang.DoanhThuId)
            {
                return BadRequest();
            }

            _context.Entry(doanhThuThang).State = EntityState.Modified;

            try
            {
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!DoanhThuThangExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return NoContent();
        }

        // POST: api/DoanhThuThangsApi
        [HttpPost]
        public async Task<ActionResult<DoanhThuThang>> PostDoanhThuThang(DoanhThuThang doanhThuThang)
        {
            _context.DoanhThuThangs.Add(doanhThuThang);
            try
            {
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateException)
            {
                if (DoanhThuThangExists(doanhThuThang.DoanhThuId))
                {
                    return Conflict();
                }
                else
                {
                    throw;
                }
            }

            return CreatedAtAction("GetDoanhThuThang", new { id = doanhThuThang.DoanhThuId }, doanhThuThang);
        }

        // DELETE: api/DoanhThuThangsApi/5
        [HttpDelete("{id}")]
        public async Task<ActionResult<DoanhThuThang>> DeleteDoanhThuThang(DateTime id)
        {
            var doanhThuThang = await _context.DoanhThuThangs.FindAsync(id);
            if (doanhThuThang == null)
            {
                return NotFound();
            }

            _context.DoanhThuThangs.Remove(doanhThuThang);
            await _context.SaveChangesAsync();

            return doanhThuThang;
        }

        private bool DoanhThuThangExists(DateTime id)
        {
            return _context.DoanhThuThangs.Any(e => e.DoanhThuId == id);
        }
    }
}
