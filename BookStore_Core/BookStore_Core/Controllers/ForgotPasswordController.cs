﻿using BookStore_Core.DAL;
using BookStore_Core.ViewModel;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Linq;
using System.Net;
using System.Net.Mail;

namespace BookStore_Core.Controllers
{
    public class ForgotPasswordController : Controller
    {
        private readonly BookStoreContext _db;
        public ForgotPasswordController(BookStoreContext db)
        {
            _db = db;
        }

        public IActionResult ForgotPassword()
        {
            if (HttpContext.Session.GetString("UserName") == null)
                return View();
            return RedirectToAction("Login", "Account", new { area = "" });
        }

        [HttpPost]
        public IActionResult ForgotPassword(string Email)
        {
            var account = _db.Logins.Where(a => a.LoginName == Email && a.IsDel == false).FirstOrDefault();
            if (account != null)
            {
                string resetCode = Guid.NewGuid().ToString();
                SendVerificationLinkEmail(account.LoginName, resetCode, "ResetPassword");
                account.ResetPassword = resetCode;
                _db.Update(account);
                _db.ChangeTracker.AutoDetectChangesEnabled = false;
                _db.SaveChanges();
            }
            return RedirectToAction("Login", "Account");
        }

        public IActionResult ResetPassword(string id)
        {
            var user = _db.Logins.Where(a => a.ResetPassword == id).FirstOrDefault();
            if (user != null)
            {
                ResetPasswordViewModel model = new ResetPasswordViewModel();
                model.ResetCode = id;
                return View(model);
            }
            else
            {
                return NotFound();
            }
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public IActionResult ResetPassword(ResetPasswordViewModel model)
        {
            var message = "";
            if (ModelState.IsValid)
            {
                var user = _db.Logins.Where(a => a.ResetPassword == model.ResetCode).FirstOrDefault();
                if (user != null)
                {
                    user.Password = model.NewPassword;
                    user.ResetPassword = "";
                    _db.Update(user);
                    _db.ChangeTracker.AutoDetectChangesEnabled = false;
                    _db.SaveChanges();
                    message = "Success!";
                }
            }
            else
            {
                message = "!!!";
            }
            ViewBag.Message = message;
            return View(model);
        }

        [NonAction]
        public void SendVerificationLinkEmail(string email, string activationCode, string emailFor)
        {
            var uriBuilder = new UriBuilder
            {
                Scheme = Request.Scheme,
                Host = Request.Host.Host,
                Port = Request.Host.Port.GetValueOrDefault(80),
                Path = $"/ForgotPassword/{emailFor}/{activationCode}"
            };
            var link = uriBuilder.Uri.AbsoluteUri;
            var fromEmail = new MailAddress("phulajpro@gmail.com", "phu dep trai");
            var toEmail = new MailAddress(email);
            var fromEmailPassword = "haydoido1";

            string subject = "";
            string body = "";
            if (emailFor == "VerifyAccount")
            {
                subject = "Tài khoản của bạn đã được tạo. Vui lòng kích hoạt tại link kèm theo!";
                body = "<br/><br/>Chúng tôi vui lòng thông báo tài khoản của bạn" +
                    " đã được tạo thành công. Vui lòng click vào link bên dưới để xác nhận" +
                    " <br/><br/><a href='" + link + "'>" + link + "</a> ";
            }
            else if (emailFor == "ResetPassword")
            {
                subject = "Reset Password";
                body = "<br/><br/>Chúng tôi nhận được thông báo yêu cầu cấp lại mật khẩu. Vui lòng click vào đường lên bên dưới để tiến hành tạo lại mật khẩu mới." +
                    "<br/><br/><a href=" + link + ">Reset Password link</a>";
            }

            var smtp = new SmtpClient
            {
                Host = "smtp.gmail.com",
                Port = 587,
                EnableSsl = true,
                DeliveryMethod = SmtpDeliveryMethod.Network,
                UseDefaultCredentials = false,
                Credentials = new NetworkCredential(fromEmail.Address, fromEmailPassword)
            };
            using (var message = new MailMessage(fromEmail, toEmail)
            {
                Subject = subject,
                Body = body,
                IsBodyHtml = true
            })
                smtp.Send(message);
        }
    }
}