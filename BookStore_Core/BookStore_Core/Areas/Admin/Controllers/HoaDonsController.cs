﻿using BookStore_Core.DAL;
using BookStore_Core.Models;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using OfficeOpenXml;
using System;
using System.Drawing;
using System.Linq;
using System.Threading.Tasks;

namespace BookStore_Core.Areas.Admin.Controllers
{
    [Area("Admin")]
    [Route("Admin/HoaDons")]
    public class HoaDonsController : Controller
    {
        private readonly BookStoreContext _context;

        public HoaDonsController(BookStoreContext context)
        {
            _context = context;
        }
        [Route("index")]
        // GET: Admin/HoaDons
        public IActionResult Index()
        {
            var hoaDon = _context.HoaDons.Include(m => m.Customer);
            return View(hoaDon);
        }
        [Route("Details")]
        // GET: Admin/HoaDons/Details/5
        public async Task<IActionResult> Details(string id)
        {
            if (id == null)
            {
                return NotFound();
            }
            var chiTietHoaDon = await _context.ChiTietHoaDons.Include(p => p.Sach).Include(p => p.HoaDon).Where(p => p.HoaDonId.ToString("ddMMyyyyHHMMss") == id).ToListAsync();
            return View(chiTietHoaDon);
        }
        [Route("XacNhan")]
        // GET: Admin/HoaDons/Create
        public IActionResult XacNhan(string id)
        {
            var hoaDon = _context.HoaDons.FirstOrDefault(p => p.HoaDonId.ToString("ddMMyyyHHMMss") == id);
            var cthd = _context.ChiTietHoaDons.Include(p => p.Sach).Where(p => p.HoaDonId == hoaDon.HoaDonId);
            hoaDon.XacNhan = true;
            hoaDon.TrangThai = 0;
            _context.Update(hoaDon);
            _context.SaveChanges();

            return RedirectToAction(nameof(Index));
        }
        [Route("XacNhanDaGiao")]
        public IActionResult XacNhanDaGiao(string id)
        {
            var hoaDon = _context.HoaDons.FirstOrDefault(p => p.HoaDonId.ToString("ddMMyyyHHMMss") == id);
            var cthd = _context.ChiTietHoaDons.Include(p => p.Sach).Where(p => p.HoaDonId == hoaDon.HoaDonId);
            //hoaDon.XacNhan = true;
            hoaDon.TrangThai = 1;
            _context.Update(hoaDon);
            _context.SaveChanges();
            int[] a = new int[8];
            foreach (var x in cthd)
            {
                var cttl = _context.LoaiSachCuThes.Include(p => p.LoaiSachTongQuat).FirstOrDefault(p => p.LoaiSachCTId == x.Sach.LoaiSachCTId);
                if (cttl.LoaiSachTongQuat.LoaiSachTQId == "1")
                {
                    a[0] += x.SoLuong;
                }
                else if (cttl.LoaiSachTongQuat.LoaiSachTQId == "2")
                {
                    a[1] += x.SoLuong;
                }
                else if (cttl.LoaiSachTongQuat.LoaiSachTQId == "3")
                {
                    a[2] += x.SoLuong;
                }
                else if (cttl.LoaiSachTongQuat.LoaiSachTQId == "4")
                {
                    a[3] += x.SoLuong;
                }
                else if (cttl.LoaiSachTongQuat.LoaiSachTQId == "5")
                {
                    a[4] += x.SoLuong;
                }
                else if (cttl.LoaiSachTongQuat.LoaiSachTQId == "6")
                {
                    a[5] += x.SoLuong;
                }
                else if (cttl.LoaiSachTongQuat.LoaiSachTQId == "7")
                {
                    a[6] += x.SoLuong;
                }
                else
                {
                    a[7] += x.SoLuong;
                }
            }
            var DTNgay = _context.DoanhThuNgays.FirstOrDefault(p => p.DoanhThuId.Date == hoaDon.HoaDonId.Date);
            if (DTNgay == null)
            {
                _context.Add(new DoanhThuNgay() { DoanhThuId = hoaDon.HoaDonId.Date, TongTien = hoaDon.TongTien, VanHoc = a[0], KinhTe = a[1], TamLy_KyNang = a[2], NuoiCon = a[3], SachThieuNhi = a[4], TieuSu_HoiKy = a[5], SGK = a[6], SachNgoaiNgu = a[7] });
                _context.SaveChanges();
            }
            else
            {
                DTNgay.TongTien += hoaDon.TongTien;
                DTNgay.VanHoc += a[0];
                DTNgay.KinhTe += a[1];
                DTNgay.TamLy_KyNang += a[2];
                DTNgay.NuoiCon += a[3];
                DTNgay.SachThieuNhi += a[4];
                DTNgay.TieuSu_HoiKy += a[5];
                DTNgay.SGK += a[6];
                DTNgay.SachNgoaiNgu += a[7];
                _context.Update(DTNgay);
                _context.SaveChanges();
            }


            var DTThang = _context.DoanhThuThangs.FirstOrDefault(p => p.DoanhThuId.Month == hoaDon.HoaDonId.Month && p.DoanhThuId.Year == hoaDon.HoaDonId.Year);
            if (DTThang == null)
            {
                string thang = hoaDon.HoaDonId.Year + "-" + hoaDon.HoaDonId.Month + "-" + "01";
                _context.Add(new DoanhThuThang() { DoanhThuId = Convert.ToDateTime(thang), TongTien = hoaDon.TongTien, VanHoc = a[0], KinhTe = a[1], TamLy_KyNang = a[2], NuoiCon = a[3], SachThieuNhi = a[4], TieuSu_HoiKy = a[5], SGK = a[6], SachNgoaiNgu = a[7] });
                _context.SaveChanges();
            }
            else
            {
                DTThang.TongTien += hoaDon.TongTien;
                DTThang.VanHoc += a[0];
                DTThang.KinhTe += a[1];
                DTThang.TamLy_KyNang += a[2];
                DTThang.NuoiCon += a[3];
                DTThang.SachThieuNhi += a[4];
                DTThang.TieuSu_HoiKy += a[5];
                DTThang.SGK += a[6];
                DTThang.SachNgoaiNgu += a[7];
                _context.Update(DTThang);
                _context.SaveChanges();
            }

            var DTNam = _context.DoanhThuNams.FirstOrDefault(p => p.DoanhThuId == hoaDon.HoaDonId.Year.ToString());
            if (DTNam == null)
            {
                _context.Add(new DoanhThuNam() { DoanhThuId = hoaDon.HoaDonId.Year.ToString(), TongTien = hoaDon.TongTien, VanHoc = a[0], KinhTe = a[1], TamLy_KyNang = a[2], NuoiCon = a[3], SachThieuNhi = a[4], TieuSu_HoiKy = a[5], SGK = a[6], SachNgoaiNgu = a[7] });
                _context.SaveChanges();
            }
            else
            {
                DTNam.TongTien += hoaDon.TongTien;
                DTNam.VanHoc += a[0];
                DTNam.KinhTe += a[1];
                DTNam.TamLy_KyNang += a[2];
                DTNam.NuoiCon += a[3];
                DTNam.SachThieuNhi += a[4];
                DTNam.TieuSu_HoiKy += a[5];
                DTNam.SGK += a[6];
                DTNam.SachNgoaiNgu += a[7]; _context.Update(DTNam);
                _context.SaveChanges();
            }

            var cus = _context.Customers.FirstOrDefault(p => p.CustomerId == hoaDon.CustomerId);
            cus.DiemSo = hoaDon.TongTien / 1000;
            _context.Customers.Update(cus);
            _context.SaveChanges();
            return RedirectToAction(nameof(Index));
        }

        [Route("BaoCaoNgay")]
        public IActionResult BaoCaoNgay()
        {
            return View(_context.DoanhThuNgays);
        }
        [Route("BaoCaoThang")]
        public IActionResult BaoCaoThang()
        {

            return View(_context.DoanhThuThangs);
        }
        [Route("BaoCaoNam")]
        public IActionResult BaoCaoNam()
        {

            return View(_context.DoanhThuNams);
        }
        public async Task<IActionResult> Delete(DateTime? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var hoaDon = await _context.HoaDons
                .FirstOrDefaultAsync(m => m.HoaDonId == id);
            if (hoaDon == null)
            {
                return NotFound();
            }

            return View(hoaDon);
        }

        // POST: Admin/HoaDons/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> DeleteConfirmed(DateTime id)
        {
            var hoaDon = await _context.HoaDons.FindAsync(id);
            _context.HoaDons.Remove(hoaDon);
            await _context.SaveChangesAsync();
            return RedirectToAction(nameof(Index));
        }

        private bool HoaDonExists(DateTime id)
        {
            return _context.HoaDons.Any(e => e.HoaDonId == id);
        }
        [Route("XuatExcel")]
        public IActionResult XuatExcel(string id)
        {
            if (id == "Ngày")
            {
                return File(FomatExcel(id), "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet", "BaoCaoNgay.xlsx");

            }
            else if (id == "Tháng")
            {
                return File(FomatExcel(id), "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet", "BaoCaoThang.xlsx");

            }
            else return File(FomatExcel(id), "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet", "BaoCaoNam.xlsx");
        }

        public byte[] FomatExcel(string id)
        {
            string[] col_name = new string[] {
                "Ngày",
                "Văn học (Cuốn)",
                "Kinh tế (Cuốn)",
                "Tâm lý - Kỹ năng sống (Cuốn)",
                "Nuôi dạy con (Cuốn)",
                "Sách thiếu nhi (Cuốn)",
                "Tiểu sử - Hồi ký (Cuốn)",
                "Sách giáo khoa - Tham khảo (Cuốn",
                "Sách ngoại ngữ (cuốn)",
                "Tổng tiền"
            };
            //step 2: Create result byte array
            //byte[] result;
            //step 3: create a new package using memory safe structure
            using (var package = new ExcelPackage())
            {
                ExcelWorksheet worksheet = null;
                if (id == "Ngày")
                {
                    worksheet = package.Workbook.Worksheets.Add("BaoCaoNgay");
                    for (int i = 0; i < col_name.Length; i++)
                    {
                        worksheet.Cells[1, i + 1].Style.Font.Size = 14;
                        worksheet.Cells[1, i + 1].Value = col_name[i];
                        worksheet.Cells[1, i + 1].Style.Font.Bold = true;
                        //border the cell
                        worksheet.Cells[1, i + 1].Style.Border.BorderAround(OfficeOpenXml.Style.ExcelBorderStyle.Thin);
                        //set backgroud color fpr each cells
                        worksheet.Cells[1, i + 1].Style.Fill.PatternType = OfficeOpenXml.Style.ExcelFillStyle.Solid;
                        worksheet.Cells[1, i + 1].Style.Fill.BackgroundColor.SetColor(Color.FromArgb(255, 243, 214));
                    }

                    int row = 2;
                    //step 6: loop through query result and fill in cells
                    foreach (DoanhThuNgay item in _context.DoanhThuNgays.ToList())
                    {
                        for (int col = 1; col <= 12; col++)
                        {
                            worksheet.Cells[row, col].Style.Font.Size = 12;
                            worksheet.Cells[row, col].Style.Border.BorderAround(OfficeOpenXml.Style.ExcelBorderStyle.Thin);
                        }
                        //set data
                        worksheet.Cells[row, 1].Value = item.DoanhThuId.ToShortDateString();
                        worksheet.Cells[row, 2].Value = item.VanHoc;
                        worksheet.Cells[row, 3].Value = item.KinhTe;
                        worksheet.Cells[row, 4].Value = item.TamLy_KyNang;
                        worksheet.Cells[row, 5].Value = item.NuoiCon;
                        worksheet.Cells[row, 6].Value = item.SachThieuNhi;
                        worksheet.Cells[row, 7].Value = item.TieuSu_HoiKy;
                        worksheet.Cells[row, 8].Value = item.SGK;
                        worksheet.Cells[row, 9].Value = item.SachNgoaiNgu;
                        worksheet.Cells[row, 10].Value = item.TongTien;
                        //toggle backgroud color
                        //even row with ribbon style
                        if (row % 2 == 0)
                        {
                            for (int i = 1; i <= 12; i++)
                            {
                                worksheet.Cells[row, i].Style.Fill.PatternType = OfficeOpenXml.Style.ExcelFillStyle.Solid;
                                worksheet.Cells[row, i].Style.Fill.BackgroundColor.SetColor(Color.FromArgb(154, 211, 157));
                            }
                        }
                        row++;
                    }
                    //step 7 : auto fit colums
                    worksheet.Cells[worksheet.Dimension.Address].AutoFitColumns();
                    //step 8: convert the package as byte array
                    return package.GetAsByteArray();
                }
                else if (id == "Tháng")
                {
                    worksheet = package.Workbook.Worksheets.Add("BaoCaoThang");
                    for (int i = 0; i < col_name.Length; i++)
                    {
                        worksheet.Cells[1, i + 1].Style.Font.Size = 14;
                        worksheet.Cells[1, i + 1].Value = col_name[i];
                        worksheet.Cells[1, i + 1].Style.Font.Bold = true;
                        //border the cell
                        worksheet.Cells[1, i + 1].Style.Border.BorderAround(OfficeOpenXml.Style.ExcelBorderStyle.Thin);
                        //set backgroud color fpr each cells
                        worksheet.Cells[1, i + 1].Style.Fill.PatternType = OfficeOpenXml.Style.ExcelFillStyle.Solid;
                        worksheet.Cells[1, i + 1].Style.Fill.BackgroundColor.SetColor(Color.FromArgb(255, 243, 214));
                    }

                    int row = 2;
                    //step 6: loop through query result and fill in cells
                    foreach (DoanhThuThang item in _context.DoanhThuThangs.ToList())
                    {
                        for (int col = 1; col <= 12; col++)
                        {
                            worksheet.Cells[row, col].Style.Font.Size = 12;
                            worksheet.Cells[row, col].Style.Border.BorderAround(OfficeOpenXml.Style.ExcelBorderStyle.Thin);
                        }
                        //set data
                        worksheet.Cells[row, 1].Value = item.DoanhThuId.ToShortDateString();
                        worksheet.Cells[row, 2].Value = item.VanHoc;
                        worksheet.Cells[row, 3].Value = item.KinhTe;
                        worksheet.Cells[row, 4].Value = item.TamLy_KyNang;
                        worksheet.Cells[row, 5].Value = item.NuoiCon;
                        worksheet.Cells[row, 6].Value = item.SachThieuNhi;
                        worksheet.Cells[row, 7].Value = item.TieuSu_HoiKy;
                        worksheet.Cells[row, 8].Value = item.SGK;
                        worksheet.Cells[row, 9].Value = item.SachNgoaiNgu;
                        worksheet.Cells[row, 10].Value = item.TongTien;
                        //toggle backgroud color
                        //even row with ribbon style
                        if (row % 2 == 0)
                        {
                            for (int i = 1; i <= 12; i++)
                            {
                                worksheet.Cells[row, i].Style.Fill.PatternType = OfficeOpenXml.Style.ExcelFillStyle.Solid;
                                worksheet.Cells[row, i].Style.Fill.BackgroundColor.SetColor(Color.FromArgb(154, 211, 157));
                            }
                        }
                        row++;
                    }
                    //step 7 : auto fit colums
                    worksheet.Cells[worksheet.Dimension.Address].AutoFitColumns();
                    //step 8: convert the package as byte array
                    return package.GetAsByteArray();
                }
                else
                {
                    worksheet = package.Workbook.Worksheets.Add("BaoCaoNam");
                    for (int i = 0; i < col_name.Length; i++)
                    {
                        worksheet.Cells[1, i + 1].Style.Font.Size = 14;
                        worksheet.Cells[1, i + 1].Value = col_name[i];
                        worksheet.Cells[1, i + 1].Style.Font.Bold = true;
                        //border the cell
                        worksheet.Cells[1, i + 1].Style.Border.BorderAround(OfficeOpenXml.Style.ExcelBorderStyle.Thin);
                        //set backgroud color fpr each cells
                        worksheet.Cells[1, i + 1].Style.Fill.PatternType = OfficeOpenXml.Style.ExcelFillStyle.Solid;
                        worksheet.Cells[1, i + 1].Style.Fill.BackgroundColor.SetColor(Color.FromArgb(255, 243, 214));
                    }

                    int row = 2;
                    //step 6: loop through query result and fill in cells
                    foreach (DoanhThuNam item in _context.DoanhThuNams.ToList())
                    {
                        for (int col = 1; col <= 12; col++)
                        {
                            worksheet.Cells[row, col].Style.Font.Size = 12;
                            worksheet.Cells[row, col].Style.Border.BorderAround(OfficeOpenXml.Style.ExcelBorderStyle.Thin);
                        }
                        //set data
                        worksheet.Cells[row, 1].Value = item.DoanhThuId;
                        worksheet.Cells[row, 2].Value = item.VanHoc;
                        worksheet.Cells[row, 3].Value = item.KinhTe;
                        worksheet.Cells[row, 4].Value = item.TamLy_KyNang;
                        worksheet.Cells[row, 5].Value = item.NuoiCon;
                        worksheet.Cells[row, 6].Value = item.SachThieuNhi;
                        worksheet.Cells[row, 7].Value = item.TieuSu_HoiKy;
                        worksheet.Cells[row, 8].Value = item.SGK;
                        worksheet.Cells[row, 9].Value = item.SachNgoaiNgu;
                        worksheet.Cells[row, 10].Value = item.TongTien;
                        //toggle backgroud color
                        //even row with ribbon style
                        if (row % 2 == 0)
                        {
                            for (int i = 1; i <= 12; i++)
                            {
                                worksheet.Cells[row, i].Style.Fill.PatternType = OfficeOpenXml.Style.ExcelFillStyle.Solid;
                                worksheet.Cells[row, i].Style.Fill.BackgroundColor.SetColor(Color.FromArgb(154, 211, 157));
                            }
                        }
                        row++;
                    }
                    //step 7 : auto fit colums
                    worksheet.Cells[worksheet.Dimension.Address].AutoFitColumns();
                    //step 8: convert the package as byte array
                    return package.GetAsByteArray();
                }

                //step 4: Create a new worksheet

                //step 5: fill in header row
                //worksheet.Cells[row,col]. {Style, Value}

            }// end using
        }
    }
}
