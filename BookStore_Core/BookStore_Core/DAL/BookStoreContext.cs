﻿using BookStore_Core.Models;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Internal;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace BookStore_Core.DAL
{
    public class BookStoreContext : DbContext
    {
        public BookStoreContext(DbContextOptions options) : base(options)
        {
        }

        public DbSet<LoaiTacGiaTQ> LoaiTacGiaTongQuats { get; set; }
        public DbSet<TacGia> TacGias { get; set; }
        public DbSet<Customer> Customers { get; set; }
        public DbSet<KhuyenMai> KhuyenMais { get; set; }
        public DbSet<LoaiSachTongQuat> LoaiSachTongQuats { get; set; }
        public DbSet<LoaiSachCuThe> LoaiSachCuThes { get; set; }
        public DbSet<Sach> Saches { get; set; }
        public DbSet<BangGia> BangGias { get; set; }        
        public DbSet<Manager> Managers { get; set; }
        public DbSet<Employee> Employees { get; set; }
        public DbSet<Role> Roles { get; set; }
        public DbSet<Login> Logins { get; set; }
        //public DbSet<BookUpdate> BookUpdates { get; set; }
        public DbSet<Cart> Carts { get; set; }
        public DbSet<HoaDon> HoaDons { get; set; }
        public DbSet<ChiTietHoaDon> ChiTietHoaDons { get; set; }
        public DbSet<AnhPhu> AnhPhus { get; set; }
        public DbSet<SachDuocThich> SachDuocThiches { get; set; }
        public DbSet<KhachHangThich> KhachHangThiches { get; set; }
        public DbSet<DoanhThuNgay> DoanhThuNgays { get; set; }
        public DbSet<DoanhThuThang> DoanhThuThangs { get; set; }
        public DbSet<DoanhThuNam> DoanhThuNams { get; set; }
    }
}
