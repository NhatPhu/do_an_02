﻿namespace BookStore_Core
{
    public class StripeSettings
    {
        public string SecretKey { get; set; }
        public string Publishablekey { get; set; }
    }
}