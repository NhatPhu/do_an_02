﻿using System;
using System.ComponentModel.DataAnnotations;

namespace BookStore_Core.Models
{
    public class ChiTietHoaDon
    {
        [Key]
        public int CTHDId { get; set; }
        public DateTime HoaDonId { get; set; }
        public string SachId { get; set; }
        public int SoLuong { get; set; }
        public int DanhGia { get; set; }
        public bool IsDanhGia { get; set; }
        public int TongTien { get; set; }
        public virtual Sach Sach { get; set; }
        public virtual HoaDon HoaDon { get; set; }
    }
}
