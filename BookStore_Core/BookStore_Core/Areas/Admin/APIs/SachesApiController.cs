﻿using BookStore_Core.DAL;
using BookStore_Core.Models;
using BookStore_Core.ViewModel;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Threading.Tasks;

namespace BookStore_Core.Areas.Admin.APIs
{
    [Route("api/[controller]")]
    [ApiController]
    public class SachesApiController : ControllerBase
    {
        private readonly BookStoreContext _context;
        [Obsolete]
        public static IHostingEnvironment _environment;

        [Obsolete]
        public SachesApiController(BookStoreContext context, IHostingEnvironment environment)
        {
            _context = context;
            _environment = environment;
        }

        // GET: api/SachesApi
        [HttpGet]
        public async Task<ActionResult<IEnumerable<SachApiViewModel>>> GetSaches()
        {
            List<SachApiViewModel> sachApiViewModels = new List<SachApiViewModel>();
            var listSach = await _context.Saches.Include(s => s.LoaiSachCuThe).Include(s => s.TacGia).ToListAsync();
            foreach (var x in listSach)
            {
                var bangGia = _context.BangGias.Where(p => p.SachId == x.SachId && p.IsDel == false).FirstOrDefault();
                int soSpBanDc = 0;
                if (_context.ChiTietHoaDons.Any())
                {
                    var chiTietHoaDon = _context.ChiTietHoaDons.Where(p => p.SachId == x.SachId && p.HoaDonId.Year == DateTime.Now.Year);
                    foreach (var k in chiTietHoaDon)
                    {
                        soSpBanDc += k.SoLuong;
                    }
                }
                else
                {
                    soSpBanDc = 0;
                }

                sachApiViewModels.Add(new SachApiViewModel()
                {
                    //Sach = x,
                    BangGia = bangGia,
                    SoSachBanDuoc = soSpBanDc
                });
            }
            return sachApiViewModels;
        }

        // GET: api/SachesApi/5
        [HttpGet("{id}")]
        public async Task<ActionResult<SachApiViewModel>> GetSach(string id)
        {
            var sach = await _context.Saches.Include(s => s.LoaiSachCuThe).Include(s => s.TacGia).Where(s => s.SachId == id).SingleOrDefaultAsync();
            
            //XacThuc xacThuc = new XacThuc() { Sach = sach, Code = new Login() { LoginName = "phu", Password="123"} };
            if (sach == null)
            {
                return NotFound();
            }
            var bangGia = _context.BangGias.Where(p => p.SachId == sach.SachId && p.IsDel == false).FirstOrDefault();
            return new SachApiViewModel() { SoSachBanDuoc = 0, BangGia = bangGia };
        }

        // PUT: api/SachesApi/5
        [HttpPut("{id}")]
        [Obsolete]
        public async Task<IActionResult> PutSach(string id, XacThuc sach)
        {
            if (id != sach.Sach.SachId)
            {
                return BadRequest();
            }

            _context.Entry(sach.Sach).State = EntityState.Modified;
            try
            {
                if (!Directory.Exists(_environment.WebRootPath + "\\BookImages\\"))
                {
                    Directory.CreateDirectory(_environment.WebRootPath + "\\BookImages\\");
                }
                if (sach.hinh != null)
                    using (var ms = new MemoryStream(sach.hinh))
                    {
                        var path1 = Path.Combine(_environment.WebRootPath, "BookImages", sach.Sach.HinhAnh);
                        var stream1 = new FileStream(path1, FileMode.Create);
                        stream1.Write(sach.hinh, 0, sach.hinh.Count());
                        stream1.Close();
                    }
            }
            catch
            {
                return NotFound();
            }

            var bangGia = _context.BangGias.FirstOrDefault(p => p.BangGiaId == sach.BangGia.BangGiaId);
            if (bangGia != null && bangGia.DonGia != sach.BangGia.DonGia)
            {
                bangGia.IsDel = true;
                _context.BangGias.Update(bangGia);
                _context.BangGias.Add(new BangGia() { NgayCoHieuLuc = DateTime.Now, DonGia = sach.BangGia.DonGia, SachId = sach.Sach.SachId, IsDel = false });
            }
            try
            {
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!SachExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }
            return CreatedAtAction("GetSach", new { id = sach.Sach.SachId }, sach);
        }

        // POST: api/SachesApi
        [HttpPost]
        [Obsolete]
        public async Task<ActionResult<Sach>> PostSach(XacThuc sach)
        {
            try
            {
                if (!Directory.Exists(_environment.WebRootPath + "\\BookImages\\"))
                {
                    Directory.CreateDirectory(_environment.WebRootPath + "\\BookImages\\");
                }
                if (sach.hinh != null)
                    using (var ms = new MemoryStream(sach.hinh))
                    {
                        var path1 = Path.Combine(_environment.WebRootPath, "BookImages", sach.Sach.HinhAnh);
                        var stream1 = new FileStream(path1, FileMode.Create);
                        stream1.Write(sach.hinh, 0, sach.hinh.Count());
                        stream1.Close();
                    }
            }
            catch
            {
                return NotFound();
            }
            _context.Saches.Add(sach.Sach);
            await _context.SaveChangesAsync();
            _context.BangGias.Add(new BangGia() { NgayCoHieuLuc = DateTime.Now, DonGia = sach.BangGia.DonGia, SachId = sach.Sach.SachId, IsDel = false });
            await _context.SaveChangesAsync();
            return CreatedAtAction("GetSach", new { id = sach.Sach.SachId }, sach);
        }

        // DELETE: api/SachesApi/5
        [HttpDelete("{id}")]
        public async Task<ActionResult<Sach>> DeleteSach(string id)
        {
            var sach = await _context.Saches.FindAsync(id);
            if (sach == null)
            {
                return NotFound();
            }

            _context.Saches.Remove(sach);
            await _context.SaveChangesAsync();

            return sach;
        }

        private bool SachExists(string id)
        {
            return _context.Saches.Any(e => e.SachId == id);
        }
    }
}
