﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace BookStore_Core.Models
{
    public class Sach
    {
        [Key]
        [Display(Name = "Id")]
        public string SachId { get; set; }
        [Display(Name = "Tên sách")]
        [Required(ErrorMessage = "Bắt buộc nhập tên sách")]
        public string TenSach { get; set; }
        [Display(Name = "Lượt xem")]
        public int LuotXem { get; set; }
        [Display(Name = "Hình ảnh")]
        public string HinhAnh { get; set; }
        public string LoaiSachCTId { get; set; }
        public string TacGiaId { get; set; }
        [Display(Name = "Mô tả")]
        public string MoTa { get; set; }
        [Display(Name = "Lượt đánh giá")]
        public int LuotDanhGia { get; set; }
        public int SaoTrungBinh { get; set; }
        public int LuotThich { get; set; }
        public DateTime NgayTao { get; set; }
        public string KichThuoc { get; set; }
        public string LoaiBia { get; set; }
        public int TrongLuong { get; set; }
        public int SoTrang { get; set; }
        public bool IsDel { get; set; }
        public virtual LoaiSachCuThe LoaiSachCuThe { get; set; }
        [NotMapped]
        public List<BangGia> BangGias { get; set; }
        //[NotMapped]
        //public List<BookUpdate> BookUpdates { get; set; }
        [NotMapped]
        public List<Cart> Carts { get; set; }
        [ForeignKey("TacGiaId")]
        public virtual TacGia TacGia { get; set; }
        [NotMapped]
        public List<SachDuocThich> SachDuocThichs { get; set; }
        [NotMapped]
        public List<ChiTietHoaDon> ChiTietHoaDons { get; set; }
        [NotMapped]
        public List<AnhPhu> AnhPhus { get; set; }
        [NotMapped]
        public List<KhachHangThich> KhachHangThiches { get; set; }
    }
}
