﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace BookStore_Core.Models
{
    public class BangGia
    {
        [Key]
        public int BangGiaId { get; set; }
        public DateTime NgayCoHieuLuc { get; set; }
        [Required(ErrorMessage = "Đơn giá là bắt buộc")]
        [Display(Name = "Đơn giá")]
        public int DonGia { get; set; }
        public string SachId { get; set; }
        public string MaKhuyenMaiId { get; set; }
        [Required(ErrorMessage = "Phần trăm khuyến mãi là bắt buộc")]
        public int PhanTramKhuyenMai { get; set; }
        public int SoTienGiam { get; set; }
        public bool IsDel { get; set; }

        [ForeignKey("SachId")]
        public virtual Sach Sach { get; set; }
        [ForeignKey("MaKhuyenMaiId")]
        public virtual KhuyenMai KhuyenMai { get; set; }
    }
}
