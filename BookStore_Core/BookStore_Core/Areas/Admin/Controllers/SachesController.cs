﻿using BookStore_Core.DAL;
using BookStore_Core.Models;
using BookStore_Core.ViewModel;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using System;
using System.IO;
using System.Linq;
using System.Threading.Tasks;

namespace BookStore_Core.Areas.Admin.Controllers
{
    [Area("Admin")]
    [Route("Admin/Saches")]
    public class SachesController : Controller
    {
        private readonly IHostingEnvironment _he;
        private readonly BookStoreContext _context;
        public string linkAnhChinh = "";
        public string linkAnhPhu1 = "";
        public string linkAnhPhu2 = "";
        public string linkAnhPhu3 = "";
        public int Gia;

        public SachesController(BookStoreContext context, IHostingEnvironment he)
        {
            _he = he;
            _context = context;
        }

        // GET: Admin/Saches
        [Route("index")]
        public async Task<IActionResult> Index()
        {
            var bookStoreContext = _context.Saches.Include(s => s.LoaiSachCuThe).Include(s => s.TacGia).Where(p => p.IsDel == false);
            return View(await bookStoreContext.ToListAsync());
        }

        // GET: Admin/Saches/Details/5
        [Route("Details/{id}")]
        public async Task<IActionResult> Details(string id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var sach = await _context.Saches
                .Include(s => s.LoaiSachCuThe)
                .Include(s => s.TacGia)
                .FirstOrDefaultAsync(m => m.SachId == id);
            var bangGia = _context.BangGias.Include(s => s.KhuyenMai)
                .Where(p => p.SachId == sach.SachId && p.IsDel == false).FirstOrDefault();
            var anhPhu = _context.AnhPhus.Where(p => p.SachId == sach.SachId).ToList();
            if (sach == null)
            {
                return NotFound();
            }
            SachViewModel sachVM = new SachViewModel() { AnhPhus = anhPhu, Sach = sach, BangGia = bangGia };
            return View(sachVM);
        }


        // GET: Admin/Saches/Create
        [Route("Create")]
        public IActionResult Create()
        {
            ViewData["LoaiSachCTId"] = new SelectList(_context.LoaiSachCuThes, "LoaiSachCTId", "TenLoaiSachCT");
            ViewData["TacGiaId"] = new SelectList(_context.TacGias, "TacGiaId", "TenTacGia");

            return View();
        }

        // POST: Admin/Saches/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [Route("Create")]
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create(ThemSachViewModel sachVM, IFormFile file1, IFormFile file2, IFormFile file3)
        {
            if (ModelState.IsValid)
            {

                var stringDate = DateTime.Now.ToString();
                stringDate = stringDate.Replace(":", ".");
                stringDate = stringDate.Replace(" ", ".");
                stringDate = stringDate.Replace("/", ".");
                var fileName = stringDate + Path.GetFileName(sachVM.file.FileName);
                var path = Path.Combine(_he.WebRootPath, "BookImages", Path.GetFileName(fileName));
                var stream = new FileStream(path, FileMode.Create);
                await sachVM.file.CopyToAsync(stream);
                sachVM.Sach.HinhAnh = fileName;
                


                //Tạo Id cho sách
                var tam = _context.Saches.ToList().Count;
                var lastId = tam + 1;
                string Idtam = "";
                var soChuSo = 0;
                while (tam > 0)
                {
                    tam = tam / 10;
                    soChuSo++;

                }
                int dem = 8 - soChuSo;
                for (int i = 0; i < dem; i++)
                {
                    Idtam += "0";
                }
                Idtam = "SACH" + Idtam + lastId;
                sachVM.Sach.SachId = Idtam;
                sachVM.Sach.NgayTao = DateTime.Today;
                _context.Add(sachVM.Sach);
                await _context.SaveChangesAsync();

                //Thêm bảng giá mới cho sách mới
                _context.Add(new BangGia() { SachId = Idtam, DonGia = sachVM.BangGia.DonGia, NgayCoHieuLuc = DateTime.Now });
                await _context.SaveChangesAsync();

                //Lưu ảnh nhỏ vào database
                //Ảnh nhỏ thứ 1
                if (file1 != null)
                {
                    var fileName1 = stringDate + "AnhNho1" + Path.GetFileName(file1.FileName);
                    var path1 = Path.Combine(_he.WebRootPath, "BookImages", Path.GetFileName(fileName1));
                    var stream1 = new FileStream(path1, FileMode.Create);
                    await file1.CopyToAsync(stream1);
                    _context.AnhPhus.Add(new AnhPhu() { HinhAnh = fileName1, SachId = Idtam });
                    await _context.SaveChangesAsync();
                }
                //Ảnh nhỏ thứ 2
                if (file2 != null)
                {
                    var fileName2 = stringDate + "AnhNho2" + Path.GetFileName(file1.FileName);
                    var path2 = Path.Combine(_he.WebRootPath, "BookImages", Path.GetFileName(fileName2));
                    var stream2 = new FileStream(path2, FileMode.Create);
                    await file2.CopyToAsync(stream2);
                    _context.AnhPhus.Add(new AnhPhu() { HinhAnh = fileName2, SachId = Idtam });
                    await _context.SaveChangesAsync();
                }
                if (file3 != null)
                {
                    var fileName3 = stringDate + "AnhNho3" + Path.GetFileName(file1.FileName);
                    var path3 = Path.Combine(_he.WebRootPath, "BookImages", Path.GetFileName(fileName3));
                    var stream3 = new FileStream(path3, FileMode.Create);
                    await file3.CopyToAsync(stream3);
                    _context.AnhPhus.Add(new AnhPhu() { HinhAnh = fileName3, SachId = Idtam });
                    await _context.SaveChangesAsync();
                }

                return RedirectToAction(nameof(Index));
            }
            //sachVM.Sach.HinhAnh = sachVM.file.FileName;
            ViewData["LoaiSachCTId"] = new SelectList(_context.LoaiSachCuThes, "LoaiSachCTId", "TenLoaiSachCT", sachVM.Sach.LoaiSachCTId);
            ViewData["TacGiaId"] = new SelectList(_context.TacGias, "TacGiaId", "TenTacGia", sachVM.Sach.TacGiaId);
            return View(sachVM);
        }
        [Route("Edit/{id}")]
        // GET: Admin/Saches/Edit/5
        public async Task<IActionResult> Edit(string id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var sach = await _context.Saches
                .Include(s => s.LoaiSachCuThe)
                .Include(s => s.TacGia)
                .FirstOrDefaultAsync(m => m.SachId == id && m.IsDel == false);
            if (sach == null)
            {
                return NotFound();
            }
            var bangGia = _context.BangGias.Include(s => s.KhuyenMai)
                .Where(p => p.SachId == sach.SachId && p.IsDel == false).FirstOrDefault();
            var anhPhu = _context.AnhPhus.Where(p => p.SachId == sach.SachId).ToList();
            if (sach == null)
            {
                return NotFound();
            }
            ThemSachViewModel sachVM = new ThemSachViewModel() { AnhPhus = anhPhu, Sach = sach, BangGia = bangGia };
            Gia = bangGia.DonGia;
            linkAnhChinh = sach.HinhAnh;
            ViewData["LoaiSachCTId"] = new SelectList(_context.LoaiSachCuThes, "LoaiSachCTId", "TenLoaiSachCT", sach.LoaiSachCTId);
            ViewData["TacGiaId"] = new SelectList(_context.TacGias, "TacGiaId", "TenTacGia", sach.TacGiaId);
            return View(sachVM);
        }

        // POST: Admin/Saches/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [Route("Edit/{id}")]
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(string id, ThemSachViewModel sachVM, IFormFile photo1, IFormFile photo2, IFormFile photo3)
        {
            if (id != sachVM.Sach.SachId)
            {
                return NotFound();
            }
            try
            {
                var stringDate = DateTime.Now.ToString();
                stringDate = stringDate.Replace(":", ".");
                stringDate = stringDate.Replace(" ", ".");
                stringDate = stringDate.Replace("/", ".");
                if (sachVM.file != null)
                {
                    var fileName = stringDate + Path.GetFileName(sachVM.file.FileName);
                    var path = Path.Combine(_he.WebRootPath, "BookImages", Path.GetFileName(fileName));
                    var stream = new FileStream(path, FileMode.Create);
                    await sachVM.file.CopyToAsync(stream);
                    sachVM.Sach.HinhAnh = fileName;
                }
                else
                {
                    sachVM.Sach.HinhAnh = sachVM.Sach.HinhAnh;
                }

                //Xử lý ảnh nhỏ 1
                if (photo1 != null)
                {
                    var fileName = stringDate + Path.GetFileName(photo1.FileName);
                    var path = Path.Combine(_he.WebRootPath, "BookImages", Path.GetFileName(fileName));
                    var stream = new FileStream(path, FileMode.Create);
                    await photo1.CopyToAsync(stream);
                    if (sachVM.AnhPhus == null || sachVM.AnhPhus.Count <= 0)
                    {
                        _context.AnhPhus.Add(new AnhPhu() { HinhAnh = fileName, SachId = id });
                        _context.SaveChanges();
                    }
                    else
                    {
                        var anhPhu01 = _context.AnhPhus.Where(p => p.IdAnh == sachVM.AnhPhus[0].IdAnh).FirstOrDefault();
                        anhPhu01.HinhAnh = fileName;
                        _context.Update(anhPhu01);
                        _context.SaveChanges();
                    }

                }

                //xử lý ảnh nhỏ 2
                if (photo2 != null)
                {
                    var fileName = stringDate + Path.GetFileName(photo2.FileName);
                    var path = Path.Combine(_he.WebRootPath, "BookImages", Path.GetFileName(fileName));
                    var stream = new FileStream(path, FileMode.Create);
                    await photo2.CopyToAsync(stream);
                    if (sachVM.AnhPhus == null || sachVM.AnhPhus.Count <= 1)
                    {
                        _context.AnhPhus.Add(new AnhPhu() { HinhAnh = fileName, SachId = id });
                        _context.SaveChanges();
                    }
                    else
                    {
                        var anhPhu02 = _context.AnhPhus.Where(p => p.IdAnh == sachVM.AnhPhus[1].IdAnh).FirstOrDefault();
                        anhPhu02.HinhAnh = fileName;
                        _context.Update(anhPhu02);
                        _context.SaveChanges();
                    }
                }
                //xử lý ảnh nhỏ 3
                if (photo3 != null)
                {
                    var fileName = stringDate + Path.GetFileName(photo3.FileName);
                    var path = Path.Combine(_he.WebRootPath, "BookImages", Path.GetFileName(fileName));
                    var stream = new FileStream(path, FileMode.Create);
                    await photo3.CopyToAsync(stream);
                    if (sachVM.AnhPhus == null || sachVM.AnhPhus.Count <= 2)
                    {
                        _context.AnhPhus.Add(new AnhPhu() { HinhAnh = fileName, SachId = id });
                        _context.SaveChanges();
                    }
                    else
                    {
                        var anhPhu03 = _context.AnhPhus.Where(p => p.IdAnh == sachVM.AnhPhus[2].IdAnh).FirstOrDefault();
                        anhPhu03.HinhAnh = fileName;
                        _context.Update(anhPhu03);
                        _context.SaveChanges();
                    }
                }
                var giaCu = _context.BangGias.FirstOrDefault(p => p.SachId == id && p.IsDel == false);
                if (sachVM.BangGia.DonGia != giaCu.DonGia)
                {
                    giaCu.IsDel = true;
                    _context.Update(giaCu);
                    await _context.SaveChangesAsync();
                    _context.BangGias.Add(new BangGia() { DonGia = sachVM.BangGia.DonGia, NgayCoHieuLuc = DateTime.Today, SachId = sachVM.Sach.SachId });
                    await _context.SaveChangesAsync();
                }
                _context.Update(sachVM.Sach);
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!SachExists(sachVM.Sach.SachId))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }
            return RedirectToAction(nameof(Index));

            //ViewData["LoaiSachCTId"] = new SelectList(_context.LoaiSachCuThes, "LoaiSachCTId", "TenLoaiSachCT", sachVM.Sach.LoaiSachCTId);
            //ViewData["TacGiaId"] = new SelectList(_context.TacGias, "TacGiaId", "TenTacGia", sachVM.Sach.TacGiaId);
            //return View(sachVM);
        }

        // GET: Admin/Saches/Delete/5
        public async Task<IActionResult> Delete(string id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var sach = await _context.Saches
                .Include(s => s.LoaiSachCuThe)
                .Include(s => s.TacGia)
                .FirstOrDefaultAsync(m => m.SachId == id);
            if (sach == null)
            {
                return NotFound();
            }

            return View(sach);
        }

        // POST: Admin/Saches/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> DeleteConfirmed(string id)
        {
            var sach = await _context.Saches.FirstOrDefaultAsync(p => p.SachId == id);
            sach.IsDel = true;
            _context.Update(sach);
            await _context.SaveChangesAsync();
            return RedirectToAction(nameof(Index));
        }

        private bool SachExists(string id)
        {
            return _context.Saches.Any(e => e.SachId == id);
        }
    }
}
