﻿using System.Collections.Generic;

namespace BookStore_Core.Models
{
    public class SachViewModel
    {
        public Sach Sach { get; set; }
        public BangGia BangGia { get; set; }
        public SachDuocThich SachDuocThich { get; set; }
        public List<AnhPhu> AnhPhus { get; set; }
        public int SoSachBanDuoc { get; set; }
        public string TrangThai { get; set; }
    }
}
