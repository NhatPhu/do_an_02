﻿using System;
using System.ComponentModel.DataAnnotations;

namespace BookStore_Core.Models
{
    public class Cart
    {
        [Key]
        public DateTime CartId { get; set; }
        public string SachId { get; set; }
        public string CustomerId { get; set; }
        public int Amount { get; set; }
        public virtual Sach Sach { get; set; }

        public virtual Customer Customer { get; set; }
    }
}
