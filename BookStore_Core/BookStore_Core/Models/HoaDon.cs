﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace BookStore_Core.Models
{
    public class HoaDon
    {
        [Key]
        public DateTime HoaDonId { get; set; }
        public string CustomerId { get; set; }
        public int TongTien { get; set; }
        public bool IsDanhGia { get; set; }
        public bool XacNhan { get; set; }
        public int TrangThai { get; set; }
        public virtual Customer Customer { get; set; }
        [NotMapped]
        public List<ChiTietHoaDon> ChiTietHoaDons { get; set; }
    }
}
