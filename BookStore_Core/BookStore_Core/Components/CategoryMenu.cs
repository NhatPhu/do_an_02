﻿using BookStore_Core.DAL;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using System.Linq;
using System.Threading.Tasks;

namespace BookStore_Core.Components
{
    public class CategoryMenu : ViewComponent
    {
        private readonly BookStoreContext _context;
        public CategoryMenu(BookStoreContext context)
        {
            _context = context;
        }

        public async Task<IViewComponentResult> InvokeAsync()
        {
            var loaiSachTQ = await _context.LoaiSachTongQuats.OrderBy(c => c.TenLoaiSachTQ).ToListAsync();
            return View(loaiSachTQ);
        }
    }
}
