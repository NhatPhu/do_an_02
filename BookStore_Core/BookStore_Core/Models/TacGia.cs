﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace BookStore_Core.Models
{
    public class TacGia
    {
        [Key]
        public string TacGiaId { get; set; }
        public string TenTacGia { get; set; }
        public int LoaiTacGiaTQId { get; set; }
        [NotMapped]
        public List<Sach> Sachs { get; set; }
        public virtual LoaiTacGiaTQ LoaiTacGiaTQ { get; set; }
    }
}
