﻿using System;
using Microsoft.EntityFrameworkCore.Metadata;
using Microsoft.EntityFrameworkCore.Migrations;

namespace BookStore_Core.Migrations
{
    public partial class update01 : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "Customers",
                columns: table => new
                {
                    CustomerId = table.Column<string>(nullable: false),
                    CustomerName = table.Column<string>(nullable: true),
                    CustomerEmail = table.Column<string>(nullable: true),
                    Phone = table.Column<string>(nullable: true),
                    Address = table.Column<string>(nullable: true),
                    BirthDay = table.Column<DateTime>(nullable: false),
                    LastUpdate = table.Column<DateTime>(nullable: false),
                    DiemSo = table.Column<int>(nullable: false),
                    IsDel = table.Column<bool>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Customers", x => x.CustomerId);
                });

            migrationBuilder.CreateTable(
                name: "DoanhThuNams",
                columns: table => new
                {
                    DoanhThuId = table.Column<string>(nullable: false),
                    VanHoc = table.Column<int>(nullable: false),
                    KinhTe = table.Column<int>(nullable: false),
                    TamLy_KyNang = table.Column<int>(nullable: false),
                    NuoiCon = table.Column<int>(nullable: false),
                    SachThieuNhi = table.Column<int>(nullable: false),
                    TieuSu_HoiKy = table.Column<int>(nullable: false),
                    SGK = table.Column<int>(nullable: false),
                    SachNgoaiNgu = table.Column<int>(nullable: false),
                    TongTien = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_DoanhThuNams", x => x.DoanhThuId);
                });

            migrationBuilder.CreateTable(
                name: "DoanhThuNgays",
                columns: table => new
                {
                    DoanhThuId = table.Column<DateTime>(nullable: false),
                    VanHoc = table.Column<int>(nullable: false),
                    KinhTe = table.Column<int>(nullable: false),
                    TamLy_KyNang = table.Column<int>(nullable: false),
                    NuoiCon = table.Column<int>(nullable: false),
                    SachThieuNhi = table.Column<int>(nullable: false),
                    TieuSu_HoiKy = table.Column<int>(nullable: false),
                    SGK = table.Column<int>(nullable: false),
                    SachNgoaiNgu = table.Column<int>(nullable: false),
                    TongTien = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_DoanhThuNgays", x => x.DoanhThuId);
                });

            migrationBuilder.CreateTable(
                name: "DoanhThuThangs",
                columns: table => new
                {
                    DoanhThuId = table.Column<DateTime>(nullable: false),
                    VanHoc = table.Column<int>(nullable: false),
                    KinhTe = table.Column<int>(nullable: false),
                    TamLy_KyNang = table.Column<int>(nullable: false),
                    NuoiCon = table.Column<int>(nullable: false),
                    SachThieuNhi = table.Column<int>(nullable: false),
                    TieuSu_HoiKy = table.Column<int>(nullable: false),
                    SGK = table.Column<int>(nullable: false),
                    SachNgoaiNgu = table.Column<int>(nullable: false),
                    TongTien = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_DoanhThuThangs", x => x.DoanhThuId);
                });

            migrationBuilder.CreateTable(
                name: "Employees",
                columns: table => new
                {
                    EmployeeId = table.Column<string>(nullable: false),
                    EmployeeName = table.Column<string>(nullable: true),
                    Email = table.Column<string>(nullable: true),
                    Phone = table.Column<string>(nullable: true),
                    Address = table.Column<string>(nullable: true),
                    BirthDay = table.Column<DateTime>(nullable: false),
                    LastUpdate = table.Column<DateTime>(nullable: false),
                    IsDel = table.Column<bool>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Employees", x => x.EmployeeId);
                });

            migrationBuilder.CreateTable(
                name: "KhuyenMais",
                columns: table => new
                {
                    MaKhuyenMaiId = table.Column<string>(nullable: false),
                    NgayBatDau = table.Column<DateTime>(nullable: false),
                    NgayKetThuc = table.Column<DateTime>(nullable: false),
                    TenKhuyenMai = table.Column<string>(nullable: true),
                    HoaDonToiThieu = table.Column<int>(nullable: false),
                    SoLuongSpCungLoai = table.Column<int>(nullable: false),
                    SoLuongSpKhacLoai = table.Column<int>(nullable: false),
                    HinhAnh = table.Column<string>(nullable: true),
                    TrangThai = table.Column<bool>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_KhuyenMais", x => x.MaKhuyenMaiId);
                });

            migrationBuilder.CreateTable(
                name: "LoaiSachTongQuats",
                columns: table => new
                {
                    LoaiSachTQId = table.Column<string>(nullable: false),
                    TenLoaiSachTQ = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_LoaiSachTongQuats", x => x.LoaiSachTQId);
                });

            migrationBuilder.CreateTable(
                name: "LoaiTacGiaTongQuats",
                columns: table => new
                {
                    LoaiTacGiaTQId = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    TenLoaiTacGiaTQ = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_LoaiTacGiaTongQuats", x => x.LoaiTacGiaTQId);
                });

            migrationBuilder.CreateTable(
                name: "Managers",
                columns: table => new
                {
                    ManagerId = table.Column<string>(nullable: false),
                    ManagerName = table.Column<string>(nullable: true),
                    Phone = table.Column<string>(nullable: true),
                    Address = table.Column<string>(nullable: true),
                    Email = table.Column<string>(nullable: true),
                    BirthDay = table.Column<DateTime>(nullable: false),
                    LastUpdate = table.Column<DateTime>(nullable: false),
                    IsDel = table.Column<bool>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Managers", x => x.ManagerId);
                });

            migrationBuilder.CreateTable(
                name: "Roles",
                columns: table => new
                {
                    RoleId = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    RoleName = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Roles", x => x.RoleId);
                });

            migrationBuilder.CreateTable(
                name: "HoaDons",
                columns: table => new
                {
                    HoaDonId = table.Column<DateTime>(nullable: false),
                    CustomerId = table.Column<string>(nullable: true),
                    TongTien = table.Column<int>(nullable: false),
                    IsDanhGia = table.Column<bool>(nullable: false),
                    XacNhan = table.Column<bool>(nullable: false),
                    TrangThai = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_HoaDons", x => x.HoaDonId);
                    table.ForeignKey(
                        name: "FK_HoaDons_Customers_CustomerId",
                        column: x => x.CustomerId,
                        principalTable: "Customers",
                        principalColumn: "CustomerId",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "LoaiSachCuThes",
                columns: table => new
                {
                    LoaiSachCTId = table.Column<string>(nullable: false),
                    TenLoaiSachCT = table.Column<string>(nullable: true),
                    LoaiSachTQId = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_LoaiSachCuThes", x => x.LoaiSachCTId);
                    table.ForeignKey(
                        name: "FK_LoaiSachCuThes_LoaiSachTongQuats_LoaiSachTQId",
                        column: x => x.LoaiSachTQId,
                        principalTable: "LoaiSachTongQuats",
                        principalColumn: "LoaiSachTQId",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "TacGias",
                columns: table => new
                {
                    TacGiaId = table.Column<string>(nullable: false),
                    TenTacGia = table.Column<string>(nullable: true),
                    LoaiTacGiaTQId = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_TacGias", x => x.TacGiaId);
                    table.ForeignKey(
                        name: "FK_TacGias_LoaiTacGiaTongQuats_LoaiTacGiaTQId",
                        column: x => x.LoaiTacGiaTQId,
                        principalTable: "LoaiTacGiaTongQuats",
                        principalColumn: "LoaiTacGiaTQId",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "Logins",
                columns: table => new
                {
                    LoginName = table.Column<string>(nullable: false),
                    Password = table.Column<string>(nullable: true),
                    RoleId = table.Column<int>(nullable: false),
                    ResetPassword = table.Column<string>(nullable: true),
                    LastUpdate = table.Column<DateTime>(nullable: false),
                    IsDel = table.Column<bool>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Logins", x => x.LoginName);
                    table.ForeignKey(
                        name: "FK_Logins_Roles_RoleId",
                        column: x => x.RoleId,
                        principalTable: "Roles",
                        principalColumn: "RoleId",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "Saches",
                columns: table => new
                {
                    SachId = table.Column<string>(nullable: false),
                    TenSach = table.Column<string>(nullable: false),
                    LuotXem = table.Column<int>(nullable: false),
                    HinhAnh = table.Column<string>(nullable: true),
                    LoaiSachCTId = table.Column<string>(nullable: true),
                    TacGiaId = table.Column<string>(nullable: true),
                    MoTa = table.Column<string>(nullable: true),
                    LuotDanhGia = table.Column<int>(nullable: false),
                    SaoTrungBinh = table.Column<int>(nullable: false),
                    LuotThich = table.Column<int>(nullable: false),
                    NgayTao = table.Column<DateTime>(nullable: false),
                    KichThuoc = table.Column<string>(nullable: true),
                    LoaiBia = table.Column<string>(nullable: true),
                    TrongLuong = table.Column<int>(nullable: false),
                    SoTrang = table.Column<int>(nullable: false),
                    IsDel = table.Column<bool>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Saches", x => x.SachId);
                    table.ForeignKey(
                        name: "FK_Saches_LoaiSachCuThes_LoaiSachCTId",
                        column: x => x.LoaiSachCTId,
                        principalTable: "LoaiSachCuThes",
                        principalColumn: "LoaiSachCTId",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_Saches_TacGias_TacGiaId",
                        column: x => x.TacGiaId,
                        principalTable: "TacGias",
                        principalColumn: "TacGiaId",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "AnhPhus",
                columns: table => new
                {
                    IdAnh = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    HinhAnh = table.Column<string>(nullable: true),
                    SachId = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_AnhPhus", x => x.IdAnh);
                    table.ForeignKey(
                        name: "FK_AnhPhus_Saches_SachId",
                        column: x => x.SachId,
                        principalTable: "Saches",
                        principalColumn: "SachId",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "BangGias",
                columns: table => new
                {
                    BangGiaId = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    NgayCoHieuLuc = table.Column<DateTime>(nullable: false),
                    DonGia = table.Column<int>(nullable: false),
                    SachId = table.Column<string>(nullable: true),
                    MaKhuyenMaiId = table.Column<string>(nullable: true),
                    PhanTramKhuyenMai = table.Column<int>(nullable: false),
                    SoTienGiam = table.Column<int>(nullable: false),
                    IsDel = table.Column<bool>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_BangGias", x => x.BangGiaId);
                    table.ForeignKey(
                        name: "FK_BangGias_KhuyenMais_MaKhuyenMaiId",
                        column: x => x.MaKhuyenMaiId,
                        principalTable: "KhuyenMais",
                        principalColumn: "MaKhuyenMaiId",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_BangGias_Saches_SachId",
                        column: x => x.SachId,
                        principalTable: "Saches",
                        principalColumn: "SachId",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "Carts",
                columns: table => new
                {
                    CartId = table.Column<DateTime>(nullable: false),
                    SachId = table.Column<string>(nullable: true),
                    CustomerId = table.Column<string>(nullable: true),
                    Amount = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Carts", x => x.CartId);
                    table.ForeignKey(
                        name: "FK_Carts_Customers_CustomerId",
                        column: x => x.CustomerId,
                        principalTable: "Customers",
                        principalColumn: "CustomerId",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_Carts_Saches_SachId",
                        column: x => x.SachId,
                        principalTable: "Saches",
                        principalColumn: "SachId",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "ChiTietHoaDons",
                columns: table => new
                {
                    CTHDId = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    HoaDonId = table.Column<DateTime>(nullable: false),
                    SachId = table.Column<string>(nullable: true),
                    SoLuong = table.Column<int>(nullable: false),
                    DanhGia = table.Column<int>(nullable: false),
                    IsDanhGia = table.Column<bool>(nullable: false),
                    TongTien = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_ChiTietHoaDons", x => x.CTHDId);
                    table.ForeignKey(
                        name: "FK_ChiTietHoaDons_HoaDons_HoaDonId",
                        column: x => x.HoaDonId,
                        principalTable: "HoaDons",
                        principalColumn: "HoaDonId",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_ChiTietHoaDons_Saches_SachId",
                        column: x => x.SachId,
                        principalTable: "Saches",
                        principalColumn: "SachId",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "KhachHangThiches",
                columns: table => new
                {
                    ThichId = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    SachId = table.Column<string>(nullable: true),
                    CustomerId = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_KhachHangThiches", x => x.ThichId);
                    table.ForeignKey(
                        name: "FK_KhachHangThiches_Customers_CustomerId",
                        column: x => x.CustomerId,
                        principalTable: "Customers",
                        principalColumn: "CustomerId",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_KhachHangThiches_Saches_SachId",
                        column: x => x.SachId,
                        principalTable: "Saches",
                        principalColumn: "SachId",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "SachDuocThiches",
                columns: table => new
                {
                    ThichId = table.Column<DateTime>(nullable: false),
                    CustomerId = table.Column<string>(nullable: true),
                    SachId = table.Column<string>(nullable: true),
                    IsLike = table.Column<bool>(nullable: false),
                    DanhGia = table.Column<int>(nullable: false),
                    NhanXet = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_SachDuocThiches", x => x.ThichId);
                    table.ForeignKey(
                        name: "FK_SachDuocThiches_Customers_CustomerId",
                        column: x => x.CustomerId,
                        principalTable: "Customers",
                        principalColumn: "CustomerId",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_SachDuocThiches_Saches_SachId",
                        column: x => x.SachId,
                        principalTable: "Saches",
                        principalColumn: "SachId",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateIndex(
                name: "IX_AnhPhus_SachId",
                table: "AnhPhus",
                column: "SachId");

            migrationBuilder.CreateIndex(
                name: "IX_BangGias_MaKhuyenMaiId",
                table: "BangGias",
                column: "MaKhuyenMaiId");

            migrationBuilder.CreateIndex(
                name: "IX_BangGias_SachId",
                table: "BangGias",
                column: "SachId");

            migrationBuilder.CreateIndex(
                name: "IX_Carts_CustomerId",
                table: "Carts",
                column: "CustomerId");

            migrationBuilder.CreateIndex(
                name: "IX_Carts_SachId",
                table: "Carts",
                column: "SachId");

            migrationBuilder.CreateIndex(
                name: "IX_ChiTietHoaDons_HoaDonId",
                table: "ChiTietHoaDons",
                column: "HoaDonId");

            migrationBuilder.CreateIndex(
                name: "IX_ChiTietHoaDons_SachId",
                table: "ChiTietHoaDons",
                column: "SachId");

            migrationBuilder.CreateIndex(
                name: "IX_HoaDons_CustomerId",
                table: "HoaDons",
                column: "CustomerId");

            migrationBuilder.CreateIndex(
                name: "IX_KhachHangThiches_CustomerId",
                table: "KhachHangThiches",
                column: "CustomerId");

            migrationBuilder.CreateIndex(
                name: "IX_KhachHangThiches_SachId",
                table: "KhachHangThiches",
                column: "SachId");

            migrationBuilder.CreateIndex(
                name: "IX_LoaiSachCuThes_LoaiSachTQId",
                table: "LoaiSachCuThes",
                column: "LoaiSachTQId");

            migrationBuilder.CreateIndex(
                name: "IX_Logins_RoleId",
                table: "Logins",
                column: "RoleId");

            migrationBuilder.CreateIndex(
                name: "IX_SachDuocThiches_CustomerId",
                table: "SachDuocThiches",
                column: "CustomerId");

            migrationBuilder.CreateIndex(
                name: "IX_SachDuocThiches_SachId",
                table: "SachDuocThiches",
                column: "SachId");

            migrationBuilder.CreateIndex(
                name: "IX_Saches_LoaiSachCTId",
                table: "Saches",
                column: "LoaiSachCTId");

            migrationBuilder.CreateIndex(
                name: "IX_Saches_TacGiaId",
                table: "Saches",
                column: "TacGiaId");

            migrationBuilder.CreateIndex(
                name: "IX_TacGias_LoaiTacGiaTQId",
                table: "TacGias",
                column: "LoaiTacGiaTQId");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "AnhPhus");

            migrationBuilder.DropTable(
                name: "BangGias");

            migrationBuilder.DropTable(
                name: "Carts");

            migrationBuilder.DropTable(
                name: "ChiTietHoaDons");

            migrationBuilder.DropTable(
                name: "DoanhThuNams");

            migrationBuilder.DropTable(
                name: "DoanhThuNgays");

            migrationBuilder.DropTable(
                name: "DoanhThuThangs");

            migrationBuilder.DropTable(
                name: "Employees");

            migrationBuilder.DropTable(
                name: "KhachHangThiches");

            migrationBuilder.DropTable(
                name: "Logins");

            migrationBuilder.DropTable(
                name: "Managers");

            migrationBuilder.DropTable(
                name: "SachDuocThiches");

            migrationBuilder.DropTable(
                name: "KhuyenMais");

            migrationBuilder.DropTable(
                name: "HoaDons");

            migrationBuilder.DropTable(
                name: "Roles");

            migrationBuilder.DropTable(
                name: "Saches");

            migrationBuilder.DropTable(
                name: "Customers");

            migrationBuilder.DropTable(
                name: "LoaiSachCuThes");

            migrationBuilder.DropTable(
                name: "TacGias");

            migrationBuilder.DropTable(
                name: "LoaiSachTongQuats");

            migrationBuilder.DropTable(
                name: "LoaiTacGiaTongQuats");
        }
    }
}
