﻿using BookStore_Core.DAL;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using System;
using System.Linq;

namespace BookStore_Core.Areas.Admin.Controllers
{
    [Area("Admin")]
    [Route("Admin/AdminMenus")]
    public class AdminMenusController : Controller
    {
        private readonly BookStoreContext _context;
        public AdminMenusController(BookStoreContext context)
        {
            _context = context;
        }
        public IActionResult Index()
        {
            var hoaDon = _context.HoaDons.Where(p => p.HoaDonId.Date.Month == DateTime.Now.Date.Month && p.XacNhan == true);
            int[] tien = new int[15];
            foreach (var x in hoaDon)
            {
                if (x.HoaDonId.Date.Day == 1 || x.HoaDonId.Date.Day == 2)
                    tien[0] += x.TongTien / 1000;
                else if (x.HoaDonId.Date.Day == 3 || x.HoaDonId.Date.Day == 4)
                    tien[1] += x.TongTien / 1000;
                else if (x.HoaDonId.Date.Day == 5 || x.HoaDonId.Date.Day == 6)
                    tien[2] += x.TongTien / 1000;
                else if (x.HoaDonId.Date.Day == 7 || x.HoaDonId.Date.Day == 8)
                    tien[3] += x.TongTien / 1000;
                else if (x.HoaDonId.Date.Day == 9 || x.HoaDonId.Date.Day == 10)
                    tien[4] += x.TongTien / 1000;
                else if (x.HoaDonId.Date.Day == 11 || x.HoaDonId.Date.Day == 12)
                    tien[5] += x.TongTien / 1000;
                else if (x.HoaDonId.Date.Day == 13 || x.HoaDonId.Date.Day == 14)
                    tien[6] += x.TongTien / 1000;
                else if (x.HoaDonId.Date.Day == 15 || x.HoaDonId.Date.Day == 16)
                    tien[7] += x.TongTien / 1000;
                else if (x.HoaDonId.Date.Day == 17 || x.HoaDonId.Date.Day == 18)
                    tien[8] += x.TongTien / 1000;
                else if (x.HoaDonId.Date.Day == 19 || x.HoaDonId.Date.Day == 20)
                    tien[9] += x.TongTien / 1000;
                else if (x.HoaDonId.Date.Day == 21 || x.HoaDonId.Date.Day == 22)
                    tien[10] += x.TongTien / 1000;
                else if (x.HoaDonId.Date.Day == 23 || x.HoaDonId.Date.Day == 24)
                    tien[11] += x.TongTien / 1000;
                else if (x.HoaDonId.Date.Day == 25 || x.HoaDonId.Date.Day == 26)
                    tien[12] += x.TongTien / 1000;
                else if (x.HoaDonId.Date.Day == 27 || x.HoaDonId.Date.Day == 28)
                    tien[13] += x.TongTien / 1000;
                else
                    tien[14] += x.TongTien / 1000;
            }
            ViewBag.Tien = tien;

            var chiTietHoaDon = _context.ChiTietHoaDons.Include(p => p.HoaDon).Include(p => p.Sach).Where(p => p.HoaDon.HoaDonId.Month == DateTime.Now.Month && p.HoaDon.HoaDonId.Year == DateTime.Now.Year && p.HoaDon.XacNhan == true).ToList();
            var loaiTongQuat = _context.LoaiSachTongQuats;
            SoLuongBanDc[] sl = new SoLuongBanDc[loaiTongQuat.Count()];

            int i = 0;
            foreach (var k in loaiTongQuat)
            {
                sl[i] = new SoLuongBanDc();
                sl[i].Ten = k.TenLoaiSachTQ;
                sl[i].PhanTram = 0;
                i++;
            }
            i = 0;
            int Tong = 0;
            foreach (var x in chiTietHoaDon)
            {
                Tong += x.TongTien;
                var theLoaiCT = _context.LoaiSachCuThes.Include(p => p.LoaiSachTongQuat).FirstOrDefault(p => p.LoaiSachCTId == x.Sach.LoaiSachCTId);
                for (int k = 0; k < sl.Length; k++)
                {
                    if (sl[k].Ten == theLoaiCT.LoaiSachTongQuat.TenLoaiSachTQ)
                    {
                        sl[k].PhanTram += x.TongTien;
                        break;
                    }
                }
            }
            for (int k = 0; k < sl.Length; k++)
            {
                sl[k].PhanTram = sl[k].PhanTram / Tong * 100;
            }
            ViewBag.SL = sl;

            var thang = _context.DoanhThuThangs.Where(p => p.DoanhThuId.Year == DateTime.Now.Year).ToList();
            int[] dtthang = new int[12];
            foreach (var x in thang)
            {
                dtthang[x.DoanhThuId.Month - 1] = x.TongTien / 1000;
            }
            ViewBag.DTThang = dtthang;
            return View();
        }
    }

    public class SoLuongBanDc
    {
        public SoLuongBanDc() { }

        public string Ten { get; set; }
        public double PhanTram { get; set; }
    }
}