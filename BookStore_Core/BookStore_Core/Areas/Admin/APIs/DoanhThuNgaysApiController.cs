﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using BookStore_Core.DAL;
using BookStore_Core.Models;

namespace BookStore_Core.Areas.Admin.APIs
{
    [Route("api/[controller]")]
    [ApiController]
    public class DoanhThuNgaysApiController : ControllerBase
    {
        private readonly BookStoreContext _context;

        public DoanhThuNgaysApiController(BookStoreContext context)
        {
            _context = context;
        }

        // GET: api/DoanhThuNgaysApi
        [HttpGet]
        public async Task<ActionResult<IEnumerable<DoanhThuNgay>>> GetDoanhThuNgays()
        {
            return await _context.DoanhThuNgays.ToListAsync();
        }

        // GET: api/DoanhThuNgaysApi/5
        [HttpGet("{id}")]
        public async Task<ActionResult<DoanhThuNgay>> GetDoanhThuNgay(DateTime id)
        {
            var doanhThuNgay = await _context.DoanhThuNgays.FindAsync(id);

            if (doanhThuNgay == null)
            {
                return NotFound();
            }

            return doanhThuNgay;
        }

        // PUT: api/DoanhThuNgaysApi/5
        [HttpPut("{id}")]
        public async Task<IActionResult> PutDoanhThuNgay(DateTime id, DoanhThuNgay doanhThuNgay)
        {
            if (id != doanhThuNgay.DoanhThuId)
            {
                return BadRequest();
            }

            _context.Entry(doanhThuNgay).State = EntityState.Modified;

            try
            {
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!DoanhThuNgayExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return NoContent();
        }

        // POST: api/DoanhThuNgaysApi
        [HttpPost]
        public async Task<ActionResult<DoanhThuNgay>> PostDoanhThuNgay(DoanhThuNgay doanhThuNgay)
        {
            _context.DoanhThuNgays.Add(doanhThuNgay);
            try
            {
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateException)
            {
                if (DoanhThuNgayExists(doanhThuNgay.DoanhThuId))
                {
                    return Conflict();
                }
                else
                {
                    throw;
                }
            }

            return CreatedAtAction("GetDoanhThuNgay", new { id = doanhThuNgay.DoanhThuId }, doanhThuNgay);
        }

        // DELETE: api/DoanhThuNgaysApi/5
        [HttpDelete("{id}")]
        public async Task<ActionResult<DoanhThuNgay>> DeleteDoanhThuNgay(DateTime id)
        {
            var doanhThuNgay = await _context.DoanhThuNgays.FindAsync(id);
            if (doanhThuNgay == null)
            {
                return NotFound();
            }

            _context.DoanhThuNgays.Remove(doanhThuNgay);
            await _context.SaveChangesAsync();

            return doanhThuNgay;
        }

        private bool DoanhThuNgayExists(DateTime id)
        {
            return _context.DoanhThuNgays.Any(e => e.DoanhThuId == id);
        }
    }
}
