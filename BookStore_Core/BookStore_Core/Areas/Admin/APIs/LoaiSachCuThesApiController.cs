﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using BookStore_Core.DAL;
using BookStore_Core.Models;

namespace BookStore_Core.Areas.Admin.APIs
{
    [Route("api/[controller]")]
    [ApiController]
    public class LoaiSachCuThesApiController : ControllerBase
    {
        private readonly BookStoreContext _context;

        public LoaiSachCuThesApiController(BookStoreContext context)
        {
            _context = context;
        }

        // GET: api/LoaiSachCuThesApi
        [HttpGet]
        public async Task<ActionResult<IEnumerable<LoaiSachCuThe>>> GetLoaiSachCuThes()
        {
            return await _context.LoaiSachCuThes.ToListAsync();
        }

        // GET: api/LoaiSachCuThesApi/5
        [HttpGet("{id}")]
        public async Task<ActionResult<LoaiSachCuThe>> GetLoaiSachCuThe(string id)
        {
            var loaiSachCuThe = await _context.LoaiSachCuThes.FindAsync(id);

            if (loaiSachCuThe == null)
            {
                return NotFound();
            }

            return loaiSachCuThe;
        }

        // PUT: api/LoaiSachCuThesApi/5
        [HttpPut("{id}")]
        public async Task<IActionResult> PutLoaiSachCuThe(string id, LoaiSachCuThe loaiSachCuThe)
        {
            if (id != loaiSachCuThe.LoaiSachCTId)
            {
                return BadRequest();
            }

            _context.Entry(loaiSachCuThe).State = EntityState.Modified;

            try
            {
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!LoaiSachCuTheExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return NoContent();
        }

        // POST: api/LoaiSachCuThesApi
        [HttpPost]
        public async Task<ActionResult<LoaiSachCuThe>> PostLoaiSachCuThe(LoaiSachCuThe loaiSachCuThe)
        {
            _context.LoaiSachCuThes.Add(loaiSachCuThe);
            await _context.SaveChangesAsync();

            return CreatedAtAction("GetLoaiSachCuThe", new { id = loaiSachCuThe.LoaiSachCTId }, loaiSachCuThe);
        }

        // DELETE: api/LoaiSachCuThesApi/5
        [HttpDelete("{id}")]
        public async Task<ActionResult<LoaiSachCuThe>> DeleteLoaiSachCuThe(string id)
        {
            var loaiSachCuThe = await _context.LoaiSachCuThes.FindAsync(id);
            if (loaiSachCuThe == null)
            {
                return NotFound();
            }

            _context.LoaiSachCuThes.Remove(loaiSachCuThe);
            await _context.SaveChangesAsync();

            return loaiSachCuThe;
        }

        private bool LoaiSachCuTheExists(string id)
        {
            return _context.LoaiSachCuThes.Any(e => e.LoaiSachCTId == id);
        }
    }
}
