﻿using BookStore_Core.DAL;
using BookStore_Core.Models;
using BookStore_Core.ViewModel;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Net.Mail;
using System.Threading.Tasks;

namespace BookStore_Core.Controllers
{
    public class AccountController : Controller
    {
        private readonly BookStoreContext _context;
        string url = "https://localhost:44305/api/SachesApi";
        HttpClient client;
        public AccountController(BookStoreContext context)
        {
            _context = context;
            client = new HttpClient();
            client.BaseAddress = new Uri(url);
            client.DefaultRequestHeaders.Accept.Clear();
            client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
        }

        public IActionResult Register()
        {
            return View();
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Register(RegisterViewModel model)
        {
            if (ModelState.IsValid)
            {
                var checkUserName = _context.Customers.Where(p => p.CustomerEmail == model.Customer.CustomerEmail).FirstOrDefault();

                if (checkUserName == null)
                {
                    if (_context.Customers.Any() == false)
                    {
                        model.Customer.CustomerId = "CUS0000001";
                    }
                    else
                    {
                        var tam = _context.Customers.ToList().Count;
                        string Idtam = "";
                        var lastId = tam + 1;

                        var soChuSo = 0;
                        while (tam > 0)
                        {
                            tam = tam / 10;
                            soChuSo++;

                        }
                        int dem = 7 - soChuSo;
                        //Idtam += "CUS";
                        for (int i = 0; i < dem; i++)
                        {
                            Idtam = "0" + Idtam;
                        }
                        Idtam = "CUS" + Idtam + lastId;
                        model.Customer.CustomerId = Idtam;
                    }

                    await _context.Customers.AddAsync(new Customer()
                    {
                        CustomerId = model.Customer.CustomerId,
                        CustomerName = model.Customer.CustomerName,
                        CustomerEmail = model.Customer.CustomerEmail,
                        Address = model.Customer.Address,
                        Phone = model.Customer.Phone,
                        BirthDay = model.Customer.BirthDay,
                        LastUpdate = DateTime.Now,
                        IsDel = false
                    });
                    await _context.Logins.AddAsync(new Login() { LoginName = model.Customer.CustomerEmail, Password = model.Login.Password, RoleId = 2, ResetPassword = "1", LastUpdate = DateTime.Now, IsDel = false });
                    _context.SaveChanges();
                    //string resetCode = Guid.NewGuid().ToString();
                    //SendVerificationLinkEmail(model.Customer.CustomerEmail, model.Customer.CustomerId, "WelcomPage");
                    return RedirectToAction("Login", "Account");
                }
                else
                {
                    ModelState.AddModelError(string.Empty, "Email already exist");
                    return View(model);
                }
            }
            return View(model);
        }

        public IActionResult Login()
        {
            //LoginViewModel a = new LoginViewModel();
            return View();
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public IActionResult Login(Login objLogin)
        {
            if (ModelState.IsValid)
            {
                var obj = _context.Logins.Where(a => a.LoginName.Equals(objLogin.LoginName) && a.Password.Equals(objLogin.Password) && a.IsDel == false).FirstOrDefault();
                if (obj != null)
                {
                    HttpContext.Session.SetString("RoleId", obj.RoleId.ToString());
                    var obj1 = _context.Roles.Where(a => a.RoleId.Equals(obj.RoleId)).FirstOrDefault();
                    if (obj1.RoleId.Equals(1) || obj1.RoleId.Equals(3))
                    {
                        //TempData["UserName"] = obj.Username.ToString();
                        HttpContext.Session.SetString("UserName", obj.LoginName.ToString());

                        return RedirectToAction("Index", "AdminMenus", new { area = "Admin" });
                    }
                    else if (obj1.RoleId.Equals(2))
                    {
                        HttpContext.Session.SetString("UserName", obj.LoginName.ToString());
                        if (HttpContext.Session.GetString("IdSach") != null && HttpContext.Session.GetString("IdSach") != "")
                        {
                            return RedirectToAction("Details", "Saches", new { area = "", id = HttpContext.Session.GetString("IdSach") });
                        }
                        return RedirectToAction("Index", "Home", new { area = "" });
                    }
                }
            }
            return View(objLogin);
        }

        public IActionResult LogOut()
        {
            HttpContext.Session.SetString("UserName", "");
            HttpContext.Session.SetString("RoleId", "");
            HttpContext.Session.SetString("IdSach", "");
            return RedirectToAction("Index", "Home");
        }

        public IActionResult AccessDenied()
        {
            return View();
        }

        public IActionResult WelcomPage(string id)
        {
            if (HttpContext.Session.GetString("UserName") != null)
                return RedirectToAction("Login", "Account", new { area = "" });
            var user = _context.Logins.Where(p => p.LoginName == id).FirstOrDefault();
            var cus = _context.Customers.Where(p => p.CustomerEmail == id).FirstOrDefault();
            if (user.IsDel == false)
            {
                return RedirectToAction("Login", "Account");
            }
            cus.IsDel = false;
            _context.Update(cus);
            _context.SaveChanges();
            user.IsDel = false;
            _context.Update(user);
            _context.SaveChanges();
            return RedirectToAction("Login", "Account");
        }

        [NonAction]
        public void SendVerificationLinkEmail(string email, string activationCode, string emailFor)
        {
            var uriBuilder02 = new UriBuilder
            {
                Scheme = Request.Scheme,
                Host = Request.Host.Host,
                Port = Request.Host.Port.GetValueOrDefault(80),
                Path = $"/Account/WelcomPage/{email}"
            };
            var link = uriBuilder02.Uri.AbsoluteUri;
            var fromEmail = new MailAddress("phulajpro@gmail.com", "phu dep trai");
            var toEmail = new MailAddress(email);
            var fromEmailPassword = "haydoido1";
            string subject = "";
            string body = "";

            if (emailFor == "WelcomPage")
            {
                subject = "Tài khoản của bạn đã được tạo. Vui lòng kích hoạt tại link kèm theo!";
                body = "<br/><br/>Chúng tôi vui lòng thông báo tài khoản của bạn" +
                    " đã được tạo thành công. Vui lòng click vào link bên dưới để xác nhận" +
                    " <br/><br/><a href='" + link + "'> Kích hoạt tài khoản </a> ";
            }

            var smtp = new SmtpClient
            {
                Host = "smtp.gmail.com",
                Port = 587,
                EnableSsl = true,
                DeliveryMethod = SmtpDeliveryMethod.Network,
                UseDefaultCredentials = false,
                Credentials = new NetworkCredential(fromEmail.Address, fromEmailPassword)
            };

            using (var message = new MailMessage(fromEmail, toEmail)
            {
                Subject = subject,
                Body = body,
                IsBodyHtml = true
            })
                smtp.Send(message);
        }
    }
}