﻿using BookStore_Core.DAL;
using BookStore_Core.Models;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using System;
using System.Linq;
using System.Threading.Tasks;

namespace BookStore_Core.Areas.Admin.Controllers
{
    [Area("Admin")]
    [Route("Admin/Managers")]
    public class ManagersController : Controller
    {
        private readonly BookStoreContext _context;

        public ManagersController(BookStoreContext context)
        {
            _context = context;
        }
        [Route("index")]
        // GET: Admin/Managers
        public async Task<IActionResult> Index()
        {
            return View(await _context.Managers.Where(p => p.IsDel == false).ToListAsync());
        }
        [Route("details")]
        // GET: Admin/Managers/Details/5
        public async Task<IActionResult> Details(string id, string email)
        {
            if (id == null && email == null)
            {
                return NotFound();
            }

            var manager = await _context.Managers
                .FirstOrDefaultAsync(m => m.ManagerId == id || m.Email == email);
            if (manager == null)
            {
                return NotFound();
            }

            return View(manager);
        }
        [Route("create")]
        // GET: Admin/Managers/Create
        public IActionResult Create()
        {
            return View();
        }
        [Route("create")]
        // POST: Admin/Managers/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create([Bind("ManagerId,ManagerName,Phone,Address,BirthDay")] Manager manager)
        {
            if (ModelState.IsValid)
            {
                var checkUserName = _context.Managers.Where(p => p.Email == manager.Email).FirstOrDefault();

                if (checkUserName == null)
                {
                    if (_context.Managers.Any() == false)
                    {
                        manager.ManagerId = "MAG0000001";
                    }
                    else
                    {
                        var tam = _context.Managers.ToList().Count;
                        string Idtam = "";
                        var lastId = tam + 1;

                        var soChuSo = 0;
                        while (tam > 0)
                        {
                            tam = tam / 10;
                            soChuSo++;

                        }
                        int dem = 7 - soChuSo;
                        //Idtam += "CUS";
                        for (int i = 0; i < dem; i++)
                        {
                            Idtam = "0" + Idtam;
                        }
                        Idtam = "CUS" + Idtam + lastId;
                        manager.ManagerId = Idtam;
                    }
                    await _context.Managers.AddAsync(new Manager()
                    {
                        ManagerId = manager.ManagerId,
                        ManagerName = manager.ManagerName,
                        Email = manager.Email,
                        Address = manager.Address,
                        Phone = manager.Phone,
                        BirthDay = manager.BirthDay,
                        LastUpdate = DateTime.Now,
                        IsDel = false
                    });
                    await _context.Logins.AddAsync(new Login() { LoginName = manager.Email, Password = "123", RoleId = 2, ResetPassword = "1", LastUpdate = DateTime.Now, IsDel = true });
                    //_context.Add(manager);
                    await _context.SaveChangesAsync();
                    return RedirectToAction(nameof(Index));
                }
            }
            return View(manager);
        }
        [Route("edit")]
        // GET: Admin/Managers/Edit/5
        public async Task<IActionResult> Edit(string id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var manager = await _context.Managers.FindAsync(id);
            if (manager == null)
            {
                return NotFound();
            }
            return View(manager);
        }
        [Route("edit")]
        // POST: Admin/Managers/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(string id, Manager manager)
        {
            if (id != manager.ManagerId)
            {
                return NotFound();
            }

            if (ModelState.IsValid)
            {
                try
                {
                    manager.LastUpdate = DateTime.Now;
                    _context.Update(manager);
                    await _context.SaveChangesAsync();
                }
                catch (DbUpdateConcurrencyException)
                {
                    if (!ManagerExists(manager.ManagerId))
                    {
                        return NotFound();
                    }
                    else
                    {
                        throw;
                    }
                }
                return RedirectToAction(nameof(Index));
            }
            return View(manager);
        }
        [Route("delete")]
        // GET: Admin/Managers/Delete/5
        public async Task<IActionResult> Delete(string id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var manager = await _context.Managers
                .FirstOrDefaultAsync(m => m.ManagerId == id);
            if (manager == null)
            {
                return NotFound();
            }

            return View(manager);
        }
        [Route("delete")]
        // POST: Admin/Managers/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> DeleteConfirmed(string id)
        {
            var manager = await _context.Managers.SingleOrDefaultAsync(p => p.ManagerId == id);
            manager.IsDel = true;
            _context.Update(manager);
            await _context.SaveChangesAsync();
            return RedirectToAction(nameof(Index));
        }

        private bool ManagerExists(string id)
        {
            return _context.Managers.Any(e => e.ManagerId == id);
        }
    }
}
