﻿using BookStore_Core.DAL;
using BookStore_Core.Models;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using System;
using System.Linq;
using System.Threading.Tasks;

namespace BookStore_Core.Areas.Admin.Controllers
{
    [Area("Admin")]
    [Route("Admin/Employees")]
    public class EmployeesController : Controller
    {
        private readonly BookStoreContext _context;

        public EmployeesController(BookStoreContext context)
        {
            _context = context;
        }
        [Route("Index")]
        // GET: Admin/Employees
        public async Task<IActionResult> Index()
        {
            return View(await _context.Employees.Where(p => p.IsDel == false).ToListAsync());
        }
        [Route("Details")]
        // GET: Admin/Employees/Details/5
        public async Task<IActionResult> Details(string id, string email)
        {
            if (id == null && email == null)
            {
                return NotFound();
            }

            var employee = await _context.Employees
                .FirstOrDefaultAsync(m => m.EmployeeId == id || m.Email == email);
            if (employee == null)
            {
                return NotFound();
            }

            return View(employee);
        }
        [Route("Create")]
        // GET: Admin/Employees/Create
        public IActionResult Create()
        {
            return View();
        }
        [Route("Create")]
        // POST: Admin/Employees/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create([Bind("EmployeeId,EmployeeName,Email,Phone,Address,BirthDay,LastUpdate,IsDel")] Employee employee)
        {
            if (ModelState.IsValid)
            {
                var checkUserName = _context.Employees.Where(p => p.Email == employee.Email).FirstOrDefault();

                if (checkUserName == null)
                {
                    if (_context.Employees.Any() == false)
                    {
                        employee.EmployeeId = "EMP0000001";
                    }
                    else
                    {
                        var tam = _context.Employees.ToList().Count;
                        string Idtam = "";
                        var lastId = tam + 1;

                        var soChuSo = 0;
                        while (tam > 0)
                        {
                            tam = tam / 10;
                            soChuSo++;

                        }
                        int dem = 7 - soChuSo;
                        //Idtam += "CUS";
                        for (int i = 0; i < dem; i++)
                        {
                            Idtam = "0" + Idtam;
                        }
                        Idtam = "EMP" + Idtam + lastId;
                        employee.EmployeeId = Idtam;
                    }
                    await _context.Employees.AddAsync(new Employee()
                    {
                        EmployeeId = employee.EmployeeId,
                        EmployeeName = employee.EmployeeName,
                        Email = employee.Email,
                        Address = employee.Address,
                        Phone = employee.Phone,
                        BirthDay = employee.BirthDay,
                        LastUpdate = DateTime.Now,
                        IsDel = false
                    });
                    await _context.Logins.AddAsync(new Login() { LoginName = employee.Email, Password = "123", RoleId = 2, ResetPassword = "1", LastUpdate = DateTime.Now, IsDel = true });
                    //_context.Add(employee);
                    await _context.SaveChangesAsync();
                    return RedirectToAction(nameof(Index));
                }
            }
            return View(employee);
        }
        [Route("Edit")]
        // GET: Admin/Employees/Edit/5
        public async Task<IActionResult> Edit(string id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var employee = await _context.Employees.FindAsync(id);
            if (employee == null)
            {
                return NotFound();
            }
            return View(employee);
        }
        [Route("Edit")]
        // POST: Admin/Employees/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(string id, [Bind("EmployeeId,EmployeeName,Email,Phone,Address,BirthDay,IsDel")] Employee employee)
        {
            if (id != employee.EmployeeId)
            {
                return NotFound();
            }

            if (ModelState.IsValid)
            {
                try
                {
                    employee.LastUpdate = DateTime.Now;
                    _context.Update(employee);
                    await _context.SaveChangesAsync();
                }
                catch (DbUpdateConcurrencyException)
                {
                    if (!EmployeeExists(employee.EmployeeId))
                    {
                        return NotFound();
                    }
                    else
                    {
                        throw;
                    }
                }
                return RedirectToAction(nameof(Index));
            }
            return View(employee);
        }
        [Route("Delete")]
        // GET: Admin/Employees/Delete/5
        public async Task<IActionResult> Delete(string id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var employee = await _context.Employees
                .FirstOrDefaultAsync(m => m.EmployeeId == id);
            if (employee == null)
            {
                return NotFound();
            }

            return View(employee);
        }
        [Route("Delete")]
        // POST: Admin/Employees/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> DeleteConfirmed(string id)
        {
            var employee = await _context.Employees.SingleOrDefaultAsync(p => p.EmployeeId == id);
            employee.IsDel = true;
            _context.Update(employee);
            await _context.SaveChangesAsync();
            return RedirectToAction(nameof(Index));
        }

        [Route("NangCap")]
        // GET: Admin/Employees/Edit/5
        public async Task<IActionResult> NangCap(string id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var employee = await _context.Employees.FindAsync(id);
            if (employee == null)
            {
                return NotFound();
            }
            return View(employee);
        }
        [Route("NangCap")]
        // POST: Admin/Employees/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> NangCap(string id, Employee employee)
        {
            if (id != employee.EmployeeId)
            {
                return NotFound();
            }

            if (ModelState.IsValid)
            {
                try
                {
                    var emp = _context.Employees.SingleOrDefault(p => p.EmployeeId == id);
                    Manager manager = new Manager();
                    if (_context.Managers.Any() == false)
                    {
                        manager.ManagerId = "MAG0000001";
                    }
                    else
                    {
                        var tam = _context.Managers.ToList().Count;
                        string Idtam = "";
                        var lastId = tam + 1;

                        var soChuSo = 0;
                        while (tam > 0)
                        {
                            tam = tam / 10;
                            soChuSo++;

                        }
                        int dem = 7 - soChuSo;
                        //Idtam += "CUS";
                        for (int i = 0; i < dem; i++)
                        {
                            Idtam = "0" + Idtam;
                        }
                        Idtam = "CUS" + Idtam + lastId;

                        manager.ManagerId = Idtam;
                        emp.LastUpdate = DateTime.Now;
                        emp.IsDel = true;
                        _context.Update(emp);
                        await _context.SaveChangesAsync();

                        _context.Managers.Add(new Manager() { ManagerId = Idtam, Email = emp.Email, ManagerName = emp.EmployeeName, BirthDay = emp.BirthDay, Phone = emp.Phone, Address = emp.Address, LastUpdate = DateTime.Now });
                        await _context.SaveChangesAsync();
                        var login = _context.Logins.SingleOrDefault(p => p.LoginName == emp.Email);
                        login.RoleId = 1;
                        _context.Logins.Update(login);
                        await _context.SaveChangesAsync();
                        return RedirectToAction("Index", "Managers");
                    }
                }
                catch (DbUpdateConcurrencyException)
                {
                    if (!EmployeeExists(employee.EmployeeId))
                    {
                        return NotFound();
                    }
                    else
                    {
                        throw;
                    }
                }
                return RedirectToAction(nameof(Index));
            }
            return View(employee);
        }

        private bool EmployeeExists(string id)
        {
            return _context.Employees.Any(e => e.EmployeeId == id);
        }
    }
}
