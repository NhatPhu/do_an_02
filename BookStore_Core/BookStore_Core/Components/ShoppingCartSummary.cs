﻿using BookStore_Core.DAL;
using BookStore_Core.Models;
using BookStore_Core.ViewModel;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace BookStore_Core.Components
{
    public class ShoppingCartSummary : ViewComponent
    {
        private readonly BookStoreContext _context;
        public ShoppingCartSummary(BookStoreContext context)
        {
            _context = context;
        }

        public async Task<IViewComponentResult> InvokeAsync()
        {
            List<ShoppingCartViewModel> items = new List<ShoppingCartViewModel>();
            if (HttpContext.Session.GetString("UserName") != null && HttpContext.Session.GetString("UserName") != "" && HttpContext.Session.GetString("RoleId") != "1")
            {
                var EmailUser = HttpContext.Session.GetString("UserName").ToString();
                
                var userTam = await _context.Customers.Where(p => p.CustomerEmail == EmailUser).FirstOrDefaultAsync();
                
                //_shoppingCart.ShoppingCartItems = items;
                var cart = _context.Carts.Where(p => p.CustomerId == userTam.CustomerId);
                ViewBag.TongTien = 0;
                ViewBag.SoLuong = 0;
                foreach(var x in cart)
                {
                    var sach = _context.Saches.FirstOrDefault(p => p.SachId == x.SachId);
                    var gia = _context.BangGias.LastOrDefault(p => p.SachId == sach.SachId);
                    items.Add(new ShoppingCartViewModel() { Cart = x, Sach = sach, BangGia = gia, GiaTong = x.Amount * (gia.DonGia - gia.DonGia * gia.PhanTramKhuyenMai / 100) });
                    ViewBag.TongTien += x.Amount * (gia.DonGia - gia.DonGia * gia.PhanTramKhuyenMai / 100);
                    ViewBag.SoLuong += x.Amount;
                }
                return View(items);
            }
            else return View(items);
        }
    }
}
