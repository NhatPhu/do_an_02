﻿using BookStore_Core.Models;

namespace BookStore_Core.ViewModel
{
    public class RegisterViewModel
    {
        public Customer Customer { get; set; }
        public Login Login { get; set; }
        public string ConfirmPass { get; set; }
    }
}
