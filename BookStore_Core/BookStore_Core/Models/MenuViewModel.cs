﻿using System.Collections.Generic;

namespace BookStore_Core.Models
{
    public class MenuViewModel
    {
        public LoaiSachTongQuat LoaiSachTongQuat { get; set; }
        public List<LoaiSachCuThe> LoaiSachCuThe { get; set; }

        public MenuViewModel()
        {
            LoaiSachTongQuat = new LoaiSachTongQuat();
            LoaiSachCuThe = new List<LoaiSachCuThe>();
        }


    }
}
