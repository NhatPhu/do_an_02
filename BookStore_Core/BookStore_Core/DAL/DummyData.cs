﻿using BookStore_Core.Models;
using Microsoft.AspNetCore.Builder;
using Microsoft.Extensions.DependencyInjection;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace BookStore_Core.DAL
{
    public class DummyData
    {
        public static void Initialize(IApplicationBuilder app)
        {
            using (var serviceScope = app.ApplicationServices.GetService<IServiceScopeFactory>().CreateScope())
            {
                var context = serviceScope.ServiceProvider.GetService<BookStoreContext>();
                if (context.LoaiSachTongQuats.Any() == false)
                {
                    var loaiSachTongQuats = DummyData.SetLoaiSachTongQuat().ToArray();
                    context.LoaiSachTongQuats.AddRange(loaiSachTongQuats);
                    context.SaveChanges();
                }
                if (context.LoaiSachCuThes.Any() == false)
                {
                    var loaiSachCuThes = DummyData.SetLoaiSachCuThe().ToArray();
                    context.LoaiSachCuThes.AddRange(loaiSachCuThes);
                    context.SaveChanges();
                }
                if (context.LoaiTacGiaTongQuats.Any() == false)
                {
                    var loaiTacGiaTQs = DummyData.SetLoaiTacGiaTQ().ToArray();
                    context.LoaiTacGiaTongQuats.AddRange(loaiTacGiaTQs);
                    context.SaveChanges();
                }
                if (context.TacGias.Any() == false)
                {
                    var tacGias = DummyData.SetTacGia().ToArray();
                    context.TacGias.AddRange(tacGias);
                    context.SaveChanges();
                }
                if (context.Roles.Any() == false)
                {
                    var role = DummyData.SetRole().ToArray();
                    context.Roles.AddRange(role);
                    context.SaveChanges();
                }
                if(context.Saches.Any() == false)
                {
                    var sach = DummyData.SetSach().ToArray();
                    context.Saches.AddRange(sach);
                    context.SaveChanges();
                }
                if (context.BangGias.Any() == false)
                {
                    var bangGia = DummyData.SetBangGia().ToArray();
                    context.BangGias.AddRange(bangGia);
                    context.SaveChanges();
                }
                if (context.Logins.Any() == false)
                {
                    var login = DummyData.SetLogin().ToArray();
                    context.Logins.AddRange(login);
                    var customer = DummyData.SetCustomer().ToArray();
                    context.Customers.AddRange(customer);
                    var manager = DummyData.SetManager().ToArray();
                    context.Managers.AddRange(manager);
                    context.SaveChanges();
                }
                if(context.KhuyenMais.Any() == false)
                {
                    var khuyenMai = DummyData.SetKhuyenMai().ToArray();
                    context.KhuyenMais.AddRange(khuyenMai);
                    context.SaveChanges();
                }
                if(context.AnhPhus.Any() == false)
                {
                    var anhPhu = DummyData.SetAnhPhu().ToArray();
                    context.AnhPhus.AddRange(anhPhu);
                    context.SaveChanges();
                }
                if (context.Employees.Any() == false)
                {
                    var employees = DummyData.SetEmployee().ToArray();
                    context.Employees.AddRange(employees);
                    context.SaveChanges();
                }
            }
        }

        public static List<Role> SetRole()
        {
            List<Role> role = new List<Role>()
                {
                    new Role {RoleName = "Admin"},
                    new Role {RoleName = "Customer"},
                    new Role {RoleName = "Employeer"}
                };
            return role;
        }
        public static List<Login> SetLogin()
        {
            List<Login> login = new List<Login>()
                {
                    new Login {LoginName = "phulajpro@gmail.com", Password = "1", RoleId = 1, LastUpdate = DateTime.Now},
                    new Login {LoginName = "phulaj@gmail.com", Password = "1", RoleId = 2, LastUpdate = DateTime.Now},
                    new Login {LoginName = "phu@gmail.com", Password = "1", RoleId = 3, LastUpdate = DateTime.Now}
                };
            return login;
        }
        public static List<Customer> SetCustomer()
        {
            List<Customer> customer = new List<Customer>()
                {
                    new Customer {CustomerId = "CUS0000001",CustomerEmail = "phulaj@gmail.com",CustomerName = "Nhat Phu", Address = "Khánh Hòa",BirthDay = Convert.ToDateTime("1998-08-25"),Phone = "0834238897", LastUpdate = DateTime.Now}
                };
            return customer;
        }
        public static List<Manager> SetManager()
        {
            List<Manager> manager = new List<Manager>()
                {
                    new Manager {ManagerId = "MAG0000001",Email="phulajpro@gmail.com", ManagerName = "Nhật Phú Admin", Address = "Khánh Hòa", BirthDay = Convert.ToDateTime("1998-08-25"),Phone = "0834238897", LastUpdate = DateTime.Now}
                };
            return manager;
        }
        public static List<Employee> SetEmployee()
        {
            List<Employee> employees = new List<Employee>()
            {
                new Employee() {EmployeeId = "EMP0000001", Email = "phu@gmail.com", EmployeeName = "Phú đẹp trai", Address = "Khánh hòa", BirthDay = Convert.ToDateTime("1998-08-25"),Phone = "0834238897", LastUpdate = DateTime.Now}
            };
            return employees;
        }
        public static List<LoaiSachTongQuat> SetLoaiSachTongQuat()
        {
            List<LoaiSachTongQuat> loaiSachTongQuats = new List<LoaiSachTongQuat>() {
                new LoaiSachTongQuat { LoaiSachTQId = "1", TenLoaiSachTQ = "Văn học"},
                new LoaiSachTongQuat { LoaiSachTQId = "2", TenLoaiSachTQ = "Kinh tế"},
                new LoaiSachTongQuat { LoaiSachTQId = "3", TenLoaiSachTQ = "Tâm lý - Kỹ năng sống"},
                new LoaiSachTongQuat { LoaiSachTQId = "4", TenLoaiSachTQ = "Nuôi dạy con"},
                new LoaiSachTongQuat { LoaiSachTQId = "5", TenLoaiSachTQ = "Sách thiếu nhi"},
                new LoaiSachTongQuat { LoaiSachTQId = "6", TenLoaiSachTQ = "Tiểu sử - Hồi ký"},
                new LoaiSachTongQuat { LoaiSachTQId = "7", TenLoaiSachTQ = "Sách giáo khoa - Tham khảo"},
                new LoaiSachTongQuat { LoaiSachTQId = "8", TenLoaiSachTQ = "Sách ngoại ngữ"},
            };
            return loaiSachTongQuats;
        }
        public static List<LoaiSachCuThe> SetLoaiSachCuThe()
        {
            List<LoaiSachCuThe> loaiSachCuThes = new List<LoaiSachCuThe>() {
                new LoaiSachCuThe { LoaiSachCTId = "1", TenLoaiSachCT = "Tiểu thuyết", LoaiSachTQId = "1" }, //1
                new LoaiSachCuThe { LoaiSachCTId = "2", TenLoaiSachCT = "Truyện ngắn - Tản văn", LoaiSachTQId = "1" }, //2
                new LoaiSachCuThe { LoaiSachCTId = "3", TenLoaiSachCT = "Light Novel", LoaiSachTQId = "1" },//3
                new LoaiSachCuThe { LoaiSachCTId = "4", TenLoaiSachCT = "Ngôn tình", LoaiSachTQId = "1" },//4
                new LoaiSachCuThe { LoaiSachCTId = "5", TenLoaiSachCT = "Truyện trinh thám - Kiếm hiệp", LoaiSachTQId = "1" },//5
                new LoaiSachCuThe { LoaiSachCTId = "6", TenLoaiSachCT = "Tác phẩm kinh điển", LoaiSachTQId = "1" },//6
                new LoaiSachCuThe { LoaiSachCTId = "7", TenLoaiSachCT = "Nhân Vật - Bài Học Kinh Doanh", LoaiSachTQId = "2" },//7
                new LoaiSachCuThe { LoaiSachCTId = "8", TenLoaiSachCT = "Quản trị - Lãnh đạo", LoaiSachTQId = "2" },//8
                new LoaiSachCuThe { LoaiSachCTId = "9", TenLoaiSachCT = "Marketing - Bán hàng", LoaiSachTQId = "2"},//9
                new LoaiSachCuThe { LoaiSachCTId = "10", TenLoaiSachCT = "Phân tích kinh tế", LoaiSachTQId = "2"},//10
                new LoaiSachCuThe { LoaiSachCTId = "11", TenLoaiSachCT = "Kỹ năng sống", LoaiSachTQId = "3"},//11
                new LoaiSachCuThe { LoaiSachCTId = "12", TenLoaiSachCT = "Rèn luyện nhân cách", LoaiSachTQId = "3"},//12
                new LoaiSachCuThe { LoaiSachCTId = "13", TenLoaiSachCT = "Tâm lý", LoaiSachTQId = "3"},//13
                new LoaiSachCuThe { LoaiSachCTId = "14", TenLoaiSachCT = "Sách cho tuổi mới lớn", LoaiSachTQId = "3" }, //14
                new LoaiSachCuThe { LoaiSachCTId = "15", TenLoaiSachCT = "Cẩm nang làm cha mẹ", LoaiSachTQId = "4" }, //15
                new LoaiSachCuThe { LoaiSachCTId = "16", TenLoaiSachCT = "Cách giáo dục trẻ các nước", LoaiSachTQId = "4" },//16
                new LoaiSachCuThe { LoaiSachCTId = "17", TenLoaiSachCT = "Phát triển trí tuệ cho trẻ", LoaiSachTQId = "4" },//17
                new LoaiSachCuThe { LoaiSachCTId = "18", TenLoaiSachCT = "Phát triển kỹ năng cho trẻ", LoaiSachTQId = "4" },//18
                new LoaiSachCuThe { LoaiSachCTId = "19", TenLoaiSachCT = "Manga - Comic", LoaiSachTQId = "5" },//19
                new LoaiSachCuThe { LoaiSachCTId = "20", TenLoaiSachCT = "Kiến thức bách khoa", LoaiSachTQId = "5" },//20
                new LoaiSachCuThe { LoaiSachCTId = "21", TenLoaiSachCT = "Sách tranh kỹ năng sống", LoaiSachTQId = "5" },//21
                new LoaiSachCuThe { LoaiSachCTId = "22", TenLoaiSachCT = "Vừa hoc - Vừa chơi với trẻ", LoaiSachTQId = "5" },//22
                new LoaiSachCuThe { LoaiSachCTId = "23", TenLoaiSachCT = "Câu chuyện cuộc đời", LoaiSachTQId = "6" },//23
                new LoaiSachCuThe { LoaiSachCTId = "24", TenLoaiSachCT = "Chính trị", LoaiSachTQId = "6" },//24
                new LoaiSachCuThe { LoaiSachCTId = "25", TenLoaiSachCT = "Kinh tế", LoaiSachTQId = "6" },//25
                new LoaiSachCuThe { LoaiSachCTId = "26", TenLoaiSachCT = "Nghệ thuật - Giải trí", LoaiSachTQId = "6" },//26
                new LoaiSachCuThe { LoaiSachCTId = "27", TenLoaiSachCT = "Cấp 1", LoaiSachTQId = "7" },//27
                new LoaiSachCuThe { LoaiSachCTId = "28", TenLoaiSachCT = "Cấp 2", LoaiSachTQId = "7" },//28
                new LoaiSachCuThe { LoaiSachCTId = "29", TenLoaiSachCT = "Cấp 3", LoaiSachTQId = "7" },//29
                new LoaiSachCuThe { LoaiSachCTId = "30", TenLoaiSachCT = "Luyện thi ĐH - CĐ", LoaiSachTQId = "7" },//30
                new LoaiSachCuThe { LoaiSachCTId = "31", TenLoaiSachCT = "Tiếng Anh", LoaiSachTQId = "8" },//31
                new LoaiSachCuThe { LoaiSachCTId = "32", TenLoaiSachCT = "Tiếng Nhật", LoaiSachTQId = "8" },//32
                new LoaiSachCuThe { LoaiSachCTId = "33", TenLoaiSachCT = "Tiếng Hoa", LoaiSachTQId = "8" },//33
                new LoaiSachCuThe { LoaiSachCTId = "34", TenLoaiSachCT = "Tiếng Hàn", LoaiSachTQId = "8" },//34
                new LoaiSachCuThe { LoaiSachCTId = "35", TenLoaiSachCT = "Tiếng Pháp", LoaiSachTQId = "8" },//35
                new LoaiSachCuThe { LoaiSachCTId = "36", TenLoaiSachCT = "Tiếng Đức", LoaiSachTQId = "8" },//36
                new LoaiSachCuThe { LoaiSachCTId = "37", TenLoaiSachCT = "Ngoại ngữ khác", LoaiSachTQId = "8" },//37
                new LoaiSachCuThe { LoaiSachCTId = "38", TenLoaiSachCT = "Luyện thi chứng chỉ A, B, C", LoaiSachTQId = "8" },//38
                new LoaiSachCuThe { LoaiSachCTId = "39", TenLoaiSachCT = "Tiếng Việt cho người nước ngoài", LoaiSachTQId = "8" }//39
            };
            return loaiSachCuThes;
        }
        public static List<LoaiTacGiaTQ> SetLoaiTacGiaTQ()
        {
            List<LoaiTacGiaTQ> loaiTacGiaTQ = new List<LoaiTacGiaTQ>() {
                new LoaiTacGiaTQ { TenLoaiTacGiaTQ = "Tác giả Việt Nam" },
                new LoaiTacGiaTQ { TenLoaiTacGiaTQ = "Tác giả nước ngoài" }
            };
            return loaiTacGiaTQ;
        }
        public static List<TacGia> SetTacGia()
        {
            List<TacGia> tacGia = new List<TacGia>() {
                new TacGia { TacGiaId = "1", TenTacGia = "Không có tác giả", LoaiTacGiaTQId = 1 },
                new TacGia { TacGiaId = "2", TenTacGia = "Nguyễn Nhật Ánh", LoaiTacGiaTQId = 1 },
                new TacGia { TacGiaId = "3", TenTacGia = "Nguyễn Ngọc Tư", LoaiTacGiaTQId = 1 },
                new TacGia { TacGiaId = "4", TenTacGia = "Hạ Vũ", LoaiTacGiaTQId = 1 },
                new TacGia { TacGiaId = "5", TenTacGia = "Minh Nhật", LoaiTacGiaTQId = 1 },
                new TacGia { TacGiaId = "6", TenTacGia = "Hamlet Trương", LoaiTacGiaTQId = 1 },
                new TacGia { TacGiaId = "7", TenTacGia = "Du Phong", LoaiTacGiaTQId = 1 },
                new TacGia { TacGiaId = "8", TenTacGia = "Gào", LoaiTacGiaTQId = 1 },
                new TacGia { TacGiaId = "9", TenTacGia = "Nguyễn Ngọc Thạch", LoaiTacGiaTQId = 1 },
                new TacGia { TacGiaId = "10", TenTacGia = "Anh Khang", LoaiTacGiaTQId = 1 },
                new TacGia { TacGiaId = "11", TenTacGia = "Huyền Trang Bất Hối", LoaiTacGiaTQId = 1 },//11
                new TacGia { TacGiaId = "12", TenTacGia = "Dương Thụy", LoaiTacGiaTQId = 1 }, //12
                new TacGia { TacGiaId = "13", TenTacGia = "Phan Ý Yên", LoaiTacGiaTQId = 1 }, //13
                new TacGia { TacGiaId = "14", TenTacGia = "Thu Hà", LoaiTacGiaTQId = 1 }, //14
                new TacGia { TacGiaId = "15", TenTacGia = "Whitney Stewart, Nancy Harrison", LoaiTacGiaTQId = 2 }, //15
                new TacGia { TacGiaId = "16", TenTacGia = "ChungBe Studios , Oh, Youngseok", LoaiTacGiaTQId = 2 }, //16
                new TacGia { TacGiaId = "17", TenTacGia = "Daniel J. Siegel & Tina Payne Bryson", LoaiTacGiaTQId = 2 }, //17
                new TacGia { TacGiaId = "18", TenTacGia = "Trần Đại Vỹ, Ngô Khu", LoaiTacGiaTQId = 1 }, //18
                new TacGia { TacGiaId = "19", TenTacGia = "Trần Hân, Thanh Nhã", LoaiTacGiaTQId = 1 }, //19
                new TacGia { TacGiaId = "20", TenTacGia = "Phạm Hồng", LoaiTacGiaTQId = 1 }, //20
                new TacGia { TacGiaId = "21", TenTacGia = "Reiko Uchida", LoaiTacGiaTQId = 2 }, //21
                new TacGia { TacGiaId = "22", TenTacGia = "Bando Mariko", LoaiTacGiaTQId = 2 }, //22
                new TacGia { TacGiaId = "23", TenTacGia = "Anna Bykova", LoaiTacGiaTQId = 2 }, //23
                new TacGia { TacGiaId = "24", TenTacGia = "Dr C L Claridge", LoaiTacGiaTQId = 2 }, //24
                new TacGia { TacGiaId = "25", TenTacGia = "Nhiều tác giả", LoaiTacGiaTQId = 1 }, //25
                new TacGia { TacGiaId = "26", TenTacGia = "Hà Thanh Phúc", LoaiTacGiaTQId = 1 }, //26
                new TacGia { TacGiaId = "27", TenTacGia = "Trí", LoaiTacGiaTQId = 1 }, //27
                new TacGia { TacGiaId = "28", TenTacGia = "Tuệ Mẫn", LoaiTacGiaTQId = 1 }, //28
                new TacGia { TacGiaId = "29", TenTacGia = "Nguyên Bảo", LoaiTacGiaTQId = 1 },// 29
                new TacGia { TacGiaId = "30", TenTacGia = "Reki Kawahara", LoaiTacGiaTQId = 2 }//30
            };
            return tacGia;
        }
        public static List<Sach> SetSach()
        {
            List<Sach> sachs = new List<Sach>()
            {
                new Sach { SachId = "SACH00000001", TrongLuong = 30, SoTrang = 100, LoaiBia = "Mềm", KichThuoc = "30x40", NgayTao = DateTime.Today, LoaiSachCTId = "1", TacGiaId = "2", HinhAnh = "SACH00000001.jpg", TenSach = "Tôi thấy hoa vàng trên cỏ xanh", MoTa ="Những câu chuyện nhỏ xảy ra ở một ngôi làng nhỏ: chuyện người, chuyện cóc, chuyện ma, chuyện công chúa và hoàng tử , rồi chuyện đói ăn, cháy nhà, lụt lội,... " },
                new Sach { SachId = "SACH00000002", TrongLuong = 30, SoTrang = 100, LoaiBia = "Mềm", KichThuoc = "30x40", NgayTao = DateTime.Today, LoaiSachCTId = "1", TacGiaId = "2", HinhAnh = "SACH00000002.jpg", TenSach = "Làm bạn với bầu trời", MoTa = "Một câu chuyện giản dị, chứa đầy bất ngờ cho tới trang cuối cùng. Và đẹp lộng lẫy, vì lòng vị tha và tình yêu thương, khiến mắt rưng rưng vì một nỗi mừng vui hân hoan. Cuốn sách như một đốm lửa thắp lên lòng khát khao sống tốt trên đời."},
                new Sach { SachId = "SACH00000003", TrongLuong = 30, SoTrang = 100, LoaiBia = "Mềm", KichThuoc = "30x40", NgayTao = DateTime.Today, LoaiSachCTId = "20", TacGiaId = "15", HinhAnh = "SACH00000003.jpg", TenSach = "BỘ SÁCH CHÂN DUNG NHỮNG NGƯỜI THAY ĐỔI THẾ GIỚI - WALT DISNEY LÀ AI? (TÁI BẢN 2019)", MoTa = "Nếu bạn dám mơ, bạn sẽ làm được."},
                new Sach { SachId = "SACH00000004", TrongLuong = 30, SoTrang = 100, LoaiBia = "Mềm", KichThuoc = "30x40", NgayTao = DateTime.Today, LoaiSachCTId = "20", TacGiaId = "1", HinhAnh = "SACH00000004.jpg", TenSach = "BỘ SÁCH CHÂN DUNG NHỮNG NGƯỜI THAY ĐỔI THẾ GIỚI - DR. SEUSS LÀ AI?", MoTa = "Bộ sách “Đó là ai?” cung cấp rất nhiều thông tin thú vị và tuyệt vời về những nhân vật nổi tiếng trong lịch sử lẫn hiện tại.. Marie Curie chắc chắn là cái tên nổi tiếng và rất quen thuộc với chúng ta."},
                new Sach { SachId = "SACH00000005", TrongLuong = 30, SoTrang = 100, LoaiBia = "Mềm", KichThuoc = "30x40", NgayTao = DateTime.Today, LoaiSachCTId = "20", TacGiaId = "16", HinhAnh = "SACH00000005.jpg", TenSach = "WHO? CHUYỆN KỂ VỀ DANH NHÂN THẾ GIỚI: COCO CHANEL (TÁI BẢN 2019)", MoTa = "Cuốn sách viết về cuộc đời và sự nghiệp của nhà thiết kế thời trang nổi tiếng thế giới người Pháp Coco Chanel."},
                new Sach { SachId = "SACH00000006", TrongLuong = 30, SoTrang = 100, LoaiBia = "Mềm", KichThuoc = "30x40", NgayTao = DateTime.Today, LoaiSachCTId = "15", TacGiaId = "17", HinhAnh = "SACH00000006.jpg", TenSach = "PHƯƠNG PHÁP DẠY CON KHÔNG ĐÒN ROI", MoTa = "Có bao giờ bạn tự hỏi mình, đặc biệt là sau mỗi cuộc đối thoại tuyệt vọng với bọn trẻ, “Mình không thể làm tốt hơn được sao?"},
                new Sach { SachId = "SACH00000007", TrongLuong = 30, SoTrang = 100, LoaiBia = "Mềm", KichThuoc = "30x40", NgayTao = DateTime.Today, LoaiSachCTId = "15", TacGiaId = "18", HinhAnh = "SACH00000007.jpg", TenSach = "101 CÁCH DẠY CON THÀNH TÀI", MoTa = "Bố mẹ ai cũng muốn con mình trở thành người tài giỏi, nhưng làm thế nào để đứa trẻ hoàn thành tốt sứ mệnh của mỗi chúng ta?"},
                new Sach { SachId = "SACH00000008", TrongLuong = 30, SoTrang = 100, LoaiBia = "Mềm", KichThuoc = "30x40", NgayTao = DateTime.Today, LoaiSachCTId = "15", TacGiaId = "19", HinhAnh = "SACH00000008.jpg", TenSach = "PHƯƠNG PHÁP GIÁO DỤC CON CỦA NGƯỜI DO THÁI", MoTa = "Người Do Thái là dân tộc thông minh nhất thế giới, họ dường như được sinh ra là để làm chủ thế giới này."},
                new Sach { SachId = "SACH00000009", TrongLuong = 30, SoTrang = 100, LoaiBia = "Mềm", KichThuoc = "30x40", NgayTao = DateTime.Today, LoaiSachCTId = "15", TacGiaId = "20", HinhAnh = "SACH00000009.jpg", TenSach = "PHƯƠNG PHÁP DẠY CON THÀNH TÀI CỦA NGƯỜI DO THÁI (TRÍ HUỆ DẠY CON THÀNH CÔNG NHẤT THẾ GIỚI)", MoTa = "Cùng với sự tiến bộ của xã hội, con người ngày càng coi trọng giáo dục hơn. Đối  ới đứa trẻ đang trong quá trình phát triển mà nói, con đường tương lai của nó ra sao, một phần rất lớn nằm trong tay cha mẹ."},
                new Sach { SachId = "SACH00000010", TrongLuong = 30, SoTrang = 100, LoaiBia = "Mềm", KichThuoc = "30x40", NgayTao = DateTime.Today, LoaiSachCTId = "15", TacGiaId = "21", HinhAnh = "SACH00000010.jpg", TenSach = "TUỔI DẬY THÌ NÓI GÌ VỚI CON?", MoTa = "Ngôn từ có một sức mạnh to lớn đối với con người. Ngay cả với con cái, mỗi lời nói của bố mẹ dù tốt hay xấu đều mang đến những ảnh hưởng vô cùng to lớn."},
                new Sach { SachId = "SACH00000011", TrongLuong = 30, SoTrang = 100, LoaiBia = "Mềm", KichThuoc = "30x40", NgayTao = DateTime.Today, LoaiSachCTId = "15", TacGiaId = "22", HinhAnh = "SACH00000011.jpg", TenSach = "PHẨM CÁCH CHA MẸ", MoTa = "Cuốn sách Phẩm cách cha mẹ của tác giả Bando Mariko gồm 7 chương: Giáo dục sinh mệnh (bắt đầu từ lời chào hỏi,  đến việc cả gia đình cùng ăn cơm, cùng con ca hát, đọc sách, khám phá thiên nhiên)"},
                new Sach { SachId = "SACH00000012", TrongLuong = 30, SoTrang = 100, LoaiBia = "Mềm", KichThuoc = "30x40", NgayTao = DateTime.Today, LoaiSachCTId = "15", TacGiaId = "14", HinhAnh = "SACH00000012.jpg", TenSach = "CON NGHĨ ĐI, MẸ KHÔNG BIẾT (TÁI BẢN 2019)", MoTa = "Con nghĩ đi, mẹ không biết là tập hợp những bài viết được đón nhận nhiệt thành trên Facebook của Thu Hà (Mẹ Xu-Sim), rất nhiều bài viết trong số đó đã từng được đăng trên các báo Tuổi trẻ, Thanh Niên, Vnexpress, Dântrí"},
                new Sach { SachId = "SACH00000013", TrongLuong = 30, SoTrang = 100, LoaiBia = "Mềm", KichThuoc = "30x40", NgayTao = DateTime.Today, LoaiSachCTId = "15", TacGiaId = "23", HinhAnh = "SACH00000013.jpg", TenSach = "MẸ BIẾT LƯỜI CON NÊN NGƯỜI", MoTa = "Những rối loạn thể hiện ở sự non nớt trong suy nghĩ, tâm lý, hành vi của những người trẻ tuổi đã trở thành một vấn đề thực sự đáng lo ngại trong xã hội hiện nay."},
                new Sach { SachId = "SACH00000014", TrongLuong = 30, SoTrang = 100, LoaiBia = "Mềm", KichThuoc = "30x40", NgayTao = DateTime.Today, LoaiSachCTId = "15", TacGiaId = "24", HinhAnh = "SACH00000014.jpg", TenSach = "NUÔI DẠY CON BẰNG TRÁI TIM CỦA MỘT VỊ PHẬT", MoTa = "Đức Phật đã dạy “Gia đình là nơi tâm trí này sống với tâm trí khác. Nếu chúng yêu thương nhau thì ngôi nhà sẽ đẹp như một vườn hoa. Ngược lại, nếu chúng bất hòa thì sẽ như cơn bão  tàn phá khu vườn.”"},
                new Sach { SachId = "SACH00000015", TrongLuong = 30, SoTrang = 100, LoaiBia = "Mềm", KichThuoc = "30x40", NgayTao = DateTime.Today, LoaiSachCTId = "15", TacGiaId = "25", HinhAnh = "SACH00000015.jpg", TenSach = "MẸ ƠI, Ở ĐÂU CON MỚI ĐƯỢC AN TOÀN?", MoTa = "Cuốn sách tập hợp những bài viết về các tình huống do cha mẹ, ông bà, thầy cô giáo, những người giám hộ, trông giữ trẻ vì một chút sơ ý, bất cẩn, tắc trách đẩy trẻ vào những tình thế nguy hiểm…"},
                new Sach { SachId = "SACH00000016", TrongLuong = 30, SoTrang = 100, LoaiBia = "Mềm", KichThuoc = "30x40", NgayTao = DateTime.Today, LoaiSachCTId = "2", TacGiaId = "26", HinhAnh = "SACH00000016.jpg", TenSach = "CHÚNG TA KHÔNG CÓ SAU NÀY", MoTa = "Một câu chuyện giản dị, chứa đầy bất ngờ cho tới trang cuối cùng. Và đẹp lộng lẫy, vì lòng vị tha và tình yêu thương, khiến mắt rưng rưng vì một nỗi mừng vui hân hoan. Cuốn sách như một đốm lửa thắp lên lòng khát khao sống tốt trên đời."},
                new Sach { SachId = "SACH00000017", TrongLuong = 30, SoTrang = 100, LoaiBia = "Mềm", KichThuoc = "30x40", NgayTao = DateTime.Today, LoaiSachCTId = "2", TacGiaId = "27", HinhAnh = "SACH00000017.jpg", TenSach = "NGHE NÓI ANH MUỐN CHIA TAY", MoTa = "Cảm giác của những ngày sau chia tay cũng giống như khi bạn đứng ở một góc tối nhìn ra thành phố rực rỡ ánh đèn vậy. Vô cùng lạc lõng, vô cùng đơn độc."},
                new Sach { SachId = "SACH00000018", TrongLuong = 30, SoTrang = 100, LoaiBia = "Mềm", KichThuoc = "30x40", NgayTao = DateTime.Today, LoaiSachCTId = "2", TacGiaId = "28", HinhAnh = "SACH00000018.jpg", TenSach = "ANH ĐÃ QUÊN EM CHƯA?", MoTa = "Này cô gái, em nghĩ mình sẽ mất bao lâu để quên người ấy sau khi chia tay?. Một tuần, một tháng, một năm, hay không bao giờ?"},
                new Sach { SachId = "SACH00000019", TrongLuong = 30, SoTrang = 100, LoaiBia = "Mềm", KichThuoc = "30x40", NgayTao = DateTime.Today, LoaiSachCTId = "2", TacGiaId = "29", HinhAnh = "SACH00000019.jpg", TenSach = "NGƯỜI THƯƠNG ĐÃ CŨ", MoTa = "Cuốn sách là những dòng tản mạn về tình yêu trong quá khứ với nhiều day dứt và trân trọng. Đó là những câu chuyện không trọn vẹn, nhưng được kể lại khi mà vết xước ngày cũ đã thôi cồn cào."},
                new Sach { SachId = "SACH00000020", TrongLuong = 30, SoTrang = 100, LoaiBia = "Mềm", KichThuoc = "30x40", NgayTao = DateTime.Today, LoaiSachCTId = "3", TacGiaId = "30", HinhAnh = "SACH00000020.jpg", TenSach = "SWORD ART ONLINE 13", MoTa = "“Nở ra, hoa hồng!”. Kirito và Eugeo cùng lên thẳng tầng cao nhất của tòa tháp trắng Central Cathedral, biểu tượng của Giáo hội Công lý, cũng là nơi ở của tư tế tối cao Administrator.."},
                //new Sach { SachId = "SACH00000021", LoaiSachCTId = 1, TacGiaId =  1, HinhAnh = "SACH00000021.jpg", TenSach = "Làm bạn với bầu trời", MoTa = "Một câu chuyện giản dị, chứa đầy bất ngờ cho tới trang cuối cùng. Và đẹp lộng lẫy, vì lòng vị tha và tình yêu thương, khiến mắt rưng rưng vì một nỗi mừng vui hân hoan. Cuốn sách như một đốm lửa thắp lên lòng khát khao sống tốt trên đời."},
                //new Sach { SachId = "SACH00000022", LoaiSachCTId = 1, TacGiaId =  1, HinhAnh = "SACH00000022.jpg", TenSach = "Làm bạn với bầu trời", MoTa = "Một câu chuyện giản dị, chứa đầy bất ngờ cho tới trang cuối cùng. Và đẹp lộng lẫy, vì lòng vị tha và tình yêu thương, khiến mắt rưng rưng vì một nỗi mừng vui hân hoan. Cuốn sách như một đốm lửa thắp lên lòng khát khao sống tốt trên đời."},
                //new Sach { SachId = "SACH00000023", LoaiSachCTId = 1, TacGiaId =  1, HinhAnh = "SACH00000023.png", TenSach = "Làm bạn với bầu trời", MoTa = "Một câu chuyện giản dị, chứa đầy bất ngờ cho tới trang cuối cùng. Và đẹp lộng lẫy, vì lòng vị tha và tình yêu thương, khiến mắt rưng rưng vì một nỗi mừng vui hân hoan. Cuốn sách như một đốm lửa thắp lên lòng khát khao sống tốt trên đời."},
                //new Sach { SachId = "SACH00000024", LoaiSachCTId = 1, TacGiaId =  1, HinhAnh = "SACH00000024.png", TenSach = "Làm bạn với bầu trời", MoTa = "Một câu chuyện giản dị, chứa đầy bất ngờ cho tới trang cuối cùng. Và đẹp lộng lẫy, vì lòng vị tha và tình yêu thương, khiến mắt rưng rưng vì một nỗi mừng vui hân hoan. Cuốn sách như một đốm lửa thắp lên lòng khát khao sống tốt trên đời."},
                //new Sach { SachId = "SACH00000025", LoaiSachCTId = 1, TacGiaId =  1, HinhAnh = "SACH00000025.png", TenSach = "Làm bạn với bầu trời", MoTa = "Một câu chuyện giản dị, chứa đầy bất ngờ cho tới trang cuối cùng. Và đẹp lộng lẫy, vì lòng vị tha và tình yêu thương, khiến mắt rưng rưng vì một nỗi mừng vui hân hoan. Cuốn sách như một đốm lửa thắp lên lòng khát khao sống tốt trên đời."},
                //new Sach { SachId = "SACH00000026", LoaiSachCTId = 1, TacGiaId =  1, HinhAnh = "SACH00000026.png", TenSach = "Làm bạn với bầu trời", MoTa = "Một câu chuyện giản dị, chứa đầy bất ngờ cho tới trang cuối cùng. Và đẹp lộng lẫy, vì lòng vị tha và tình yêu thương, khiến mắt rưng rưng vì một nỗi mừng vui hân hoan. Cuốn sách như một đốm lửa thắp lên lòng khát khao sống tốt trên đời."},
                //new Sach { SachId = "SACH00000027", LoaiSachCTId = 1, TacGiaId =  1, HinhAnh = "SACH00000027.png", TenSach = "Làm bạn với bầu trời", MoTa = "Một câu chuyện giản dị, chứa đầy bất ngờ cho tới trang cuối cùng. Và đẹp lộng lẫy, vì lòng vị tha và tình yêu thương, khiến mắt rưng rưng vì một nỗi mừng vui hân hoan. Cuốn sách như một đốm lửa thắp lên lòng khát khao sống tốt trên đời."},
                //new Sach { SachId = "SACH00000028", LoaiSachCTId = 1, TacGiaId =  1, HinhAnh = "SACH00000028.png", TenSach = "Làm bạn với bầu trời", MoTa = "Một câu chuyện giản dị, chứa đầy bất ngờ cho tới trang cuối cùng. Và đẹp lộng lẫy, vì lòng vị tha và tình yêu thương, khiến mắt rưng rưng vì một nỗi mừng vui hân hoan. Cuốn sách như một đốm lửa thắp lên lòng khát khao sống tốt trên đời."},
                //new Sach { SachId = "SACH00000029", LoaiSachCTId = 1, TacGiaId =  1, HinhAnh = "SACH00000029.png", TenSach = "Làm bạn với bầu trời", MoTa = "Một câu chuyện giản dị, chứa đầy bất ngờ cho tới trang cuối cùng. Và đẹp lộng lẫy, vì lòng vị tha và tình yêu thương, khiến mắt rưng rưng vì một nỗi mừng vui hân hoan. Cuốn sách như một đốm lửa thắp lên lòng khát khao sống tốt trên đời."},
                //new Sach { SachId = "SACH00000030", LoaiSachCTId = 1, TacGiaId =  1, HinhAnh = "SACH00000030.png", TenSach = "Làm bạn với bầu trời", MoTa = "Một câu chuyện giản dị, chứa đầy bất ngờ cho tới trang cuối cùng. Và đẹp lộng lẫy, vì lòng vị tha và tình yêu thương, khiến mắt rưng rưng vì một nỗi mừng vui hân hoan. Cuốn sách như một đốm lửa thắp lên lòng khát khao sống tốt trên đời."},
                //new Sach { SachId = "SACH00000031", LoaiSachCTId = 1, TacGiaId =  1, HinhAnh = "SACH00000031.png", TenSach = "Làm bạn với bầu trời", MoTa = "Một câu chuyện giản dị, chứa đầy bất ngờ cho tới trang cuối cùng. Và đẹp lộng lẫy, vì lòng vị tha và tình yêu thương, khiến mắt rưng rưng vì một nỗi mừng vui hân hoan. Cuốn sách như một đốm lửa thắp lên lòng khát khao sống tốt trên đời."},
                //new Sach { SachId = "SACH00000032", LoaiSachCTId = 1, TacGiaId =  1, HinhAnh = "SACH00000032.png", TenSach = "Làm bạn với bầu trời", MoTa = "Một câu chuyện giản dị, chứa đầy bất ngờ cho tới trang cuối cùng. Và đẹp lộng lẫy, vì lòng vị tha và tình yêu thương, khiến mắt rưng rưng vì một nỗi mừng vui hân hoan. Cuốn sách như một đốm lửa thắp lên lòng khát khao sống tốt trên đời."},
                //new Sach { SachId = "SACH00000033", LoaiSachCTId = 1, TacGiaId =  1, HinhAnh = "SACH00000033.png", TenSach = "Làm bạn với bầu trời", MoTa = "Một câu chuyện giản dị, chứa đầy bất ngờ cho tới trang cuối cùng. Và đẹp lộng lẫy, vì lòng vị tha và tình yêu thương, khiến mắt rưng rưng vì một nỗi mừng vui hân hoan. Cuốn sách như một đốm lửa thắp lên lòng khát khao sống tốt trên đời."},
                //new Sach { SachId = "SACH00000034", LoaiSachCTId = 1, TacGiaId =  1, HinhAnh = "SACH00000034.png", TenSach = "Làm bạn với bầu trời", MoTa = "Một câu chuyện giản dị, chứa đầy bất ngờ cho tới trang cuối cùng. Và đẹp lộng lẫy, vì lòng vị tha và tình yêu thương, khiến mắt rưng rưng vì một nỗi mừng vui hân hoan. Cuốn sách như một đốm lửa thắp lên lòng khát khao sống tốt trên đời."},
                //new Sach { SachId = "SACH00000035", LoaiSachCTId = 1, TacGiaId =  1, HinhAnh = "SACH00000035.png", TenSach = "Làm bạn với bầu trời", MoTa = "Một câu chuyện giản dị, chứa đầy bất ngờ cho tới trang cuối cùng. Và đẹp lộng lẫy, vì lòng vị tha và tình yêu thương, khiến mắt rưng rưng vì một nỗi mừng vui hân hoan. Cuốn sách như một đốm lửa thắp lên lòng khát khao sống tốt trên đời."},
                //new Sach { SachId = "SACH00000036", LoaiSachCTId = 1, TacGiaId =  1, HinhAnh = "SACH00000036.png", TenSach = "Làm bạn với bầu trời", MoTa = "Một câu chuyện giản dị, chứa đầy bất ngờ cho tới trang cuối cùng. Và đẹp lộng lẫy, vì lòng vị tha và tình yêu thương, khiến mắt rưng rưng vì một nỗi mừng vui hân hoan. Cuốn sách như một đốm lửa thắp lên lòng khát khao sống tốt trên đời."},
                //new Sach { SachId = "SACH00000037", LoaiSachCTId = 1, TacGiaId =  1, HinhAnh = "SACH00000037.png", TenSach = "Làm bạn với bầu trời", MoTa = "Một câu chuyện giản dị, chứa đầy bất ngờ cho tới trang cuối cùng. Và đẹp lộng lẫy, vì lòng vị tha và tình yêu thương, khiến mắt rưng rưng vì một nỗi mừng vui hân hoan. Cuốn sách như một đốm lửa thắp lên lòng khát khao sống tốt trên đời."},
                //new Sach { SachId = "SACH00000038", LoaiSachCTId = 1, TacGiaId =  1, HinhAnh = "SACH00000038.png", TenSach = "Làm bạn với bầu trời", MoTa = "Một câu chuyện giản dị, chứa đầy bất ngờ cho tới trang cuối cùng. Và đẹp lộng lẫy, vì lòng vị tha và tình yêu thương, khiến mắt rưng rưng vì một nỗi mừng vui hân hoan. Cuốn sách như một đốm lửa thắp lên lòng khát khao sống tốt trên đời."},
                //new Sach { SachId = "SACH00000039", LoaiSachCTId = 1, TacGiaId =  1, HinhAnh = "SACH00000039.png", TenSach = "Làm bạn với bầu trời", MoTa = "Một câu chuyện giản dị, chứa đầy bất ngờ cho tới trang cuối cùng. Và đẹp lộng lẫy, vì lòng vị tha và tình yêu thương, khiến mắt rưng rưng vì một nỗi mừng vui hân hoan. Cuốn sách như một đốm lửa thắp lên lòng khát khao sống tốt trên đời."},
                //new Sach { SachId = "SACH00000040", LoaiSachCTId = 1, TacGiaId =  1, HinhAnh = "SACH00000040.png", TenSach = "Làm bạn với bầu trời", MoTa = "Một câu chuyện giản dị, chứa đầy bất ngờ cho tới trang cuối cùng. Và đẹp lộng lẫy, vì lòng vị tha và tình yêu thương, khiến mắt rưng rưng vì một nỗi mừng vui hân hoan. Cuốn sách như một đốm lửa thắp lên lòng khát khao sống tốt trên đời."},
                //new Sach { SachId = "SACH00000041", LoaiSachCTId = 1, TacGiaId =  1, HinhAnh = "SACH00000041.png", TenSach = "Làm bạn với bầu trời", MoTa = "Một câu chuyện giản dị, chứa đầy bất ngờ cho tới trang cuối cùng. Và đẹp lộng lẫy, vì lòng vị tha và tình yêu thương, khiến mắt rưng rưng vì một nỗi mừng vui hân hoan. Cuốn sách như một đốm lửa thắp lên lòng khát khao sống tốt trên đời."},
                //new Sach { SachId = "SACH00000042", LoaiSachCTId = 1, TacGiaId =  1, HinhAnh = "SACH00000042.png", TenSach = "Làm bạn với bầu trời", MoTa = "Một câu chuyện giản dị, chứa đầy bất ngờ cho tới trang cuối cùng. Và đẹp lộng lẫy, vì lòng vị tha và tình yêu thương, khiến mắt rưng rưng vì một nỗi mừng vui hân hoan. Cuốn sách như một đốm lửa thắp lên lòng khát khao sống tốt trên đời."},
                //new Sach { SachId = "SACH00000043", LoaiSachCTId = 1, TacGiaId =  1, HinhAnh = "SACH00000043.png", TenSach = "Làm bạn với bầu trời", MoTa = "Một câu chuyện giản dị, chứa đầy bất ngờ cho tới trang cuối cùng. Và đẹp lộng lẫy, vì lòng vị tha và tình yêu thương, khiến mắt rưng rưng vì một nỗi mừng vui hân hoan. Cuốn sách như một đốm lửa thắp lên lòng khát khao sống tốt trên đời."},
                //new Sach { SachId = "SACH00000044", LoaiSachCTId = 1, TacGiaId =  1, HinhAnh = "SACH00000044.png", TenSach = "Làm bạn với bầu trời", MoTa = "Một câu chuyện giản dị, chứa đầy bất ngờ cho tới trang cuối cùng. Và đẹp lộng lẫy, vì lòng vị tha và tình yêu thương, khiến mắt rưng rưng vì một nỗi mừng vui hân hoan. Cuốn sách như một đốm lửa thắp lên lòng khát khao sống tốt trên đời."},
                //new Sach { SachId = "SACH00000045", LoaiSachCTId = 1, TacGiaId =  1, HinhAnh = "SACH00000045.png", TenSach = "Làm bạn với bầu trời", MoTa = "Một câu chuyện giản dị, chứa đầy bất ngờ cho tới trang cuối cùng. Và đẹp lộng lẫy, vì lòng vị tha và tình yêu thương, khiến mắt rưng rưng vì một nỗi mừng vui hân hoan. Cuốn sách như một đốm lửa thắp lên lòng khát khao sống tốt trên đời."},
                //new Sach { SachId = "SACH00000046", LoaiSachCTId = 1, TacGiaId =  1, HinhAnh = "SACH00000046.png", TenSach = "Làm bạn với bầu trời", MoTa = "Một câu chuyện giản dị, chứa đầy bất ngờ cho tới trang cuối cùng. Và đẹp lộng lẫy, vì lòng vị tha và tình yêu thương, khiến mắt rưng rưng vì một nỗi mừng vui hân hoan. Cuốn sách như một đốm lửa thắp lên lòng khát khao sống tốt trên đời."},
                //new Sach { SachId = "SACH00000047", LoaiSachCTId = 1, TacGiaId =  1, HinhAnh = "SACH00000047.png", TenSach = "Làm bạn với bầu trời", MoTa = "Một câu chuyện giản dị, chứa đầy bất ngờ cho tới trang cuối cùng. Và đẹp lộng lẫy, vì lòng vị tha và tình yêu thương, khiến mắt rưng rưng vì một nỗi mừng vui hân hoan. Cuốn sách như một đốm lửa thắp lên lòng khát khao sống tốt trên đời."},
                //new Sach { SachId = "SACH00000048", LoaiSachCTId = 1, TacGiaId =  1, HinhAnh = "SACH00000048.png", TenSach = "Làm bạn với bầu trời", MoTa = "Một câu chuyện giản dị, chứa đầy bất ngờ cho tới trang cuối cùng. Và đẹp lộng lẫy, vì lòng vị tha và tình yêu thương, khiến mắt rưng rưng vì một nỗi mừng vui hân hoan. Cuốn sách như một đốm lửa thắp lên lòng khát khao sống tốt trên đời."},
                //new Sach { SachId = "SACH00000049", LoaiSachCTId = 1, TacGiaId =  1, HinhAnh = "SACH00000049.png", TenSach = "Làm bạn với bầu trời", MoTa = "Một câu chuyện giản dị, chứa đầy bất ngờ cho tới trang cuối cùng. Và đẹp lộng lẫy, vì lòng vị tha và tình yêu thương, khiến mắt rưng rưng vì một nỗi mừng vui hân hoan. Cuốn sách như một đốm lửa thắp lên lòng khát khao sống tốt trên đời."},
                //new Sach { SachId = "SACH00000050", LoaiSachCTId = 1, TacGiaId =  1, HinhAnh = "SACH00000050.png", TenSach = "Làm bạn với bầu trời", MoTa = "Một câu chuyện giản dị, chứa đầy bất ngờ cho tới trang cuối cùng. Và đẹp lộng lẫy, vì lòng vị tha và tình yêu thương, khiến mắt rưng rưng vì một nỗi mừng vui hân hoan. Cuốn sách như một đốm lửa thắp lên lòng khát khao sống tốt trên đời."} 
            };
            return sachs;
        }
        public static List<AnhPhu> SetAnhPhu()
        {
            List<AnhPhu> anhPhus = new List<AnhPhu>()
            {
                new AnhPhu(){ HinhAnh = "SACH00000001_AnhPhu.jpg" , SachId = "SACH00000001"}
            };
            return anhPhus;
        }
        public static List<BangGia> SetBangGia()
        {
            List<BangGia> BangGias = new List<BangGia>()
            {
                new BangGia() { SachId = "SACH00000001", DonGia = 100000, NgayCoHieuLuc = DateTime.Now },
                new BangGia() { SachId = "SACH00000002", DonGia = 100000, NgayCoHieuLuc = DateTime.Now },
                new BangGia() { SachId = "SACH00000003", DonGia = 100000, NgayCoHieuLuc = DateTime.Now },
                new BangGia() { SachId = "SACH00000004", DonGia = 100000, NgayCoHieuLuc = DateTime.Now },
                new BangGia() { SachId = "SACH00000005", DonGia = 100000, NgayCoHieuLuc = DateTime.Now },
                new BangGia() { SachId = "SACH00000006", DonGia = 100000, NgayCoHieuLuc = DateTime.Now },
                new BangGia() { SachId = "SACH00000007", DonGia = 50000, NgayCoHieuLuc = DateTime.Now },
                new BangGia() { SachId = "SACH00000008", DonGia = 62000, NgayCoHieuLuc = DateTime.Now },
                new BangGia() { SachId = "SACH00000009", DonGia = 95000, NgayCoHieuLuc = DateTime.Now },
                new BangGia() { SachId = "SACH00000010", DonGia = 65000, NgayCoHieuLuc = DateTime.Now },
                new BangGia() { SachId = "SACH00000011", DonGia = 89000, NgayCoHieuLuc = DateTime.Now },
                new BangGia() { SachId = "SACH00000012", DonGia = 79000, NgayCoHieuLuc = DateTime.Now },
                new BangGia() { SachId = "SACH00000013", DonGia = 66750, NgayCoHieuLuc = DateTime.Now },
                new BangGia() { SachId = "SACH00000014", DonGia = 85000, NgayCoHieuLuc = DateTime.Now },
                new BangGia() { SachId = "SACH00000015", DonGia = 119000, NgayCoHieuLuc = DateTime.Now },
                new BangGia() { SachId = "SACH00000016", DonGia = 98000, NgayCoHieuLuc = DateTime.Now },
                new BangGia() { SachId = "SACH00000017", DonGia = 88000, NgayCoHieuLuc = DateTime.Now },
                new BangGia() { SachId = "SACH00000018", DonGia = 75000, NgayCoHieuLuc = DateTime.Now },
                new BangGia() { SachId = "SACH00000019", DonGia = 89000, NgayCoHieuLuc = DateTime.Now },
                new BangGia() { SachId = "SACH00000020", DonGia = 125000, NgayCoHieuLuc = DateTime.Now },
                //new BangGia() { SachId = "SACH00000021", DonGia = 100000, NgayCoHieuLuc = DateTime.Now },
                //new BangGia() { SachId = "SACH00000022", DonGia = 100000, NgayCoHieuLuc = DateTime.Now },
                //new BangGia() { SachId = "SACH00000023", DonGia = 100000, NgayCoHieuLuc = DateTime.Now },
                //new BangGia() { SachId = "SACH00000024", DonGia = 100000, NgayCoHieuLuc = DateTime.Now },
                //new BangGia() { SachId = "SACH00000025", DonGia = 100000, NgayCoHieuLuc = DateTime.Now },
                //new BangGia() { SachId = "SACH00000026", DonGia = 100000, NgayCoHieuLuc = DateTime.Now },
                //new BangGia() { SachId = "SACH00000027", DonGia = 100000, NgayCoHieuLuc = DateTime.Now },
                //new BangGia() { SachId = "SACH00000028", DonGia = 100000, NgayCoHieuLuc = DateTime.Now },
                //new BangGia() { SachId = "SACH00000029", DonGia = 100000, NgayCoHieuLuc = DateTime.Now },
                //new BangGia() { SachId = "SACH00000030", DonGia = 100000, NgayCoHieuLuc = DateTime.Now },
                //new BangGia() { SachId = "SACH00000031", DonGia = 100000, NgayCoHieuLuc = DateTime.Now },
                //new BangGia() { SachId = "SACH00000032", DonGia = 100000, NgayCoHieuLuc = DateTime.Now },
                //new BangGia() { SachId = "SACH00000033", DonGia = 100000, NgayCoHieuLuc = DateTime.Now },
                //new BangGia() { SachId = "SACH00000034", DonGia = 100000, NgayCoHieuLuc = DateTime.Now },
                //new BangGia() { SachId = "SACH00000035", DonGia = 100000, NgayCoHieuLuc = DateTime.Now },
                //new BangGia() { SachId = "SACH00000036", DonGia = 100000, NgayCoHieuLuc = DateTime.Now },
                //new BangGia() { SachId = "SACH00000037", DonGia = 100000, NgayCoHieuLuc = DateTime.Now },
                //new BangGia() { SachId = "SACH00000038", DonGia = 100000, NgayCoHieuLuc = DateTime.Now },
                //new BangGia() { SachId = "SACH00000039", DonGia = 100000, NgayCoHieuLuc = DateTime.Now },
                //new BangGia() { SachId = "SACH00000040", DonGia = 100000, NgayCoHieuLuc = DateTime.Now },
                //new BangGia() { SachId = "SACH00000041", DonGia = 100000, NgayCoHieuLuc = DateTime.Now },
                //new BangGia() { SachId = "SACH00000042", DonGia = 100000, NgayCoHieuLuc = DateTime.Now },
                //new BangGia() { SachId = "SACH00000043", DonGia = 100000, NgayCoHieuLuc = DateTime.Now },
                //new BangGia() { SachId = "SACH00000044", DonGia = 100000, NgayCoHieuLuc = DateTime.Now },
                //new BangGia() { SachId = "SACH00000045", DonGia = 100000, NgayCoHieuLuc = DateTime.Now },
                //new BangGia() { SachId = "SACH00000046", DonGia = 100000, NgayCoHieuLuc = DateTime.Now },
                //new BangGia() { SachId = "SACH00000047", DonGia = 100000, NgayCoHieuLuc = DateTime.Now },
                //new BangGia() { SachId = "SACH00000048", DonGia = 100000, NgayCoHieuLuc = DateTime.Now },
                //new BangGia() { SachId = "SACH00000049", DonGia = 100000, NgayCoHieuLuc = DateTime.Now },
                //new BangGia() { SachId = "SACH00000050", DonGia = 100000, NgayCoHieuLuc = DateTime.Now }
            };
            return BangGias;
        }
        public static List<KhuyenMai> SetKhuyenMai()
        {
            List<KhuyenMai> khuyenMais = new List<KhuyenMai>()
            {
                new KhuyenMai() { MaKhuyenMaiId = "KMT10", TenKhuyenMai = "Khuyến mãi tháng 10", NgayBatDau = DateTime.Today, NgayKetThuc = Convert.ToDateTime("2019-11-30 0:0:0"), TrangThai = true, HinhAnh = "KmT10.jpg" },
                new KhuyenMai() { MaKhuyenMaiId = "KMTMuaDong", TenKhuyenMai = "Khuyến mãi mùa đông", NgayBatDau = DateTime.Today, NgayKetThuc = Convert.ToDateTime("2020-01-30 0:0:0"), TrangThai = true, HinhAnh = "KmMuaDong.jpg" },
                new KhuyenMai() { MaKhuyenMaiId = "KMCuoiNam", TenKhuyenMai = "Khuyến mãi cuối năm", NgayBatDau = DateTime.Today, NgayKetThuc = Convert.ToDateTime("2020-01-01 0:0:0"), TrangThai = true, HinhAnh = "KmCuoiNam.jpg" }
            };
            return khuyenMais;
        }
    } 
}
