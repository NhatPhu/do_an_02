﻿using BookStore_Core.Models;

namespace BookStore_Core.ViewModel
{
    public class SachApiViewModel
    {
        //public Sach Sach { get; set; }
        public BangGia BangGia { get; set; }
        public int SoSachBanDuoc { get; set; }
    }
}
