﻿using BookStore_Core.DAL;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace BookStore_Core.Components
{
    public class TacGiaMenu : ViewComponent
    {
        private readonly BookStoreContext _context;
        public TacGiaMenu(BookStoreContext context)
        {
            _context = context;
        }

        public async Task<IViewComponentResult> InvokeAsync()
        {
            var tacGias = await _context.TacGias.Where(c => c.TacGiaId != "1").ToListAsync();
            return View(tacGias);
        }
    }
}
