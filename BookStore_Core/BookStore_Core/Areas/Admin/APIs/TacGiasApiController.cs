﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using BookStore_Core.DAL;
using BookStore_Core.Models;

namespace BookStore_Core.Areas.Admin.APIs
{
    [Route("api/[controller]")]
    [ApiController]
    public class TacGiasApiController : ControllerBase
    {
        private readonly BookStoreContext _context;

        public TacGiasApiController(BookStoreContext context)
        {
            _context = context;
        }

        // GET: api/TacGiasApi
        [HttpGet]
        public async Task<ActionResult<IEnumerable<TacGia>>> GetTacGias()
        {
            return await _context.TacGias.ToListAsync();
        }

        // GET: api/TacGiasApi/5
        [HttpGet("{id}")]
        public async Task<ActionResult<TacGia>> GetTacGia(string id)
        {
            var tacGia = await _context.TacGias.FindAsync(id);

            if (tacGia == null)
            {
                return NotFound();
            }

            return tacGia;
        }

        // PUT: api/TacGiasApi/5
        [HttpPut("{id}")]
        public async Task<IActionResult> PutTacGia(string id, TacGia tacGia)
        {
            if (id != tacGia.TacGiaId)
            {
                return BadRequest();
            }

            _context.Entry(tacGia).State = EntityState.Modified;

            try
            {
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!TacGiaExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return NoContent();
        }

        // POST: api/TacGiasApi
        [HttpPost]
        public async Task<ActionResult<TacGia>> PostTacGia(TacGia tacGia)
        {
            _context.TacGias.Add(tacGia);
            await _context.SaveChangesAsync();

            return CreatedAtAction("GetTacGia", new { id = tacGia.TacGiaId }, tacGia);
        }

        // DELETE: api/TacGiasApi/5
        [HttpDelete("{id}")]
        public async Task<ActionResult<TacGia>> DeleteTacGia(string id)
        {
            var tacGia = await _context.TacGias.FindAsync(id);
            if (tacGia == null)
            {
                return NotFound();
            }

            _context.TacGias.Remove(tacGia);
            await _context.SaveChangesAsync();

            return tacGia;
        }

        private bool TacGiaExists(string id)
        {
            return _context.TacGias.Any(e => e.TacGiaId == id);
        }
    }
}
