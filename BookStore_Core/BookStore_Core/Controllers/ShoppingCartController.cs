﻿using BookStore_Core.DAL;
using BookStore_Core.Models;
using BookStore_Core.ViewModel;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Stripe;
using System;
using System.Collections.Generic;
using System.Linq;

namespace BookStore_Core.Controllers
{
    public class ShoppingCartController : Controller
    {
        private readonly BookStoreContext _context;
        // GET: ShoppingCart
        public ShoppingCartController(BookStoreContext context)
        {
            _context = context;
        }
        public ActionResult AddToShoppingCart(string id, int qty)
        {
            if (HttpContext.Session.GetString("UserName") == null || HttpContext.Session.GetString("UserName") == "")
            {
                HttpContext.Session.SetString("IdSach", id);
                return RedirectToAction("Login", "Account", new { id = id });
            }

            if (HttpContext.Session.GetString("RoleId").ToString() == "1" || HttpContext.Session.GetString("RoleId").ToString() == "3")
            {
                return RedirectToAction("Details", "Saches", new { id = id });
            }
            var cusId = HttpContext.Session.GetString("UserName").ToString();
            var user = _context.Customers.Where(p => p.CustomerEmail == cusId).SingleOrDefault();
            if (_context.Carts.Any() == false)
            {
                _context.Carts.Add(new Cart() { CustomerId = user.CustomerId, SachId = id, Amount = qty });
                _context.SaveChanges();
                return RedirectToAction("Details", "Saches", new { id = id });
            }
            var cart = _context.Carts.Where(p => p.CustomerId == user.CustomerId);
            if (cart.Any() == false)
            {
                _context.Carts.Add(new Cart() { CustomerId = user.CustomerId, SachId = id, Amount = qty });
                _context.SaveChanges();
                return RedirectToAction("Details", "Saches", new { id = id });
            }
            bool isFindSameBook = false;
            Cart tam = new Cart();
            foreach (var x in cart)
            {
                if (x.SachId == id)
                {
                    tam = x;
                    isFindSameBook = true;
                    break;
                }
            }
            tam.Amount += qty;
            _context.Carts.Update(tam);
            _context.SaveChanges();
            if (isFindSameBook)
            {
                _context.SaveChanges();
                return RedirectToAction("Details", "Saches", new { id = id });
            }
            _context.Carts.Add(new Cart() { CartId = DateTime.Now, CustomerId = user.CustomerId, SachId = id, Amount = qty });
            _context.SaveChanges();
            return RedirectToAction("Details", "Saches", new { id = id });
        }

        public IActionResult ThanhToan()
        {

            List<ShoppingCartViewModel> cartVM = new List<ShoppingCartViewModel>();
            var acc = HttpContext.Session.GetString("UserName").ToString();
            var khachHang = _context.Customers.FirstOrDefault(p => p.CustomerEmail == acc);
            var cart = _context.Carts.Include(p => p.Customer).Include(p => p.Sach).Where(p => p.CustomerId == khachHang.CustomerId);
            int[] soLuong = new int[cart.Count()];
            int i = 0;
            foreach (var x in cart)
            {
                var bangGia = _context.BangGias.LastOrDefault(p => p.SachId == x.SachId);
                cartVM.Add(new ShoppingCartViewModel() { Cart = x, BangGia = bangGia, GiaTong = x.Amount * (bangGia.DonGia - bangGia.DonGia * bangGia.PhanTramKhuyenMai / 100) });
                soLuong[i] = x.Amount;
                i++;
            }
            ViewBag.SoLuong = soLuong;
            return View(cartVM);
        }
        [HttpPost]
        public IActionResult KetThucThanhToan(int[] SoLuong, string stripeEmail, string stripeToken)
        {
            var customers = new CustomerService();
            var charges = new ChargeService();
            var customer = customers.Create(new CustomerCreateOptions
            {
                Email = stripeEmail,
                SourceToken = stripeToken
            });
            //int a = SoLuong[0];
            var cus = _context.Customers.Where(p => p.CustomerEmail == HttpContext.Session.GetString("UserName").ToString()).FirstOrDefault();
            var hoaDon = new HoaDon() { HoaDonId = DateTime.Now, CustomerId = cus.CustomerId, TrangThai = -1 };
            _context.HoaDons.Add(hoaDon);
            _context.SaveChanges();
            int tongTien = 0;
            var cart = _context.Carts.Where(p => p.CustomerId == cus.CustomerId);
            int i = 0;
            foreach (var x in cart)
            {
                var gia = _context.BangGias.LastOrDefault(p => p.SachId == x.SachId);
                _context.ChiTietHoaDons.Add(new ChiTietHoaDon() { HoaDonId = hoaDon.HoaDonId, SachId = x.SachId, SoLuong = SoLuong[i], TongTien = SoLuong[i] * (gia.DonGia - gia.DonGia * gia.PhanTramKhuyenMai / 100) });
                tongTien += SoLuong[i] * (gia.DonGia - gia.DonGia * gia.PhanTramKhuyenMai / 100);
                _context.Remove(x);
                i++;
            }
            if (cus.DiemSo >= 10000)
            {
                tongTien -= tongTien * 20 / 100;
            }
            else if (cus.DiemSo >= 5000)
            {
                tongTien -= tongTien * 10 / 100;
            }
            else if (cus.DiemSo >= 1000)
            {
                tongTien -= tongTien * 5 / 100;
            }
            hoaDon.TongTien = tongTien;
            _context.Update(hoaDon);
            _context.SaveChanges();
            var charge = charges.Create(new ChargeCreateOptions
            {
                Amount = tongTien,
                Description = "Test Payment",
                Currency = "VND",
                CustomerId = customer.Id
            });
            if (charge.Status == "succeeded")
            {
                string BalanceTransactionId = charge.BalanceTransactionId;
                RedirectToAction("Index", "Home");
            }
            return RedirectToAction("Index", "Home");
        }
    }
}