﻿using BookStore_Core.DAL;
using BookStore_Core.Models;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using OfficeOpenXml;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Threading.Tasks;

namespace BookStore_Core.Controllers
{
    public class HoaDonsController : Controller
    {
        private readonly BookStoreContext _context;

        public HoaDonsController(BookStoreContext context)
        {
            _context = context;
        }

        // GET: HoaDons
        public async Task<IActionResult> Index()
        {
            return View(await _context.HoaDons.ToListAsync());
        }

        // GET: HoaDons/Details/5
        public async Task<IActionResult> Details(DateTime? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var hoaDon = await _context.HoaDons
                .FirstOrDefaultAsync(m => m.HoaDonId == id);
            if (hoaDon == null)
            {
                return NotFound();
            }
            return View(hoaDon);
        }
        public IActionResult LichSuMuaHang(string id)
        {
            var cus = _context.Customers.FirstOrDefault(p => p.CustomerEmail == id);
            var hoaDon = _context.HoaDons.Where(p => p.CustomerId == cus.CustomerId && p.XacNhan == true).OrderByDescending(p => p.HoaDonId);
            List<int> a = new List<int>();
            foreach (var x in hoaDon)
            {
                var cthd = _context.ChiTietHoaDons.Where(p => p.HoaDonId == x.HoaDonId);
                var count = 0;
                foreach (var i in cthd)
                {
                    count += i.SoLuong;
                }
                a.Add(count);
            }
            ViewBag.SoLuong = a;
            return View(hoaDon);
        }
        // GET: HoaDons/Create
        public IActionResult Create()
        {
            return View();
        }
        public IActionResult ChiTietHoaDon(string id)
        {
            var chiTietHoaDon = _context.ChiTietHoaDons.Include(p => p.Sach).Include(p => p.HoaDon).Where(p => p.HoaDonId.ToString("yyyyMMddHHmmss") == id).ToList();
            return View(chiTietHoaDon);
        }
        public IActionResult DanhGia(int id)
        {
            if (HttpContext.Session.GetString("UserName") == null || HttpContext.Session.GetString("UserName").ToString() == "")
            {
                return RedirectToAction("Login", "Account");
            }
            var chiTietHoaDon = _context.ChiTietHoaDons.Include(p => p.Sach).Include(p => p.HoaDon).Where(p => p.CTHDId == id).FirstOrDefault();
            if (chiTietHoaDon.DanhGia > 0)
            {
                return RedirectToAction("LichSuMuaHang", "HoaDons", new { id = HttpContext.Session.GetString("UserName").ToString() });
            }
            return View(chiTietHoaDon);
        }
        [HttpPost]
        public IActionResult DanhGia(int id, int rating3, string comment)
        {

            var chiTietHoaDon = _context.ChiTietHoaDons.Include(p => p.Sach).FirstOrDefault(p => p.CTHDId == id);
            if (rating3 == 0)
            {
                ViewBag.Loi = "Bạn bắt buộc phải chọn sao để hoàn thành đánh giá";
                return View(chiTietHoaDon);
            }
            else
            {
                chiTietHoaDon.DanhGia = rating3;
                chiTietHoaDon.IsDanhGia = true;
                _context.Update(chiTietHoaDon);
                _context.SaveChanges();
            }

            //Tính sao trung bình
            double tam = (chiTietHoaDon.Sach.SaoTrungBinh * chiTietHoaDon.Sach.LuotDanhGia + rating3) * 1.0 / (chiTietHoaDon.Sach.LuotDanhGia + 1);
            chiTietHoaDon.Sach.SaoTrungBinh = (int)Math.Round((double)tam);

            //Thêm 1 lượt đánh giá
            chiTietHoaDon.Sach.LuotDanhGia++;
            //Cập nhất sách
            _context.Update(chiTietHoaDon.Sach);
            _context.SaveChanges();

            //đánh dấu là chi tiết hóa đơn đã đc đánh giá rồi
            var hoaDon = _context.HoaDons.FirstOrDefault(p => p.HoaDonId == chiTietHoaDon.HoaDonId);
            var dsChiTietHoaDon = _context.ChiTietHoaDons.Where(p => p.HoaDonId == hoaDon.HoaDonId && p.DanhGia == 0);
            if (dsChiTietHoaDon.Any() == false)
            {
                hoaDon.IsDanhGia = true;
                _context.Update(hoaDon);
                _context.SaveChanges();
            }

            //Kiểm tra xem khách hàng có comment gì ko?
            if (comment != null)
            {
                var cus = _context.Customers.FirstOrDefault(p => p.CustomerEmail == HttpContext.Session.GetString("UserName").ToString());
                _context.SachDuocThiches.Add(new SachDuocThich() { ThichId = DateTime.Now, CustomerId = cus.CustomerId, SachId = chiTietHoaDon.Sach.SachId, NhanXet = comment, DanhGia = rating3 });
                _context.SaveChanges();
            }
            return RedirectToAction("LichSuMuaHang", "HoaDons", new { id = HttpContext.Session.GetString("UserName").ToString() });
        }
        // POST: HoaDons/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create([Bind("HoaDonId,CustomerId,TongTien")] HoaDon hoaDon)
        {
            if (ModelState.IsValid)
            {
                _context.Add(hoaDon);
                await _context.SaveChangesAsync();
                return RedirectToAction(nameof(Index));
            }
            return View(hoaDon);
        }

        // GET: HoaDons/Edit/5
        public async Task<IActionResult> Edit(DateTime? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var hoaDon = await _context.HoaDons.FindAsync(id);
            if (hoaDon == null)
            {
                return NotFound();
            }
            return View(hoaDon);
        }

        // POST: HoaDons/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(DateTime id, [Bind("HoaDonId,CustomerId,TongTien")] HoaDon hoaDon)
        {
            if (id != hoaDon.HoaDonId)
            {
                return NotFound();
            }

            if (ModelState.IsValid)
            {
                try
                {
                    _context.Update(hoaDon);
                    await _context.SaveChangesAsync();
                }
                catch (DbUpdateConcurrencyException)
                {
                    if (!HoaDonExists(hoaDon.HoaDonId))
                    {
                        return NotFound();
                    }
                    else
                    {
                        throw;
                    }
                }
                return RedirectToAction(nameof(Index));
            }
            return View(hoaDon);
        }

        // GET: HoaDons/Delete/5
        public async Task<IActionResult> Delete(DateTime? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var hoaDon = await _context.HoaDons
                .FirstOrDefaultAsync(m => m.HoaDonId == id);
            if (hoaDon == null)
            {
                return NotFound();
            }

            return View(hoaDon);
        }

        // POST: HoaDons/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> DeleteConfirmed(DateTime id)
        {
            var hoaDon = await _context.HoaDons.FindAsync(id);
            _context.HoaDons.Remove(hoaDon);
            await _context.SaveChangesAsync();
            return RedirectToAction(nameof(Index));
        }


        public IActionResult XuatHoaDon(string id)
        {
            return File(FomatExcel(id), "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet", "HoaDon.xlsx");
        }

        private bool HoaDonExists(DateTime id)
        {
            return _context.HoaDons.Any(e => e.HoaDonId == id);
        }

        public byte[] FomatExcel(string id)
        {
            string[] col_name = new string[] {
                "STT",
                "Tên sách",
                "Số lượng",
                "Đơn giá",
                "Tổng tiền"
            };
            //step 2: Create result byte array
            //byte[] result;
            //step 3: create a new package using memory safe structure
            using (var package = new ExcelPackage())
            {
                var hoadon = _context.HoaDons.Include(p => p.Customer).Where(p => p.HoaDonId.ToString("yyyyMMddHHmmss") == id).FirstOrDefault();
                ExcelWorksheet worksheet = null;
                worksheet = package.Workbook.Worksheets.Add("HoaDonBanHang");
                worksheet.Cells[1, 1].Value = "HÓA ĐƠN BÁN LẺ";
                worksheet.Cells[1, 1].Style.Font.Bold = true;
                worksheet.Cells[2, 1].Value = "Lập lúc:";
                worksheet.Cells[2, 2].Value = hoadon.HoaDonId.ToString();
                worksheet.Cells[2, 3].Value = "Bởi Admin BookStore";
                worksheet.Cells[3, 1].Value = "Cho khách hàng";
                worksheet.Cells[3, 2].Value = hoadon.Customer.CustomerName;

                worksheet.Cells[4, 3].Value = "NỘI DUNG";
                for (int i = 0; i < col_name.Length; i++)
                {
                    worksheet.Cells[5, i + 1].Style.Font.Size = 14;
                    worksheet.Cells[5, i + 1].Value = col_name[i];
                    worksheet.Cells[5, i + 1].Style.Font.Bold = true;
                    //border the cell
                    worksheet.Cells[5, i + 1].Style.Border.BorderAround(OfficeOpenXml.Style.ExcelBorderStyle.Thin);
                    //set backgroud color fpr each cells
                    worksheet.Cells[5, i + 1].Style.Fill.PatternType = OfficeOpenXml.Style.ExcelFillStyle.Solid;
                    worksheet.Cells[5, i + 1].Style.Fill.BackgroundColor.SetColor(Color.FromArgb(255, 243, 214));
                }

                int row = 6;
                var cthd = _context.ChiTietHoaDons.Include(p => p.Sach).Where(p => p.HoaDonId == hoadon.HoaDonId).ToList();
                int stt = 1;
                //step 6: loop through query result and fill in cells
                foreach (ChiTietHoaDon item in cthd)
                {
                    for (int col = 1; col <= 12; col++)
                    {
                        worksheet.Cells[row, col].Style.Font.Size = 12;
                        worksheet.Cells[row, col].Style.Border.BorderAround(OfficeOpenXml.Style.ExcelBorderStyle.Thin);
                    }
                    //set data
                    worksheet.Cells[row, 1].Value = stt++;
                    worksheet.Cells[row, 2].Value = item.Sach.TenSach;
                    worksheet.Cells[row, 3].Value = item.SoLuong;
                    worksheet.Cells[row, 4].Value = item.TongTien / item.SoLuong;
                    worksheet.Cells[row, 5].Value = item.TongTien;
                    //toggle backgroud color
                    //even row with ribbon style
                    if (row % 2 == 0)
                    {
                        for (int i = 1; i <= col_name.Length; i++)
                        {
                            worksheet.Cells[row, i].Style.Fill.PatternType = OfficeOpenXml.Style.ExcelFillStyle.Solid;
                            worksheet.Cells[row, i].Style.Fill.BackgroundColor.SetColor(Color.FromArgb(154, 211, 157));
                        }
                    }
                    row++;
                }
                worksheet.Cells[row, 5].Value = hoadon.TongTien;
                //step 7 : auto fit colums
                worksheet.Cells[worksheet.Dimension.Address].AutoFitColumns();
                //step 8: convert the package as byte array
                return package.GetAsByteArray();


                //step 4: Create a new worksheet

                //step 5: fill in header row
                //worksheet.Cells[row,col]. {Style, Value}

            }// end using
        }
    }
}
