﻿using BookStore_Core.DAL;
using BookStore_Core.Models;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;

namespace BookStore_Core.Controllers
{
    public class HomeController : Controller
    {
        private readonly BookStoreContext _context;
        public HomeController(BookStoreContext context)
        {
            _context = context;
        }
        public IActionResult Index()
        {
            //Hiển thị ảnh khuyến mãi trên panner.
            ViewBag.KhuyenMai = _context.KhuyenMais.Where(p => p.TrangThai == true).ToArray();

            List<SachViewModel> sachVM = new List<SachViewModel>();
            var dsSach = _context.Saches.Where(p => p.IsDel == false).ToList();
            foreach (var x in dsSach)
            {
                var bangGia = _context.BangGias.Where(p => p.SachId == x.SachId).LastOrDefault();
                int soSpBanDc = 0;
                string trangThai = "";
                if (_context.ChiTietHoaDons.Any())
                {
                    var slBan = _context.ChiTietHoaDons.Where(p => p.SachId == x.SachId && p.HoaDonId.Year == DateTime.Now.Year);
                    foreach (var k in slBan)
                    {
                        soSpBanDc += k.SoLuong;
                    }
                }
                else
                {
                    soSpBanDc = 0;
                }
                if (DateTime.Now.Year == x.NgayTao.Year && DateTime.Now.Month - x.NgayTao.Month < 2)
                {
                    trangThai = "Mới";
                }
                sachVM.Add(new SachViewModel() { Sach = x, BangGia = bangGia, SoSachBanDuoc = soSpBanDc, TrangThai = trangThai });
            }
            ViewBag.sachs = sachVM;

            //ViewBag.BestSeller = sachVM;
            return View(sachVM);
        }
    }
}
