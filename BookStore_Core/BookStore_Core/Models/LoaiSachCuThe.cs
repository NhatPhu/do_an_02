﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace BookStore_Core.Models
{
    public class LoaiSachCuThe
    {
        [Key]
        public string LoaiSachCTId { get; set; }
        public string TenLoaiSachCT { get; set; }
        public string LoaiSachTQId { get; set; }
        public virtual LoaiSachTongQuat LoaiSachTongQuat { get; set; }
        [NotMapped]
        public List<Sach> Sachs { get; set; }
    }
}
