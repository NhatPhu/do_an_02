﻿using System;
using System.ComponentModel.DataAnnotations;

namespace BookStore_Core.Models
{
    public class SachDuocThich
    {
        [Key]
        public DateTime ThichId { get; set; }
        public string CustomerId { get; set; }
        public string SachId { get; set; }
        public bool IsLike { get; set; }
        public int DanhGia { get; set; }
        public string NhanXet { get; set; }
        public virtual Sach Sach { get; set; }
        public virtual Customer Customer { get; set; }

    }
}
