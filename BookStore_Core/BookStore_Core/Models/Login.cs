﻿using System;
using System.ComponentModel.DataAnnotations;

namespace BookStore_Core.Models
{
    public class Login
    {
        [Key]
        public string LoginName { get; set; }
        public string Password { get; set; }
        public int RoleId { get; set; }
        public string ResetPassword { get; set; }
        public DateTime LastUpdate { get; set; }
        public bool IsDel { get; set; }

        public virtual Role Roles { get; set; }


    }
}
