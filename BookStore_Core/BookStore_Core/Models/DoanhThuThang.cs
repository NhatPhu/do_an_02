﻿using System;
using System.ComponentModel.DataAnnotations;

namespace BookStore_Core.Models
{
    public class DoanhThuThang
    {
        [Key]
        public DateTime DoanhThuId { get; set; }
        public int VanHoc { get; set; }
        public int KinhTe { get; set; }
        public int TamLy_KyNang { get; set; }
        public int NuoiCon { get; set; }
        public int SachThieuNhi { get; set; }
        public int TieuSu_HoiKy { get; set; }
        public int SGK { get; set; }
        public int SachNgoaiNgu { get; set; }
        public int TongTien { get; set; }
    }
}
