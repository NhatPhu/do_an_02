﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace BookStore_Core.Models
{
    public class Customer
    {
        [Key]
        public string CustomerId { get; set; }
        public string CustomerName { get; set; }
        public string CustomerEmail { get; set; }
        public string Phone { get; set; }
        public string Address { get; set; }
        public DateTime BirthDay { get; set; }
        public DateTime LastUpdate { get; set; }
        public int DiemSo { get; set; }
        public bool IsDel { get; set; }
        [NotMapped]
        public List<Cart> Carts { get; set; }
        [NotMapped]
        public List<SachDuocThich> SachDuocThichs { get; set; }
        [NotMapped]
        public List<HoaDon> HoaDons { get; set; }
        [NotMapped]
        public List<KhachHangThich> KhachHangThiches { get; set; }
    }
}
